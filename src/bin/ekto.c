/*
** $Id: ekto.c,v 1.160.1.2 2007/12/28 15:32:23 roberto Exp $
** Ekto stand-alone interpreter
** See Copyright Notice in ekto.h
*/


#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ekto_c

#include "Ekto.h"

#include "Ekto_Aux.h"
#include "Ekto_Lib.h"



static ekto_State *globalL = NULL;

static const char *progname = EKTO_PROGNAME;



static void lstop (ekto_State *L, ekto_Debug *ar) {
  (void)ar;  /* unused arg. */
  ekto_sethook(L, NULL, 0, 0);
  ektoL_error(L, "interrupted!");
}


static void laction (int i) {
  signal(i, SIG_DFL); /* if another SIGINT happens before lstop,
                              terminate process (default action) */
  ekto_sethook(globalL, lstop, EKTO_MASKCALL | EKTO_MASKRET | EKTO_MASKCOUNT, 1);
}


static void print_usage (void) {
  fprintf(stderr,
  "usage: %s [options] [script [args]].\n"
  "Available options are:\n"
  "  -e stat  execute string " EKTO_QL("stat") "\n"
  "  -l name  require library " EKTO_QL("name") "\n"
  "  -i       enter interactive mode after executing " EKTO_QL("script") "\n"
  "  -v       show version information\n"
  "  --       stop handling options\n"
  "  -        execute stdin and stop handling options\n"
  ,
  progname);
  fflush(stderr);
}


static void l_message (const char *pname, const char *msg) {
  if (pname) fprintf(stderr, "%s: ", pname);
  fprintf(stderr, "%s\n", msg);
  fflush(stderr);
}


static int report (ekto_State *L, int status) {
  if (status && !ekto_isnil(L, -1)) {
    const char *msg = ekto_tostring(L, -1);
    if (msg == NULL) msg = "(error object is not a string)";
    l_message(progname, msg);
    ekto_pop(L, 1);
  }
  return status;
}


static int traceback (ekto_State *L) {
  if (!ekto_isstring(L, 1))  /* 'message' not a string? */
    return 1;  /* keep it intact */
  ekto_getfield(L, EKTO_GLOBALSINDEX, "debug");
  if (!ekto_istable(L, -1)) {
    ekto_pop(L, 1);
    return 1;
  }
  ekto_getfield(L, -1, "traceback");
  if (!ekto_isfunction(L, -1)) {
    ekto_pop(L, 2);
    return 1;
  }
  ekto_pushvalue(L, 1);  /* pass error message */
  ekto_pushinteger(L, 2);  /* skip this function and traceback */
  ekto_call(L, 2, 1);  /* call debug.traceback */
  return 1;
}


static int docall (ekto_State *L, int narg, int clear) {
  int status;
  int base = ekto_gettop(L) - narg;  /* function index */
  ekto_pushcfunction(L, traceback);  /* push traceback function */
  ekto_insert(L, base);  /* put it under chunk and args */
  signal(SIGINT, laction);
  status = ekto_pcall(L, narg, (clear ? 0 : EKTO_MULTRET), base);
  signal(SIGINT, SIG_DFL);
  ekto_remove(L, base);  /* remove traceback function */
  /* force a complete garbage collection in case of errors */
  if (status != 0) ekto_gc(L, EKTO_GCCOLLECT, 0);
  return status;
}


static void print_version (void) {
  l_message(NULL, EKTO_RELEASE "  " EKTO_COPYRIGHT);
}


static int getargs (ekto_State *L, char **argv, int n) {
  int narg;
  int i;
  int argc = 0;
  while (argv[argc]) argc++;  /* count total number of arguments */
  narg = argc - (n + 1);  /* number of arguments to the script */
  ektoL_checkstack(L, narg + 3, "too many arguments to script");
  for (i=n+1; i < argc; i++)
    ekto_pushstring(L, argv[i]);
  ekto_createtable(L, narg, n + 1);
  for (i=0; i < argc; i++) {
    ekto_pushstring(L, argv[i]);
    ekto_rawseti(L, -2, i - n);
  }
  return narg;
}


static int dofile (ekto_State *L, const char *name) {
  int status = ektoL_loadfile(L, name) || docall(L, 0, 1);
  return report(L, status);
}


static int dostring (ekto_State *L, const char *s, const char *name) {
  int status = ektoL_loadbuffer(L, s, strlen(s), name) || docall(L, 0, 1);
  return report(L, status);
}


static int dolibrary (ekto_State *L, const char *name) {
  ekto_getglobal(L, "require");
  ekto_pushstring(L, name);
  return report(L, docall(L, 1, 1));
}


static const char *get_prompt (ekto_State *L, int firstline) {
  const char *p;
  ekto_getfield(L, EKTO_GLOBALSINDEX, firstline ? "_PROMPT" : "_PROMPT2");
  p = ekto_tostring(L, -1);
  if (p == NULL) p = (firstline ? EKTO_PROMPT : EKTO_PROMPT2);
  ekto_pop(L, 1);  /* remove global */
  return p;
}


static int incomplete (ekto_State *L, int status) {
  if (status == EKTO_ERRSYNTAX) {
    size_t lmsg;
    const char *msg = ekto_tolstring(L, -1, &lmsg);
    const char *tp = msg + lmsg - (sizeof(EKTO_QL("<eof>")) - 1);
    if (strstr(msg, EKTO_QL("<eof>")) == tp) {
      ekto_pop(L, 1);
      return 1;
    }
  }
  return 0;  /* else... */
}


static int pushline (ekto_State *L, int firstline) {
  char buffer[EKTO_MAXINPUT];
  char *b = buffer;
  size_t l;
  const char *prmt = get_prompt(L, firstline);
  if (ekto_readline(L, b, prmt) == 0)
    return 0;  /* no input */
  l = strlen(b);
  if (l > 0 && b[l-1] == '\n')  /* line ends with newline? */
    b[l-1] = '\0';  /* remove it */
  if (firstline && b[0] == '=')  /* first line starts with `=' ? */
    ekto_pushfstring(L, "return %s", b+1);  /* change it to `return' */
  else
    ekto_pushstring(L, b);
  ekto_freeline(L, b);
  return 1;
}


static int loadline (ekto_State *L) {
  int status;
  ekto_settop(L, 0);
  if (!pushline(L, 1))
    return -1;  /* no input */
  for (;;) {  /* repeat until gets a complete line */
    status = ektoL_loadbuffer(L, ekto_tostring(L, 1), ekto_strlen(L, 1), "=stdin");
    if (!incomplete(L, status)) break;  /* cannot try to add lines? */
    if (!pushline(L, 0))  /* no more input? */
      return -1;
    ekto_pushliteral(L, "\n");  /* add a new line... */
    ekto_insert(L, -2);  /* ...between the two lines */
    ekto_concat(L, 3);  /* join them */
  }
  ekto_saveline(L, 1);
  ekto_remove(L, 1);  /* remove line */
  return status;
}


static void dotty (ekto_State *L) {
  int status;
  const char *oldprogname = progname;
  progname = NULL;
  while ((status = loadline(L)) != -1) {
    if (status == 0) status = docall(L, 0, 0);
    report(L, status);
    if (status == 0 && ekto_gettop(L) > 0) {  /* any result to print? */
      ekto_getglobal(L, "print");
      ekto_insert(L, 1);
      if (ekto_pcall(L, ekto_gettop(L)-1, 0, 0) != 0)
        l_message(progname, ekto_pushfstring(L,
                               "error calling " EKTO_QL("print") " (%s)",
                               ekto_tostring(L, -1)));
    }
  }
  ekto_settop(L, 0);  /* clear stack */
  fputs("\n", stdout);
  fflush(stdout);
  progname = oldprogname;
}


static int handle_script (ekto_State *L, char **argv, int n) {
  int status;
  const char *fname;
  int narg = getargs(L, argv, n);  /* collect arguments */
  ekto_setglobal(L, "arg");
  fname = argv[n];
  if (strcmp(fname, "-") == 0 && strcmp(argv[n-1], "--") != 0) 
    fname = NULL;  /* stdin */
  status = ektoL_loadfile(L, fname);
  ekto_insert(L, -(narg+1));
  if (status == 0)
    status = docall(L, narg, 0);
  else
    ekto_pop(L, narg);      
  return report(L, status);
}


/* check that argument has no extra characters at the end */
#define notail(x)	{if ((x)[2] != '\0') return -1;}


static int collectargs (char **argv, int *pi, int *pv, int *pe) {
  int i;
  for (i = 1; argv[i] != NULL; i++) {
    if (argv[i][0] != '-')  /* not an option? */
        return i;
    switch (argv[i][1]) {  /* option */
      case '-':
        notail(argv[i]);
        return (argv[i+1] != NULL ? i+1 : 0);
      case '\0':
        return i;
      case 'i':
        notail(argv[i]);
        *pi = 1;  /* go through */
      case 'v':
        notail(argv[i]);
        *pv = 1;
        break;
      case 'e':
        *pe = 1;  /* go through */
      case 'l':
        if (argv[i][2] == '\0') {
          i++;
          if (argv[i] == NULL) return -1;
        }
        break;
      default: return -1;  /* invalid option */
    }
  }
  return 0;
}


static int runargs (ekto_State *L, char **argv, int n) {
  int i;
  for (i = 1; i < n; i++) {
    if (argv[i] == NULL) continue;
    ekto_assert(argv[i][0] == '-');
    switch (argv[i][1]) {  /* option */
      case 'e': {
        const char *chunk = argv[i] + 2;
        if (*chunk == '\0') chunk = argv[++i];
        ekto_assert(chunk != NULL);
        if (dostring(L, chunk, "=(command line)") != 0)
          return 1;
        break;
      }
      case 'l': {
        const char *filename = argv[i] + 2;
        if (*filename == '\0') filename = argv[++i];
        ekto_assert(filename != NULL);
        if (dolibrary(L, filename))
          return 1;  /* stop if file fails */
        break;
      }
      default: break;
    }
  }
  return 0;
}


static int handle_ektoinit (ekto_State *L) {
  const char *init = getenv(EKTO_INIT);
  if (init == NULL) return 0;  /* status OK */
  else if (init[0] == '@')
    return dofile(L, init+1);
  else
    return dostring(L, init, "=" EKTO_INIT);
}


struct Smain {
  int argc;
  char **argv;
  int status;
};


static int pmain (ekto_State *L) {
  struct Smain *s = (struct Smain *)ekto_touserdata(L, 1);
  char **argv = s->argv;
  int script;
  int has_i = 0, has_v = 0, has_e = 0;
  globalL = L;
  if (argv[0] && argv[0][0]) progname = argv[0];
  ekto_gc(L, EKTO_GCSTOP, 0);  /* stop collector during initialization */
  ektoL_openlibs(L);  /* open libraries */
  ekto_gc(L, EKTO_GCRESTART, 0);
  s->status = handle_ektoinit(L);
  if (s->status != 0) return 0;
  script = collectargs(argv, &has_i, &has_v, &has_e);
  if (script < 0) {  /* invalid args? */
    print_usage();
    s->status = 1;
    return 0;
  }
  if (has_v) print_version();
  s->status = runargs(L, argv, (script > 0) ? script : s->argc);
  if (s->status != 0) return 0;
  if (script)
    s->status = handle_script(L, argv, script);
  if (s->status != 0) return 0;
  if (has_i)
    dotty(L);
  else if (script == 0 && !has_e && !has_v) {
    if (ekto_stdin_is_tty()) {
      print_version();
      dotty(L);
    }
    else dofile(L, NULL);  /* executes stdin as a file */
  }
  return 0;
}


int main (int argc, char **argv) {
  int status;
  struct Smain s;
  ekto_State *L = ekto_open();  /* create state */
  if (L == NULL) {
    l_message(argv[0], "cannot create state: not enough memory");
    return EXIT_FAILURE;
  }
  s.argc = argc;
  s.argv = argv;
  status = ekto_cpcall(L, &pmain, &s);
  report(L, status);
  ekto_close(L);
  return (status || s.status) ? EXIT_FAILURE : EXIT_SUCCESS;
}

