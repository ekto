/*
** $Id: ldblib.c,v 1.104.1.3 2008/01/21 13:11:21 roberto Exp $
** Interface from Ekto to its debug API
** See Copyright Notice in ekto.h
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ldblib_c
#define EKTO_LIB

#include "Ekto.h"

#include "Ekto_Aux.h"
#include "Ekto_Lib.h"



static int db_getregistry (ekto_State *L) {
  ekto_pushvalue(L, EKTO_REGISTRYINDEX);
  return 1;
}


static int db_getmetatable (ekto_State *L) {
  ektoL_checkany(L, 1);
  if (!ekto_getmetatable(L, 1)) {
    ekto_pushnil(L);  /* no metatable */
  }
  return 1;
}


static int db_setmetatable (ekto_State *L) {
  int t = ekto_type(L, 2);
  ektoL_argcheck(L, t == EKTO_TNIL || t == EKTO_TTABLE, 2,
                    "nil or table expected");
  ekto_settop(L, 2);
  ekto_pushboolean(L, ekto_setmetatable(L, 1));
  return 1;
}


static int db_getfenv (ekto_State *L) {
  ekto_getfenv(L, 1);
  return 1;
}


static int db_setfenv (ekto_State *L) {
  ektoL_checktype(L, 2, EKTO_TTABLE);
  ekto_settop(L, 2);
  if (ekto_setfenv(L, 1) == 0)
    ektoL_error(L, EKTO_QL("setfenv")
                  " cannot change environment of given object");
  return 1;
}


static void settabss (ekto_State *L, const char *i, const char *v) {
  ekto_pushstring(L, v);
  ekto_setfield(L, -2, i);
}


static void settabsi (ekto_State *L, const char *i, int v) {
  ekto_pushinteger(L, v);
  ekto_setfield(L, -2, i);
}


static ekto_State *getthread (ekto_State *L, int *arg) {
  if (ekto_isthread(L, 1)) {
    *arg = 1;
    return ekto_tothread(L, 1);
  }
  else {
    *arg = 0;
    return L;
  }
}


static void treatstackoption (ekto_State *L, ekto_State *L1, const char *fname) {
  if (L == L1) {
    ekto_pushvalue(L, -2);
    ekto_remove(L, -3);
  }
  else
    ekto_xmove(L1, L, 1);
  ekto_setfield(L, -2, fname);
}


static int db_getinfo (ekto_State *L) {
  ekto_Debug ar;
  int arg;
  ekto_State *L1 = getthread(L, &arg);
  const char *options = ektoL_optstring(L, arg+2, "flnSu");
  if (ekto_isnumber(L, arg+1)) {
    if (!ekto_getstack(L1, (int)ekto_tointeger(L, arg+1), &ar)) {
      ekto_pushnil(L);  /* level out of range */
      return 1;
    }
  }
  else if (ekto_isfunction(L, arg+1)) {
    ekto_pushfstring(L, ">%s", options);
    options = ekto_tostring(L, -1);
    ekto_pushvalue(L, arg+1);
    ekto_xmove(L, L1, 1);
  }
  else
    return ektoL_argerror(L, arg+1, "function or level expected");
  if (!ekto_getinfo(L1, options, &ar))
    return ektoL_argerror(L, arg+2, "invalid option");
  ekto_createtable(L, 0, 2);
  if (strchr(options, 'S')) {
    settabss(L, "source", ar.source);
    settabss(L, "short_src", ar.short_src);
    settabsi(L, "linedefined", ar.linedefined);
    settabsi(L, "lastlinedefined", ar.lastlinedefined);
    settabss(L, "what", ar.what);
  }
  if (strchr(options, 'l'))
    settabsi(L, "currentline", ar.currentline);
  if (strchr(options, 'u'))
    settabsi(L, "nups", ar.nups);
  if (strchr(options, 'n')) {
    settabss(L, "name", ar.name);
    settabss(L, "namewhat", ar.namewhat);
  }
  if (strchr(options, 'L'))
    treatstackoption(L, L1, "activelines");
  if (strchr(options, 'f'))
    treatstackoption(L, L1, "func");
  return 1;  /* return table */
}
    

static int db_getlocal (ekto_State *L) {
  int arg;
  ekto_State *L1 = getthread(L, &arg);
  ekto_Debug ar;
  const char *name;
  if (!ekto_getstack(L1, ektoL_checkint(L, arg+1), &ar))  /* out of range? */
    return ektoL_argerror(L, arg+1, "level out of range");
  name = ekto_getlocal(L1, &ar, ektoL_checkint(L, arg+2));
  if (name) {
    ekto_xmove(L1, L, 1);
    ekto_pushstring(L, name);
    ekto_pushvalue(L, -2);
    return 2;
  }
  else {
    ekto_pushnil(L);
    return 1;
  }
}


static int db_setlocal (ekto_State *L) {
  int arg;
  ekto_State *L1 = getthread(L, &arg);
  ekto_Debug ar;
  if (!ekto_getstack(L1, ektoL_checkint(L, arg+1), &ar))  /* out of range? */
    return ektoL_argerror(L, arg+1, "level out of range");
  ektoL_checkany(L, arg+3);
  ekto_settop(L, arg+3);
  ekto_xmove(L, L1, 1);
  ekto_pushstring(L, ekto_setlocal(L1, &ar, ektoL_checkint(L, arg+2)));
  return 1;
}


static int auxupvalue (ekto_State *L, int get) {
  const char *name;
  int n = ektoL_checkint(L, 2);
  ektoL_checktype(L, 1, EKTO_TFUNCTION);
  if (ekto_iscfunction(L, 1)) return 0;  /* cannot touch C upvalues from Ekto */
  name = get ? ekto_getupvalue(L, 1, n) : ekto_setupvalue(L, 1, n);
  if (name == NULL) return 0;
  ekto_pushstring(L, name);
  ekto_insert(L, -(get+1));
  return get + 1;
}


static int db_getupvalue (ekto_State *L) {
  return auxupvalue(L, 1);
}


static int db_setupvalue (ekto_State *L) {
  ektoL_checkany(L, 3);
  return auxupvalue(L, 0);
}



static const char KEY_HOOK = 'h';


static void hookf (ekto_State *L, ekto_Debug *ar) {
  static const char *const hooknames[] =
    {"call", "return", "line", "count", "tail return"};
  ekto_pushlightuserdata(L, (void *)&KEY_HOOK);
  ekto_rawget(L, EKTO_REGISTRYINDEX);
  ekto_pushlightuserdata(L, L);
  ekto_rawget(L, -2);
  if (ekto_isfunction(L, -1)) {
    ekto_pushstring(L, hooknames[(int)ar->event]);
    if (ar->currentline >= 0)
      ekto_pushinteger(L, ar->currentline);
    else ekto_pushnil(L);
    ekto_assert(ekto_getinfo(L, "lS", ar));
    ekto_call(L, 2, 0);
  }
}


static int makemask (const char *smask, int count) {
  int mask = 0;
  if (strchr(smask, 'c')) mask |= EKTO_MASKCALL;
  if (strchr(smask, 'r')) mask |= EKTO_MASKRET;
  if (strchr(smask, 'l')) mask |= EKTO_MASKLINE;
  if (count > 0) mask |= EKTO_MASKCOUNT;
  return mask;
}


static char *unmakemask (int mask, char *smask) {
  int i = 0;
  if (mask & EKTO_MASKCALL) smask[i++] = 'c';
  if (mask & EKTO_MASKRET) smask[i++] = 'r';
  if (mask & EKTO_MASKLINE) smask[i++] = 'l';
  smask[i] = '\0';
  return smask;
}


static void gethooktable (ekto_State *L) {
  ekto_pushlightuserdata(L, (void *)&KEY_HOOK);
  ekto_rawget(L, EKTO_REGISTRYINDEX);
  if (!ekto_istable(L, -1)) {
    ekto_pop(L, 1);
    ekto_createtable(L, 0, 1);
    ekto_pushlightuserdata(L, (void *)&KEY_HOOK);
    ekto_pushvalue(L, -2);
    ekto_rawset(L, EKTO_REGISTRYINDEX);
  }
}


static int db_sethook (ekto_State *L) {
  int arg, mask, count;
  ekto_Hook func;
  ekto_State *L1 = getthread(L, &arg);
  if (ekto_isnoneornil(L, arg+1)) {
    ekto_settop(L, arg+1);
    func = NULL; mask = 0; count = 0;  /* turn off hooks */
  }
  else {
    const char *smask = ektoL_checkstring(L, arg+2);
    ektoL_checktype(L, arg+1, EKTO_TFUNCTION);
    count = ektoL_optint(L, arg+3, 0);
    func = hookf; mask = makemask(smask, count);
  }
  gethooktable(L);
  ekto_pushlightuserdata(L, L1);
  ekto_pushvalue(L, arg+1);
  ekto_rawset(L, -3);  /* set new hook */
  ekto_pop(L, 1);  /* remove hook table */
  ekto_sethook(L1, func, mask, count);  /* set hooks */
  return 0;
}


static int db_gethook (ekto_State *L) {
  int arg;
  ekto_State *L1 = getthread(L, &arg);
  char buff[5];
  int mask = ekto_gethookmask(L1);
  ekto_Hook hook = ekto_gethook(L1);
  if (hook != NULL && hook != hookf)  /* external hook? */
    ekto_pushliteral(L, "external hook");
  else {
    gethooktable(L);
    ekto_pushlightuserdata(L, L1);
    ekto_rawget(L, -2);   /* get hook */
    ekto_remove(L, -2);  /* remove hook table */
  }
  ekto_pushstring(L, unmakemask(mask, buff));
  ekto_pushinteger(L, ekto_gethookcount(L1));
  return 3;
}


static int db_debug (ekto_State *L) {
  for (;;) {
    char buffer[250];
    fputs("ekto_debug> ", stderr);
    if (fgets(buffer, sizeof(buffer), stdin) == 0 ||
        strcmp(buffer, "cont\n") == 0)
      return 0;
    if (ektoL_loadbuffer(L, buffer, strlen(buffer), "=(debug command)") ||
        ekto_pcall(L, 0, 0, 0)) {
      fputs(ekto_tostring(L, -1), stderr);
      fputs("\n", stderr);
    }
    ekto_settop(L, 0);  /* remove eventual returns */
  }
}


#define LEVELS1	12	/* size of the first part of the stack */
#define LEVELS2	10	/* size of the second part of the stack */

static int db_errorfb (ekto_State *L) {
  int level;
  int firstpart = 1;  /* still before eventual `...' */
  int arg;
  ekto_State *L1 = getthread(L, &arg);
  ekto_Debug ar;
  if (ekto_isnumber(L, arg+2)) {
    level = (int)ekto_tointeger(L, arg+2);
    ekto_pop(L, 1);
  }
  else
    level = (L == L1) ? 1 : 0;  /* level 0 may be this own function */
  if (ekto_gettop(L) == arg)
    ekto_pushliteral(L, "");
  else if (!ekto_isstring(L, arg+1)) return 1;  /* message is not a string */
  else ekto_pushliteral(L, "\n");
  ekto_pushliteral(L, "stack traceback:");
  while (ekto_getstack(L1, level++, &ar)) {
    if (level > LEVELS1 && firstpart) {
      /* no more than `LEVELS2' more levels? */
      if (!ekto_getstack(L1, level+LEVELS2, &ar))
        level--;  /* keep going */
      else {
        ekto_pushliteral(L, "\n\t...");  /* too many levels */
        while (ekto_getstack(L1, level+LEVELS2, &ar))  /* find last levels */
          level++;
      }
      firstpart = 0;
      continue;
    }
    ekto_pushliteral(L, "\n\t");
    ekto_getinfo(L1, "Snl", &ar);
    ekto_pushfstring(L, "%s:", ar.short_src);
    if (ar.currentline > 0)
      ekto_pushfstring(L, "%d:", ar.currentline);
    if (*ar.namewhat != '\0')  /* is there a name? */
        ekto_pushfstring(L, " in function " EKTO_QS, ar.name);
    else {
      if (*ar.what == 'm')  /* main? */
        ekto_pushfstring(L, " in main chunk");
      else if (*ar.what == 'C' || *ar.what == 't')
        ekto_pushliteral(L, " ?");  /* C function or tail call */
      else
        ekto_pushfstring(L, " in function <%s:%d>",
                           ar.short_src, ar.linedefined);
    }
    ekto_concat(L, ekto_gettop(L) - arg);
  }
  ekto_concat(L, ekto_gettop(L) - arg);
  return 1;
}


static const ektoL_Reg dblib[] = {
  {"debug", db_debug},
  {"getfenv", db_getfenv},
  {"gethook", db_gethook},
  {"getinfo", db_getinfo},
  {"getlocal", db_getlocal},
  {"getregistry", db_getregistry},
  {"getmetatable", db_getmetatable},
  {"getupvalue", db_getupvalue},
  {"setfenv", db_setfenv},
  {"sethook", db_sethook},
  {"setlocal", db_setlocal},
  {"setmetatable", db_setmetatable},
  {"setupvalue", db_setupvalue},
  {"traceback", db_errorfb},
  {NULL, NULL}
};


EKTOLIB_API int ektoopen_debug (ekto_State *L) {
  ektoL_register(L, EKTO_DBLIBNAME, dblib);
  return 1;
}

