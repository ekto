/*
** $Id: lapi.c,v 2.55.1.5 2008/07/04 18:41:18 roberto Exp $
** Ekto API
** See Copyright Notice in ekto.h
*/


#include <assert.h>
#include <math.h>
#include <stdarg.h>
#include <string.h>

#define lapi_c
#define EKTO_CORE

#include "Ekto.h"

#include "ekto_api.h"
#include "ekto_debug.h"
#include "ekto_do.h"
#include "ekto_func.h"
#include "ekto_gc.h"
#include "ekto_mem.h"
#include "ekto_object.h"
#include "ekto_state.h"
#include "ekto_string.h"
#include "ekto_table.h"
#include "ekto_tm.h"
#include "ekto_undump.h"
#include "ekto_vm.h"



const char ekto_ident[] =
  "$Ekto: " EKTO_RELEASE " " EKTO_COPYRIGHT " $\n"
  "$Authors: " EKTO_AUTHORS " $\n"
  "$URL: www.ekto.org $\n";



#define api_checknelems(L, n)	api_check(L, (n) <= (L->top - L->base))

#define api_checkvalidindex(L, i)	api_check(L, (i) != ektoO_nilobject)

#define api_incr_top(L)   {api_check(L, L->top < L->ci->top); L->top++;}



static TValue *index2adr (ekto_State *L, int idx) {
  if (idx > 0) {
    TValue *o = L->base + (idx - 1);
    api_check(L, idx <= L->ci->top - L->base);
    if (o >= L->top) return cast(TValue *, ektoO_nilobject);
    else return o;
  }
  else if (idx > EKTO_REGISTRYINDEX) {
    api_check(L, idx != 0 && -idx <= L->top - L->base);
    return L->top + idx;
  }
  else switch (idx) {  /* pseudo-indices */
    case EKTO_REGISTRYINDEX: return registry(L);
    case EKTO_ENVIRONINDEX: {
      Closure *func = curr_func(L);
      sethvalue(L, &L->env, func->c.env);
      return &L->env;
    }
    case EKTO_GLOBALSINDEX: return gt(L);
    default: {
      Closure *func = curr_func(L);
      idx = EKTO_GLOBALSINDEX - idx;
      return (idx <= func->c.nupvalues)
                ? &func->c.upvalue[idx-1]
                : cast(TValue *, ektoO_nilobject);
    }
  }
}


static Table *getcurrenv (ekto_State *L) {
  if (L->ci == L->base_ci)  /* no enclosing function? */
    return hvalue(gt(L));  /* use global table as environment */
  else {
    Closure *func = curr_func(L);
    return func->c.env;
  }
}


void ektoA_pushobject (ekto_State *L, const TValue *o) {
  setobj2s(L, L->top, o);
  api_incr_top(L);
}


EKTO_API int ekto_checkstack (ekto_State *L, int size) {
  int res = 1;
  ekto_lock(L);
  if (size > EKTOI_MAXCSTACK || (L->top - L->base + size) > EKTOI_MAXCSTACK)
    res = 0;  /* stack overflow */
  else if (size > 0) {
    ektoD_checkstack(L, size);
    if (L->ci->top < L->top + size)
      L->ci->top = L->top + size;
  }
  ekto_unlock(L);
  return res;
}


EKTO_API void ekto_xmove (ekto_State *from, ekto_State *to, int n) {
  int i;
  if (from == to) return;
  ekto_lock(to);
  api_checknelems(from, n);
  api_check(from, G(from) == G(to));
  api_check(from, to->ci->top - to->top >= n);
  from->top -= n;
  for (i = 0; i < n; i++) {
    setobj2s(to, to->top++, from->top + i);
  }
  ekto_unlock(to);
}


EKTO_API void ekto_setlevel (ekto_State *from, ekto_State *to) {
  to->nCcalls = from->nCcalls;
}


EKTO_API ekto_CFunction ekto_atpanic (ekto_State *L, ekto_CFunction panicf) {
  ekto_CFunction old;
  ekto_lock(L);
  old = G(L)->panic;
  G(L)->panic = panicf;
  ekto_unlock(L);
  return old;
}


EKTO_API ekto_State *ekto_newthread (ekto_State *L) {
  ekto_State *L1;
  ekto_lock(L);
  ektoC_checkGC(L);
  L1 = ektoE_newthread(L);
  setthvalue(L, L->top, L1);
  api_incr_top(L);
  ekto_unlock(L);
  ektoi_userstatethread(L, L1);
  return L1;
}



/*
** basic stack manipulation
*/


EKTO_API int ekto_gettop (ekto_State *L) {
  return cast_int(L->top - L->base);
}


EKTO_API void ekto_settop (ekto_State *L, int idx) {
  ekto_lock(L);
  if (idx >= 0) {
    api_check(L, idx <= L->stack_last - L->base);
    while (L->top < L->base + idx)
      setnilvalue(L->top++);
    L->top = L->base + idx;
  }
  else {
    api_check(L, -(idx+1) <= (L->top - L->base));
    L->top += idx+1;  /* `subtract' index (index is negative) */
  }
  ekto_unlock(L);
}


EKTO_API void ekto_remove (ekto_State *L, int idx) {
  StkId p;
  ekto_lock(L);
  p = index2adr(L, idx);
  api_checkvalidindex(L, p);
  while (++p < L->top) setobjs2s(L, p-1, p);
  L->top--;
  ekto_unlock(L);
}


EKTO_API void ekto_insert (ekto_State *L, int idx) {
  StkId p;
  StkId q;
  ekto_lock(L);
  p = index2adr(L, idx);
  api_checkvalidindex(L, p);
  for (q = L->top; q>p; q--) setobjs2s(L, q, q-1);
  setobjs2s(L, p, L->top);
  ekto_unlock(L);
}


EKTO_API void ekto_replace (ekto_State *L, int idx) {
  StkId o;
  ekto_lock(L);
  /* explicit test for incompatible code */
  if (idx == EKTO_ENVIRONINDEX && L->ci == L->base_ci)
    ektoG_runerror(L, "no calling environment");
  api_checknelems(L, 1);
  o = index2adr(L, idx);
  api_checkvalidindex(L, o);
  if (idx == EKTO_ENVIRONINDEX) {
    Closure *func = curr_func(L);
    api_check(L, ttistable(L->top - 1)); 
    func->c.env = hvalue(L->top - 1);
    ektoC_barrier(L, func, L->top - 1);
  }
  else {
    setobj(L, o, L->top - 1);
    if (idx < EKTO_GLOBALSINDEX)  /* function upvalue? */
      ektoC_barrier(L, curr_func(L), L->top - 1);
  }
  L->top--;
  ekto_unlock(L);
}


EKTO_API void ekto_pushvalue (ekto_State *L, int idx) {
  ekto_lock(L);
  setobj2s(L, L->top, index2adr(L, idx));
  api_incr_top(L);
  ekto_unlock(L);
}



/*
** access functions (stack -> C)
*/


EKTO_API int ekto_type (ekto_State *L, int idx) {
  StkId o = index2adr(L, idx);
  return (o == ektoO_nilobject) ? EKTO_TNONE : ttype(o);
}


EKTO_API const char *ekto_typename (ekto_State *L, int t) {
  UNUSED(L);
  return (t == EKTO_TNONE) ? "no value" : ektoT_typenames[t];
}


EKTO_API int ekto_iscfunction (ekto_State *L, int idx) {
  StkId o = index2adr(L, idx);
  return iscfunction(o);
}


EKTO_API int ekto_isnumber (ekto_State *L, int idx) {
  TValue n;
  const TValue *o = index2adr(L, idx);
  return tonumber(o, &n);
}


EKTO_API int ekto_isstring (ekto_State *L, int idx) {
  int t = ekto_type(L, idx);
  return (t == EKTO_TSTRING || t == EKTO_TNUMBER);
}


EKTO_API int ekto_isuserdata (ekto_State *L, int idx) {
  const TValue *o = index2adr(L, idx);
  return (ttisuserdata(o) || ttislightuserdata(o));
}


EKTO_API int ekto_rawequal (ekto_State *L, int index1, int index2) {
  StkId o1 = index2adr(L, index1);
  StkId o2 = index2adr(L, index2);
  return (o1 == ektoO_nilobject || o2 == ektoO_nilobject) ? 0
         : ektoO_rawequalObj(o1, o2);
}


EKTO_API int ekto_equal (ekto_State *L, int index1, int index2) {
  StkId o1, o2;
  int i;
  ekto_lock(L);  /* may call tag method */
  o1 = index2adr(L, index1);
  o2 = index2adr(L, index2);
  i = (o1 == ektoO_nilobject || o2 == ektoO_nilobject) ? 0 : equalobj(L, o1, o2);
  ekto_unlock(L);
  return i;
}


EKTO_API int ekto_lessthan (ekto_State *L, int index1, int index2) {
  StkId o1, o2;
  int i;
  ekto_lock(L);  /* may call tag method */
  o1 = index2adr(L, index1);
  o2 = index2adr(L, index2);
  i = (o1 == ektoO_nilobject || o2 == ektoO_nilobject) ? 0
       : ektoV_lessthan(L, o1, o2);
  ekto_unlock(L);
  return i;
}



EKTO_API ekto_Number ekto_tonumber (ekto_State *L, int idx) {
  TValue n;
  const TValue *o = index2adr(L, idx);
  if (tonumber(o, &n))
    return nvalue(o);
  else
    return 0;
}


EKTO_API ekto_Integer ekto_tointeger (ekto_State *L, int idx) {
  TValue n;
  const TValue *o = index2adr(L, idx);
  if (tonumber(o, &n)) {
    ekto_Integer res;
    ekto_Number num = nvalue(o);
    ekto_number2integer(res, num);
    return res;
  }
  else
    return 0;
}


EKTO_API int ekto_toboolean (ekto_State *L, int idx) {
  const TValue *o = index2adr(L, idx);
  return !l_isfalse(o);
}


EKTO_API const char *ekto_tolstring (ekto_State *L, int idx, size_t *len) {
  StkId o = index2adr(L, idx);
  if (!ttisstring(o)) {
    ekto_lock(L);  /* `ektoV_tostring' may create a new string */
    if (!ektoV_tostring(L, o)) {  /* conversion failed? */
      if (len != NULL) *len = 0;
      ekto_unlock(L);
      return NULL;
    }
    ektoC_checkGC(L);
    o = index2adr(L, idx);  /* previous call may reallocate the stack */
    ekto_unlock(L);
  }
  if (len != NULL) *len = tsvalue(o)->len;
  return svalue(o);
}


EKTO_API size_t ekto_objlen (ekto_State *L, int idx) {
  StkId o = index2adr(L, idx);
  switch (ttype(o)) {
    case EKTO_TSTRING: return tsvalue(o)->len;
    case EKTO_TUSERDATA: return uvalue(o)->len;
    case EKTO_TTABLE: return ektoH_getn(hvalue(o));
    case EKTO_TNUMBER: {
      size_t l;
      ekto_lock(L);  /* `ektoV_tostring' may create a new string */
      l = (ektoV_tostring(L, o) ? tsvalue(o)->len : 0);
      ekto_unlock(L);
      return l;
    }
    default: return 0;
  }
}


EKTO_API ekto_CFunction ekto_tocfunction (ekto_State *L, int idx) {
  StkId o = index2adr(L, idx);
  return (!iscfunction(o)) ? NULL : clvalue(o)->c.f;
}


EKTO_API void *ekto_touserdata (ekto_State *L, int idx) {
  StkId o = index2adr(L, idx);
  switch (ttype(o)) {
    case EKTO_TUSERDATA: return (rawuvalue(o) + 1);
    case EKTO_TLIGHTUSERDATA: return pvalue(o);
    default: return NULL;
  }
}


EKTO_API ekto_State *ekto_tothread (ekto_State *L, int idx) {
  StkId o = index2adr(L, idx);
  return (!ttisthread(o)) ? NULL : thvalue(o);
}


EKTO_API const void *ekto_topointer (ekto_State *L, int idx) {
  StkId o = index2adr(L, idx);
  switch (ttype(o)) {
    case EKTO_TTABLE: return hvalue(o);
    case EKTO_TFUNCTION: return clvalue(o);
    case EKTO_TTHREAD: return thvalue(o);
    case EKTO_TUSERDATA:
    case EKTO_TLIGHTUSERDATA:
      return ekto_touserdata(L, idx);
    default: return NULL;
  }
}



/*
** push functions (C -> stack)
*/


EKTO_API void ekto_pushnil (ekto_State *L) {
  ekto_lock(L);
  setnilvalue(L->top);
  api_incr_top(L);
  ekto_unlock(L);
}


EKTO_API void ekto_pushnumber (ekto_State *L, ekto_Number n) {
  ekto_lock(L);
  setnvalue(L->top, n);
  api_incr_top(L);
  ekto_unlock(L);
}


EKTO_API void ekto_pushinteger (ekto_State *L, ekto_Integer n) {
  ekto_lock(L);
  setnvalue(L->top, cast_num(n));
  api_incr_top(L);
  ekto_unlock(L);
}


EKTO_API void ekto_pushlstring (ekto_State *L, const char *s, size_t len) {
  ekto_lock(L);
  ektoC_checkGC(L);
  setsvalue2s(L, L->top, ektoS_newlstr(L, s, len));
  api_incr_top(L);
  ekto_unlock(L);
}


EKTO_API void ekto_pushstring (ekto_State *L, const char *s) {
  if (s == NULL)
    ekto_pushnil(L);
  else
    ekto_pushlstring(L, s, strlen(s));
}


EKTO_API const char *ekto_pushvfstring (ekto_State *L, const char *fmt,
                                      va_list argp) {
  const char *ret;
  ekto_lock(L);
  ektoC_checkGC(L);
  ret = ektoO_pushvfstring(L, fmt, argp);
  ekto_unlock(L);
  return ret;
}


EKTO_API const char *ekto_pushfstring (ekto_State *L, const char *fmt, ...) {
  const char *ret;
  va_list argp;
  ekto_lock(L);
  ektoC_checkGC(L);
  va_start(argp, fmt);
  ret = ektoO_pushvfstring(L, fmt, argp);
  va_end(argp);
  ekto_unlock(L);
  return ret;
}


EKTO_API void ekto_pushcclosure (ekto_State *L, ekto_CFunction fn, int n) {
  Closure *cl;
  ekto_lock(L);
  ektoC_checkGC(L);
  api_checknelems(L, n);
  cl = ektoF_newCclosure(L, n, getcurrenv(L));
  cl->c.f = fn;
  L->top -= n;
  while (n--)
    setobj2n(L, &cl->c.upvalue[n], L->top+n);
  setclvalue(L, L->top, cl);
  ekto_assert(iswhite(obj2gco(cl)));
  api_incr_top(L);
  ekto_unlock(L);
}


EKTO_API void ekto_pushboolean (ekto_State *L, int b) {
  ekto_lock(L);
  setbvalue(L->top, (b != 0));  /* ensure that true is 1 */
  api_incr_top(L);
  ekto_unlock(L);
}


EKTO_API void ekto_pushlightuserdata (ekto_State *L, void *p) {
  ekto_lock(L);
  setpvalue(L->top, p);
  api_incr_top(L);
  ekto_unlock(L);
}


EKTO_API int ekto_pushthread (ekto_State *L) {
  ekto_lock(L);
  setthvalue(L, L->top, L);
  api_incr_top(L);
  ekto_unlock(L);
  return (G(L)->mainthread == L);
}



/*
** get functions (Ekto -> stack)
*/


EKTO_API void ekto_gettable (ekto_State *L, int idx) {
  StkId t;
  ekto_lock(L);
  t = index2adr(L, idx);
  api_checkvalidindex(L, t);
  ektoV_gettable(L, t, L->top - 1, L->top - 1);
  ekto_unlock(L);
}


EKTO_API void ekto_getfield (ekto_State *L, int idx, const char *k) {
  StkId t;
  TValue key;
  ekto_lock(L);
  t = index2adr(L, idx);
  api_checkvalidindex(L, t);
  setsvalue(L, &key, ektoS_new(L, k));
  ektoV_gettable(L, t, &key, L->top);
  api_incr_top(L);
  ekto_unlock(L);
}


EKTO_API void ekto_rawget (ekto_State *L, int idx) {
  StkId t;
  ekto_lock(L);
  t = index2adr(L, idx);
  api_check(L, ttistable(t));
  setobj2s(L, L->top - 1, ektoH_get(hvalue(t), L->top - 1));
  ekto_unlock(L);
}


EKTO_API void ekto_rawgeti (ekto_State *L, int idx, int n) {
  StkId o;
  ekto_lock(L);
  o = index2adr(L, idx);
  api_check(L, ttistable(o));
  setobj2s(L, L->top, ektoH_getnum(hvalue(o), n));
  api_incr_top(L);
  ekto_unlock(L);
}


EKTO_API void ekto_createtable (ekto_State *L, int narray, int nrec) {
  ekto_lock(L);
  ektoC_checkGC(L);
  sethvalue(L, L->top, ektoH_new(L, narray, nrec));
  api_incr_top(L);
  ekto_unlock(L);
}


EKTO_API int ekto_getmetatable (ekto_State *L, int objindex) {
  const TValue *obj;
  Table *mt = NULL;
  int res;
  ekto_lock(L);
  obj = index2adr(L, objindex);
  switch (ttype(obj)) {
    case EKTO_TTABLE:
      mt = hvalue(obj)->metatable;
      break;
    case EKTO_TUSERDATA:
      mt = uvalue(obj)->metatable;
      break;
    default:
      mt = G(L)->mt[ttype(obj)];
      break;
  }
  if (mt == NULL)
    res = 0;
  else {
    sethvalue(L, L->top, mt);
    api_incr_top(L);
    res = 1;
  }
  ekto_unlock(L);
  return res;
}


EKTO_API void ekto_getfenv (ekto_State *L, int idx) {
  StkId o;
  ekto_lock(L);
  o = index2adr(L, idx);
  api_checkvalidindex(L, o);
  switch (ttype(o)) {
    case EKTO_TFUNCTION:
      sethvalue(L, L->top, clvalue(o)->c.env);
      break;
    case EKTO_TUSERDATA:
      sethvalue(L, L->top, uvalue(o)->env);
      break;
    case EKTO_TTHREAD:
      setobj2s(L, L->top,  gt(thvalue(o)));
      break;
    default:
      setnilvalue(L->top);
      break;
  }
  api_incr_top(L);
  ekto_unlock(L);
}


/*
** set functions (stack -> Ekto)
*/


EKTO_API void ekto_settable (ekto_State *L, int idx) {
  StkId t;
  ekto_lock(L);
  api_checknelems(L, 2);
  t = index2adr(L, idx);
  api_checkvalidindex(L, t);
  ektoV_settable(L, t, L->top - 2, L->top - 1);
  L->top -= 2;  /* pop index and value */
  ekto_unlock(L);
}


EKTO_API void ekto_setfield (ekto_State *L, int idx, const char *k) {
  StkId t;
  TValue key;
  ekto_lock(L);
  api_checknelems(L, 1);
  t = index2adr(L, idx);
  api_checkvalidindex(L, t);
  setsvalue(L, &key, ektoS_new(L, k));
  ektoV_settable(L, t, &key, L->top - 1);
  L->top--;  /* pop value */
  ekto_unlock(L);
}


EKTO_API void ekto_rawset (ekto_State *L, int idx) {
  StkId t;
  ekto_lock(L);
  api_checknelems(L, 2);
  t = index2adr(L, idx);
  api_check(L, ttistable(t));
  setobj2t(L, ektoH_set(L, hvalue(t), L->top-2), L->top-1);
  ektoC_barriert(L, hvalue(t), L->top-1);
  L->top -= 2;
  ekto_unlock(L);
}


EKTO_API void ekto_rawseti (ekto_State *L, int idx, int n) {
  StkId o;
  ekto_lock(L);
  api_checknelems(L, 1);
  o = index2adr(L, idx);
  api_check(L, ttistable(o));
  setobj2t(L, ektoH_setnum(L, hvalue(o), n), L->top-1);
  ektoC_barriert(L, hvalue(o), L->top-1);
  L->top--;
  ekto_unlock(L);
}


EKTO_API int ekto_setmetatable (ekto_State *L, int objindex) {
  TValue *obj;
  Table *mt;
  ekto_lock(L);
  api_checknelems(L, 1);
  obj = index2adr(L, objindex);
  api_checkvalidindex(L, obj);
  if (ttisnil(L->top - 1))
    mt = NULL;
  else {
    api_check(L, ttistable(L->top - 1));
    mt = hvalue(L->top - 1);
  }
  switch (ttype(obj)) {
    case EKTO_TTABLE: {
      hvalue(obj)->metatable = mt;
      if (mt)
        ektoC_objbarriert(L, hvalue(obj), mt);
      break;
    }
    case EKTO_TUSERDATA: {
      uvalue(obj)->metatable = mt;
      if (mt)
        ektoC_objbarrier(L, rawuvalue(obj), mt);
      break;
    }
    default: {
      G(L)->mt[ttype(obj)] = mt;
      break;
    }
  }
  L->top--;
  ekto_unlock(L);
  return 1;
}


EKTO_API int ekto_setfenv (ekto_State *L, int idx) {
  StkId o;
  int res = 1;
  ekto_lock(L);
  api_checknelems(L, 1);
  o = index2adr(L, idx);
  api_checkvalidindex(L, o);
  api_check(L, ttistable(L->top - 1));
  switch (ttype(o)) {
    case EKTO_TFUNCTION:
      clvalue(o)->c.env = hvalue(L->top - 1);
      break;
    case EKTO_TUSERDATA:
      uvalue(o)->env = hvalue(L->top - 1);
      break;
    case EKTO_TTHREAD:
      sethvalue(L, gt(thvalue(o)), hvalue(L->top - 1));
      break;
    default:
      res = 0;
      break;
  }
  if (res) ektoC_objbarrier(L, gcvalue(o), hvalue(L->top - 1));
  L->top--;
  ekto_unlock(L);
  return res;
}


/*
** `load' and `call' functions (run Ekto code)
*/


#define adjustresults(L,nres) \
    { if (nres == EKTO_MULTRET && L->top >= L->ci->top) L->ci->top = L->top; }


#define checkresults(L,na,nr) \
     api_check(L, (nr) == EKTO_MULTRET || (L->ci->top - L->top >= (nr) - (na)))
	

EKTO_API void ekto_call (ekto_State *L, int nargs, int nresults) {
  StkId func;
  ekto_lock(L);
  api_checknelems(L, nargs+1);
  checkresults(L, nargs, nresults);
  func = L->top - (nargs+1);
  ektoD_call(L, func, nresults);
  adjustresults(L, nresults);
  ekto_unlock(L);
}



/*
** Execute a protected call.
*/
struct CallS {  /* data to `f_call' */
  StkId func;
  int nresults;
};


static void f_call (ekto_State *L, void *ud) {
  struct CallS *c = cast(struct CallS *, ud);
  ektoD_call(L, c->func, c->nresults);
}



EKTO_API int ekto_pcall (ekto_State *L, int nargs, int nresults, int errfunc) {
  struct CallS c;
  int status;
  ptrdiff_t func;
  ekto_lock(L);
  api_checknelems(L, nargs+1);
  checkresults(L, nargs, nresults);
  if (errfunc == 0)
    func = 0;
  else {
    StkId o = index2adr(L, errfunc);
    api_checkvalidindex(L, o);
    func = savestack(L, o);
  }
  c.func = L->top - (nargs+1);  /* function to be called */
  c.nresults = nresults;
  status = ektoD_pcall(L, f_call, &c, savestack(L, c.func), func);
  adjustresults(L, nresults);
  ekto_unlock(L);
  return status;
}


/*
** Execute a protected C call.
*/
struct CCallS {  /* data to `f_Ccall' */
  ekto_CFunction func;
  void *ud;
};


static void f_Ccall (ekto_State *L, void *ud) {
  struct CCallS *c = cast(struct CCallS *, ud);
  Closure *cl;
  cl = ektoF_newCclosure(L, 0, getcurrenv(L));
  cl->c.f = c->func;
  setclvalue(L, L->top, cl);  /* push function */
  api_incr_top(L);
  setpvalue(L->top, c->ud);  /* push only argument */
  api_incr_top(L);
  ektoD_call(L, L->top - 2, 0);
}


EKTO_API int ekto_cpcall (ekto_State *L, ekto_CFunction func, void *ud) {
  struct CCallS c;
  int status;
  ekto_lock(L);
  c.func = func;
  c.ud = ud;
  status = ektoD_pcall(L, f_Ccall, &c, savestack(L, L->top), 0);
  ekto_unlock(L);
  return status;
}


EKTO_API int ekto_load (ekto_State *L, ekto_Reader reader, void *data,
                      const char *chunkname) {
  ZIO z;
  int status;
  ekto_lock(L);
  if (!chunkname) chunkname = "?";
  ektoZ_init(L, &z, reader, data);
  status = ektoD_protectedparser(L, &z, chunkname);
  ekto_unlock(L);
  return status;
}


EKTO_API int ekto_dump (ekto_State *L, ekto_Writer writer, void *data) {
  int status;
  TValue *o;
  ekto_lock(L);
  api_checknelems(L, 1);
  o = L->top - 1;
  if (isLfunction(o))
    status = ektoU_dump(L, clvalue(o)->l.p, writer, data, 0);
  else
    status = 1;
  ekto_unlock(L);
  return status;
}


EKTO_API int  ekto_status (ekto_State *L) {
  return L->status;
}


/*
** Garbage-collection function
*/

EKTO_API int ekto_gc (ekto_State *L, int what, int data) {
  int res = 0;
  global_State *g;
  ekto_lock(L);
  g = G(L);
  switch (what) {
    case EKTO_GCSTOP: {
      g->GCthreshold = MAX_LUMEM;
      break;
    }
    case EKTO_GCRESTART: {
      g->GCthreshold = g->totalbytes;
      break;
    }
    case EKTO_GCCOLLECT: {
      ektoC_fullgc(L);
      break;
    }
    case EKTO_GCCOUNT: {
      /* GC values are expressed in Kbytes: #bytes/2^10 */
      res = cast_int(g->totalbytes >> 10);
      break;
    }
    case EKTO_GCCOUNTB: {
      res = cast_int(g->totalbytes & 0x3ff);
      break;
    }
    case EKTO_GCSTEP: {
      lu_mem a = (cast(lu_mem, data) << 10);
      if (a <= g->totalbytes)
        g->GCthreshold = g->totalbytes - a;
      else
        g->GCthreshold = 0;
      while (g->GCthreshold <= g->totalbytes) {
        ektoC_step(L);
        if (g->gcstate == GCSpause) {  /* end of cycle? */
          res = 1;  /* signal it */
          break;
        }
      }
      break;
    }
    case EKTO_GCSETPAUSE: {
      res = g->gcpause;
      g->gcpause = data;
      break;
    }
    case EKTO_GCSETSTEPMUL: {
      res = g->gcstepmul;
      g->gcstepmul = data;
      break;
    }
    default: res = -1;  /* invalid option */
  }
  ekto_unlock(L);
  return res;
}



/*
** miscellaneous functions
*/


EKTO_API int ekto_error (ekto_State *L) {
  ekto_lock(L);
  api_checknelems(L, 1);
  ektoG_errormsg(L);
  ekto_unlock(L);
  return 0;  /* to avoid warnings */
}


EKTO_API int ekto_next (ekto_State *L, int idx) {
  StkId t;
  int more;
  ekto_lock(L);
  t = index2adr(L, idx);
  api_check(L, ttistable(t));
  more = ektoH_next(L, hvalue(t), L->top - 1);
  if (more) {
    api_incr_top(L);
  }
  else  /* no more elements */
    L->top -= 1;  /* remove key */
  ekto_unlock(L);
  return more;
}


EKTO_API void ekto_concat (ekto_State *L, int n) {
  ekto_lock(L);
  api_checknelems(L, n);
  if (n >= 2) {
    ektoC_checkGC(L);
    ektoV_concat(L, n, cast_int(L->top - L->base) - 1);
    L->top -= (n-1);
  }
  else if (n == 0) {  /* push empty string */
    setsvalue2s(L, L->top, ektoS_newlstr(L, "", 0));
    api_incr_top(L);
  }
  /* else n == 1; nothing to do */
  ekto_unlock(L);
}


EKTO_API ekto_Alloc ekto_getallocf (ekto_State *L, void **ud) {
  ekto_Alloc f;
  ekto_lock(L);
  if (ud) *ud = G(L)->ud;
  f = G(L)->frealloc;
  ekto_unlock(L);
  return f;
}


EKTO_API void ekto_setallocf (ekto_State *L, ekto_Alloc f, void *ud) {
  ekto_lock(L);
  G(L)->ud = ud;
  G(L)->frealloc = f;
  ekto_unlock(L);
}


EKTO_API void *ekto_newuserdata (ekto_State *L, size_t size) {
  Udata *u;
  ekto_lock(L);
  ektoC_checkGC(L);
  u = ektoS_newudata(L, size, getcurrenv(L));
  setuvalue(L, L->top, u);
  api_incr_top(L);
  ekto_unlock(L);
  return u + 1;
}




static const char *aux_upvalue (StkId fi, int n, TValue **val) {
  Closure *f;
  if (!ttisfunction(fi)) return NULL;
  f = clvalue(fi);
  if (f->c.isC) {
    if (!(1 <= n && n <= f->c.nupvalues)) return NULL;
    *val = &f->c.upvalue[n-1];
    return "";
  }
  else {
    Proto *p = f->l.p;
    if (!(1 <= n && n <= p->sizeupvalues)) return NULL;
    *val = f->l.upvals[n-1]->v;
    return getstr(p->upvalues[n-1]);
  }
}


EKTO_API const char *ekto_getupvalue (ekto_State *L, int funcindex, int n) {
  const char *name;
  TValue *val;
  ekto_lock(L);
  name = aux_upvalue(index2adr(L, funcindex), n, &val);
  if (name) {
    setobj2s(L, L->top, val);
    api_incr_top(L);
  }
  ekto_unlock(L);
  return name;
}


EKTO_API const char *ekto_setupvalue (ekto_State *L, int funcindex, int n) {
  const char *name;
  TValue *val;
  StkId fi;
  ekto_lock(L);
  fi = index2adr(L, funcindex);
  api_checknelems(L, 1);
  name = aux_upvalue(fi, n, &val);
  if (name) {
    L->top--;
    setobj(L, val, L->top);
    ektoC_barrier(L, clvalue(fi), L->top);
  }
  ekto_unlock(L);
  return name;
}

