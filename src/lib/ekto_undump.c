/*
** $Id: lundump.c,v 2.7.1.4 2008/04/04 19:51:41 roberto Exp $
** load precompiled Ekto chunks
** See Copyright Notice in ekto.h
*/

#include <string.h>

#define lundump_c
#define EKTO_CORE

#include "Ekto.h"

#include "ekto_debug.h"
#include "ekto_do.h"
#include "ekto_func.h"
#include "ekto_mem.h"
#include "ekto_object.h"
#include "ekto_string.h"
#include "ekto_undump.h"
#include "ekto_zio.h"

typedef struct {
 ekto_State* L;
 ZIO* Z;
 Mbuffer* b;
 const char* name;
} LoadState;

#ifdef EKTOC_TRUST_BINARIES
#define IF(c,s)
#define error(S,s)
#else
#define IF(c,s)		if (c) error(S,s)

static void error(LoadState* S, const char* why)
{
 ektoO_pushfstring(S->L,"%s: %s in precompiled chunk",S->name,why);
 ektoD_throw(S->L,EKTO_ERRSYNTAX);
}
#endif

#define LoadMem(S,b,n,size)	LoadBlock(S,b,(n)*(size))
#define	LoadByte(S)		(lu_byte)LoadChar(S)
#define LoadVar(S,x)		LoadMem(S,&x,1,sizeof(x))
#define LoadVector(S,b,n,size)	LoadMem(S,b,n,size)

static void LoadBlock(LoadState* S, void* b, uint32_t size)
{
 uint32_t r=ektoZ_read(S->Z,b,size);
 IF (r!=0, "unexpected end");
}

static int LoadChar(LoadState* S)
{
 char x;
 LoadVar(S,x);
 return x;
}

static int LoadInt(LoadState* S)
{
 int x;
 LoadVar(S,x);
 uint32_t *y = &x;
#ifdef WORDS_BIGENDIAN
 uint32_t z = bswap_32 (*y);
#else
 uint32_t z = *y;
#endif
 IF (z<0, "bad integer");
 return *(&z);
}

static ekto_Number LoadNumber(LoadState* S)
{
 ekto_Number x;
 LoadVar(S,x);
 uint64_t *y = &x;
#ifdef WORDS_BIGENDIAN
 uint64_t z = bswap_64 (*y);
#else
 uint64_t z = *y;
#endif
 return *(&z);
}

static TString* LoadString(LoadState* S)
{
 uint32_t size;
 LoadVar(S,size);
 if (size==0)
  return NULL;
 else
 {
  char* s=ektoZ_openspace(S->L,S->b,size);
  LoadBlock(S,s,size);
  return ektoS_newlstr(S->L,s,size-1);		/* remove trailing '\0' */
 }
}

static void LoadCode(LoadState* S, Proto* f)
{
 int n=LoadInt(S);
 f->code=ektoM_newvector(S->L,n,Instruction);
 f->sizecode=n;
 LoadVector(S,f->code,n,sizeof(Instruction));
}

static Proto* LoadFunction(LoadState* S, TString* p);

static void LoadConstants(LoadState* S, Proto* f)
{
 int i,n;
 n=LoadInt(S);
 f->k=ektoM_newvector(S->L,n,TValue);
 f->sizek=n;
 for (i=0; i<n; i++) setnilvalue(&f->k[i]);
 for (i=0; i<n; i++)
 {
  TValue* o=&f->k[i];
  int t=LoadChar(S);
  switch (t)
  {
   case EKTO_TNIL:
   	setnilvalue(o);
	break;
   case EKTO_TBOOLEAN:
   	setbvalue(o,LoadChar(S)!=0);
	break;
   case EKTO_TNUMBER:
	setnvalue(o,LoadNumber(S));
	break;
   case EKTO_TSTRING:
	setsvalue2n(S->L,o,LoadString(S));
	break;
   default:
	error(S,"bad constant");
	break;
  }
 }
 n=LoadInt(S);
 f->p=ektoM_newvector(S->L,n,Proto*);
 f->sizep=n;
 for (i=0; i<n; i++) f->p[i]=NULL;
 for (i=0; i<n; i++) f->p[i]=LoadFunction(S,f->source);
}

static void LoadDebug(LoadState* S, Proto* f)
{
 int i,n;
 n=LoadInt(S);
 f->lineinfo=ektoM_newvector(S->L,n,int);
 f->sizelineinfo=n;
 LoadVector(S,f->lineinfo,n,sizeof(int));
 n=LoadInt(S);
 f->locvars=ektoM_newvector(S->L,n,LocVar);
 f->sizelocvars=n;
 for (i=0; i<n; i++) f->locvars[i].varname=NULL;
 for (i=0; i<n; i++)
 {
  f->locvars[i].varname=LoadString(S);
  f->locvars[i].startpc=LoadInt(S);
  f->locvars[i].endpc=LoadInt(S);
 }
 n=LoadInt(S);
 f->upvalues=ektoM_newvector(S->L,n,TString*);
 f->sizeupvalues=n;
 for (i=0; i<n; i++) f->upvalues[i]=NULL;
 for (i=0; i<n; i++) f->upvalues[i]=LoadString(S);
}

static Proto* LoadFunction(LoadState* S, TString* p)
{
 Proto* f;
 if (++S->L->nCcalls > EKTOI_MAXCCALLS) error(S,"code too deep");
 f=ektoF_newproto(S->L);
 setptvalue2s(S->L,S->L->top,f); incr_top(S->L);
 f->source=LoadString(S); if (f->source==NULL) f->source=p;
 f->linedefined=LoadInt(S);
 f->lastlinedefined=LoadInt(S);
 f->nups=LoadByte(S);
 f->numparams=LoadByte(S);
 f->is_vararg=LoadByte(S);
 f->maxstacksize=LoadByte(S);
 LoadCode(S,f);
 LoadConstants(S,f);
 LoadDebug(S,f);
 IF (!ektoG_checkcode(f), "bad code");
 S->L->top--;
 S->L->nCcalls--;
 return f;
}

static void LoadHeader(LoadState* S)
{
 char h[EKTOC_HEADERSIZE];
 char s[EKTOC_HEADERSIZE];
 ektoU_header(h);
 LoadBlock(S,s,EKTOC_HEADERSIZE);
 IF (memcmp(h,s,EKTOC_HEADERSIZE)!=0, "bad header");
}

/*
** load precompiled chunk
*/
Proto* ektoU_undump (ekto_State* L, ZIO* Z, Mbuffer* buff, const char* name)
{
 LoadState S;
 if (*name=='@' || *name=='=')
  S.name=name+1;
 else if (*name==EKTO_SIGNATURE[0])
  S.name="binary string";
 else
  S.name=name;
 S.L=L;
 S.Z=Z;
 S.b=buff;
 LoadHeader(&S);
 return LoadFunction(&S,ektoS_newliteral(L,"=?"));
}

/*
* make header
*/
void ektoU_header (char* h)
{
 int x=0;
 memcpy(h,EKTO_SIGNATURE,sizeof(EKTO_SIGNATURE)-1);
 h+=sizeof(EKTO_SIGNATURE)-1;
 *h++=(char)EKTOC_VERSION;
 *h++=(char)EKTOC_FORMAT;
 *h++=(char)*(char*)&x;				/* endianness */
 *h++=(char)sizeof(int);
 *h++=(char)sizeof(uint32_t);
 *h++=(char)sizeof(Instruction);
 *h++=(char)sizeof(ekto_Number);
 *h++=(char)(((ekto_Number)0.5)==0);		/* is ekto_Number integral? */
}
