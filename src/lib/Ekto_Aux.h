/*
** $Id: lauxlib.h,v 1.88.1.1 2007/12/27 13:02:25 roberto Exp $
** Auxiliary functions for building Ekto libraries
** See Copyright Notice in ekto.h
*/


#ifndef lauxlib_h
#define lauxlib_h


#include <stddef.h>
#include <stdio.h>

#include "Ekto.h"


#if defined(EKTO_COMPAT_GETN)
EKTOLIB_API int (ektoL_getn) (ekto_State *L, int t);
EKTOLIB_API void (ektoL_setn) (ekto_State *L, int t, int n);
#else
#define ektoL_getn(L,i)          ((int)ekto_objlen(L, i))
#define ektoL_setn(L,i,j)        ((void)0)  /* no op! */
#endif

#if defined(EKTO_COMPAT_OPENLIB)
#define ektoI_openlib	ektoL_openlib
#endif


/* extra error code for `ektoL_load' */
#define EKTO_ERRFILE     (EKTO_ERRERR+1)


typedef struct ektoL_Reg {
  const char *name;
  ekto_CFunction func;
} ektoL_Reg;



EKTOLIB_API void (ektoI_openlib) (ekto_State *L, const char *libname,
                                const ektoL_Reg *l, int nup);
EKTOLIB_API void (ektoL_register) (ekto_State *L, const char *libname,
                                const ektoL_Reg *l);
EKTOLIB_API int (ektoL_getmetafield) (ekto_State *L, int obj, const char *e);
EKTOLIB_API int (ektoL_callmeta) (ekto_State *L, int obj, const char *e);
EKTOLIB_API int (ektoL_typerror) (ekto_State *L, int narg, const char *tname);
EKTOLIB_API int (ektoL_argerror) (ekto_State *L, int numarg, const char *extramsg);
EKTOLIB_API const char *(ektoL_checklstring) (ekto_State *L, int numArg,
                                                          size_t *l);
EKTOLIB_API const char *(ektoL_optlstring) (ekto_State *L, int numArg,
                                          const char *def, size_t *l);
EKTOLIB_API ekto_Number (ektoL_checknumber) (ekto_State *L, int numArg);
EKTOLIB_API ekto_Number (ektoL_optnumber) (ekto_State *L, int nArg, ekto_Number def);

EKTOLIB_API ekto_Integer (ektoL_checkinteger) (ekto_State *L, int numArg);
EKTOLIB_API ekto_Integer (ektoL_optinteger) (ekto_State *L, int nArg,
                                          ekto_Integer def);

EKTOLIB_API void (ektoL_checkstack) (ekto_State *L, int sz, const char *msg);
EKTOLIB_API void (ektoL_checktype) (ekto_State *L, int narg, int t);
EKTOLIB_API void (ektoL_checkany) (ekto_State *L, int narg);

EKTOLIB_API int   (ektoL_newmetatable) (ekto_State *L, const char *tname);
EKTOLIB_API void *(ektoL_checkudata) (ekto_State *L, int ud, const char *tname);

EKTOLIB_API void (ektoL_where) (ekto_State *L, int lvl);
EKTOLIB_API int (ektoL_error) (ekto_State *L, const char *fmt, ...);

EKTOLIB_API int (ektoL_checkoption) (ekto_State *L, int narg, const char *def,
                                   const char *const lst[]);

EKTOLIB_API int (ektoL_ref) (ekto_State *L, int t);
EKTOLIB_API void (ektoL_unref) (ekto_State *L, int t, int ref);

EKTOLIB_API int (ektoL_loadfile) (ekto_State *L, const char *filename);
EKTOLIB_API int (ektoL_loadbuffer) (ekto_State *L, const char *buff, size_t sz,
                                  const char *name);
EKTOLIB_API int (ektoL_loadstring) (ekto_State *L, const char *s);

EKTOLIB_API ekto_State *(ektoL_newstate) (void);


EKTOLIB_API const char *(ektoL_gsub) (ekto_State *L, const char *s, const char *p,
                                                  const char *r);

EKTOLIB_API const char *(ektoL_findtable) (ekto_State *L, int idx,
                                         const char *fname, int szhint);




/*
** ===============================================================
** some useful macros
** ===============================================================
*/

#define ektoL_argcheck(L, cond,numarg,extramsg)	\
		((void)((cond) || ektoL_argerror(L, (numarg), (extramsg))))
#define ektoL_checkstring(L,n)	(ektoL_checklstring(L, (n), NULL))
#define ektoL_optstring(L,n,d)	(ektoL_optlstring(L, (n), (d), NULL))
#define ektoL_checkint(L,n)	((int)ektoL_checkinteger(L, (n)))
#define ektoL_optint(L,n,d)	((int)ektoL_optinteger(L, (n), (d)))
#define ektoL_checklong(L,n)	((long)ektoL_checkinteger(L, (n)))
#define ektoL_optlong(L,n,d)	((long)ektoL_optinteger(L, (n), (d)))

#define ektoL_typename(L,i)	ekto_typename(L, ekto_type(L,(i)))

#define ektoL_dofile(L, fn) \
	(ektoL_loadfile(L, fn) || ekto_pcall(L, 0, EKTO_MULTRET, 0))

#define ektoL_dostring(L, s) \
	(ektoL_loadstring(L, s) || ekto_pcall(L, 0, EKTO_MULTRET, 0))

#define ektoL_getmetatable(L,n)	(ekto_getfield(L, EKTO_REGISTRYINDEX, (n)))

#define ektoL_opt(L,f,n,d)	(ekto_isnoneornil(L,(n)) ? (d) : f(L,(n)))

/*
** {======================================================
** Generic Buffer manipulation
** =======================================================
*/



typedef struct ektoL_Buffer {
  char *p;			/* current position in buffer */
  int lvl;  /* number of strings in the stack (level) */
  ekto_State *L;
  char buffer[EKTOL_BUFFERSIZE];
} ektoL_Buffer;

#define ektoL_addchar(B,c) \
  ((void)((B)->p < ((B)->buffer+EKTOL_BUFFERSIZE) || ektoL_prepbuffer(B)), \
   (*(B)->p++ = (char)(c)))

/* compatibility only */
#define ektoL_putchar(B,c)	ektoL_addchar(B,c)

#define ektoL_addsize(B,n)	((B)->p += (n))

EKTOLIB_API void (ektoL_buffinit) (ekto_State *L, ektoL_Buffer *B);
EKTOLIB_API char *(ektoL_prepbuffer) (ektoL_Buffer *B);
EKTOLIB_API void (ektoL_addlstring) (ektoL_Buffer *B, const char *s, size_t l);
EKTOLIB_API void (ektoL_addstring) (ektoL_Buffer *B, const char *s);
EKTOLIB_API void (ektoL_addvalue) (ektoL_Buffer *B);
EKTOLIB_API void (ektoL_pushresult) (ektoL_Buffer *B);


/* }====================================================== */


/* compatibility with ref system */

/* pre-defined references */
#define EKTO_NOREF       (-2)
#define EKTO_REFNIL      (-1)

#define ekto_ref(L,lock) ((lock) ? ektoL_ref(L, EKTO_REGISTRYINDEX) : \
      (ekto_pushstring(L, "unlocked references are obsolete"), ekto_error(L), 0))

#define ekto_unref(L,ref)        ektoL_unref(L, EKTO_REGISTRYINDEX, (ref))

#define ekto_getref(L,ref)       ekto_rawgeti(L, EKTO_REGISTRYINDEX, (ref))


#define ektoL_reg	ektoL_Reg

#endif


