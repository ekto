/*
** $Id: lapi.h,v 2.2.1.1 2007/12/27 13:02:25 roberto Exp $
** Auxiliary functions from Ekto API
** See Copyright Notice in ekto.h
*/

#ifndef lapi_h
#define lapi_h


#include "ekto_object.h"


EKTOI_FUNC void ektoA_pushobject (ekto_State *L, const TValue *o);

#endif
