/*
** $Id: lfunc.h,v 2.4.1.1 2007/12/27 13:02:25 roberto Exp $
** Auxiliary functions to manipulate prototypes and closures
** See Copyright Notice in ekto.h
*/

#ifndef lfunc_h
#define lfunc_h


#include "ekto_object.h"


#define sizeCclosure(n)	(cast(int, sizeof(CClosure)) + \
                         cast(int, sizeof(TValue)*((n)-1)))

#define sizeLclosure(n)	(cast(int, sizeof(LClosure)) + \
                         cast(int, sizeof(TValue *)*((n)-1)))


EKTOI_FUNC Proto *ektoF_newproto (ekto_State *L);
EKTOI_FUNC Closure *ektoF_newCclosure (ekto_State *L, int nelems, Table *e);
EKTOI_FUNC Closure *ektoF_newLclosure (ekto_State *L, int nelems, Table *e);
EKTOI_FUNC UpVal *ektoF_newupval (ekto_State *L);
EKTOI_FUNC UpVal *ektoF_findupval (ekto_State *L, StkId level);
EKTOI_FUNC void ektoF_close (ekto_State *L, StkId level);
EKTOI_FUNC void ektoF_freeproto (ekto_State *L, Proto *f);
EKTOI_FUNC void ektoF_freeclosure (ekto_State *L, Closure *c);
EKTOI_FUNC void ektoF_freeupval (ekto_State *L, UpVal *uv);
EKTOI_FUNC const char *ektoF_getlocalname (const Proto *func, int local_number,
                                         int pc);


#endif
