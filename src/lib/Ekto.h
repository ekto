/*
** $Id: ekto.h,v 1.218.1.5 2008/08/06 13:30:12 roberto Exp $
** Ekto - An Extensible Extension Language
** Ekto.org, PUC-Rio, Brazil (http://www.ekto.org)
** See Copyright Notice at the end of this file
*/


#ifndef ekto_h
#define ekto_h

#include <stdarg.h>
#include <stddef.h>


#include "Ekto_Conf.h"


#define EKTO_VERSION	"Ekto 5.1"
#define EKTO_RELEASE	"Ekto 5.1.4"
#define EKTO_VERSION_NUM	501
#define EKTO_COPYRIGHT	"Copyright (C) 1994-2008 Ekto.org, PUC-Rio"
#define EKTO_AUTHORS 	"R. Ierusalimschy, L. H. de Figueiredo & W. Celes"


/* mark for precompiled code (`Ekto') */
#define	EKTO_SIGNATURE	"Ekto"

/* option for multiple returns in `ekto_pcall' and `ekto_call' */
#define EKTO_MULTRET	(-1)


/*
** pseudo-indices
*/
#define EKTO_REGISTRYINDEX	(-10000)
#define EKTO_ENVIRONINDEX	(-10001)
#define EKTO_GLOBALSINDEX	(-10002)
#define ekto_upvalueindex(i)	(EKTO_GLOBALSINDEX-(i))


/* thread status; 0 is OK */
#define EKTO_YIELD	1
#define EKTO_ERRRUN	2
#define EKTO_ERRSYNTAX	3
#define EKTO_ERRMEM	4
#define EKTO_ERRERR	5


typedef struct ekto_State ekto_State;

typedef int (*ekto_CFunction) (ekto_State *L);


/*
** functions that read/write blocks when loading/dumping Ekto chunks
*/
typedef const char * (*ekto_Reader) (ekto_State *L, void *ud, size_t *sz);

typedef int (*ekto_Writer) (ekto_State *L, const void* p, size_t sz, void* ud);


/*
** prototype for memory-allocation functions
*/
typedef void * (*ekto_Alloc) (void *ud, void *ptr, size_t osize, size_t nsize);


/*
** basic types
*/
#define EKTO_TNONE		(-1)

#define EKTO_TNIL		0
#define EKTO_TBOOLEAN		1
#define EKTO_TLIGHTUSERDATA	2
#define EKTO_TNUMBER		3
#define EKTO_TSTRING		4
#define EKTO_TTABLE		5
#define EKTO_TFUNCTION		6
#define EKTO_TUSERDATA		7
#define EKTO_TTHREAD		8



/* minimum Ekto stack available to a C function */
#define EKTO_MINSTACK	20


/*
** generic extra include file
*/
#if defined(EKTO_USER_H)
#include EKTO_USER_H
#endif


/* type of numbers in Ekto */
typedef EKTO_NUMBER ekto_Number;


/* type for integer functions */
typedef EKTO_INTEGER ekto_Integer;



/*
** state manipulation
*/
EKTO_API ekto_State *(ekto_newstate) (ekto_Alloc f, void *ud);
EKTO_API void       (ekto_close) (ekto_State *L);
EKTO_API ekto_State *(ekto_newthread) (ekto_State *L);

EKTO_API ekto_CFunction (ekto_atpanic) (ekto_State *L, ekto_CFunction panicf);


/*
** basic stack manipulation
*/
EKTO_API int   (ekto_gettop) (ekto_State *L);
EKTO_API void  (ekto_settop) (ekto_State *L, int idx);
EKTO_API void  (ekto_pushvalue) (ekto_State *L, int idx);
EKTO_API void  (ekto_remove) (ekto_State *L, int idx);
EKTO_API void  (ekto_insert) (ekto_State *L, int idx);
EKTO_API void  (ekto_replace) (ekto_State *L, int idx);
EKTO_API int   (ekto_checkstack) (ekto_State *L, int sz);

EKTO_API void  (ekto_xmove) (ekto_State *from, ekto_State *to, int n);


/*
** access functions (stack -> C)
*/

EKTO_API int             (ekto_isnumber) (ekto_State *L, int idx);
EKTO_API int             (ekto_isstring) (ekto_State *L, int idx);
EKTO_API int             (ekto_iscfunction) (ekto_State *L, int idx);
EKTO_API int             (ekto_isuserdata) (ekto_State *L, int idx);
EKTO_API int             (ekto_type) (ekto_State *L, int idx);
EKTO_API const char     *(ekto_typename) (ekto_State *L, int tp);

EKTO_API int            (ekto_equal) (ekto_State *L, int idx1, int idx2);
EKTO_API int            (ekto_rawequal) (ekto_State *L, int idx1, int idx2);
EKTO_API int            (ekto_lessthan) (ekto_State *L, int idx1, int idx2);

EKTO_API ekto_Number      (ekto_tonumber) (ekto_State *L, int idx);
EKTO_API ekto_Integer     (ekto_tointeger) (ekto_State *L, int idx);
EKTO_API int             (ekto_toboolean) (ekto_State *L, int idx);
EKTO_API const char     *(ekto_tolstring) (ekto_State *L, int idx, size_t *len);
EKTO_API size_t          (ekto_objlen) (ekto_State *L, int idx);
EKTO_API ekto_CFunction   (ekto_tocfunction) (ekto_State *L, int idx);
EKTO_API void	       *(ekto_touserdata) (ekto_State *L, int idx);
EKTO_API ekto_State      *(ekto_tothread) (ekto_State *L, int idx);
EKTO_API const void     *(ekto_topointer) (ekto_State *L, int idx);


/*
** push functions (C -> stack)
*/
EKTO_API void  (ekto_pushnil) (ekto_State *L);
EKTO_API void  (ekto_pushnumber) (ekto_State *L, ekto_Number n);
EKTO_API void  (ekto_pushinteger) (ekto_State *L, ekto_Integer n);
EKTO_API void  (ekto_pushlstring) (ekto_State *L, const char *s, size_t l);
EKTO_API void  (ekto_pushstring) (ekto_State *L, const char *s);
EKTO_API const char *(ekto_pushvfstring) (ekto_State *L, const char *fmt,
                                                      va_list argp);
EKTO_API const char *(ekto_pushfstring) (ekto_State *L, const char *fmt, ...);
EKTO_API void  (ekto_pushcclosure) (ekto_State *L, ekto_CFunction fn, int n);
EKTO_API void  (ekto_pushboolean) (ekto_State *L, int b);
EKTO_API void  (ekto_pushlightuserdata) (ekto_State *L, void *p);
EKTO_API int   (ekto_pushthread) (ekto_State *L);


/*
** get functions (Ekto -> stack)
*/
EKTO_API void  (ekto_gettable) (ekto_State *L, int idx);
EKTO_API void  (ekto_getfield) (ekto_State *L, int idx, const char *k);
EKTO_API void  (ekto_rawget) (ekto_State *L, int idx);
EKTO_API void  (ekto_rawgeti) (ekto_State *L, int idx, int n);
EKTO_API void  (ekto_createtable) (ekto_State *L, int narr, int nrec);
EKTO_API void *(ekto_newuserdata) (ekto_State *L, size_t sz);
EKTO_API int   (ekto_getmetatable) (ekto_State *L, int objindex);
EKTO_API void  (ekto_getfenv) (ekto_State *L, int idx);


/*
** set functions (stack -> Ekto)
*/
EKTO_API void  (ekto_settable) (ekto_State *L, int idx);
EKTO_API void  (ekto_setfield) (ekto_State *L, int idx, const char *k);
EKTO_API void  (ekto_rawset) (ekto_State *L, int idx);
EKTO_API void  (ekto_rawseti) (ekto_State *L, int idx, int n);
EKTO_API int   (ekto_setmetatable) (ekto_State *L, int objindex);
EKTO_API int   (ekto_setfenv) (ekto_State *L, int idx);


/*
** `load' and `call' functions (load and run Ekto code)
*/
EKTO_API void  (ekto_call) (ekto_State *L, int nargs, int nresults);
EKTO_API int   (ekto_pcall) (ekto_State *L, int nargs, int nresults, int errfunc);
EKTO_API int   (ekto_cpcall) (ekto_State *L, ekto_CFunction func, void *ud);
EKTO_API int   (ekto_load) (ekto_State *L, ekto_Reader reader, void *dt,
                                        const char *chunkname);

EKTO_API int (ekto_dump) (ekto_State *L, ekto_Writer writer, void *data);


/*
** coroutine functions
*/
EKTO_API int  (ekto_yield) (ekto_State *L, int nresults);
EKTO_API int  (ekto_resume) (ekto_State *L, int narg);
EKTO_API int  (ekto_status) (ekto_State *L);

/*
** garbage-collection function and options
*/

#define EKTO_GCSTOP		0
#define EKTO_GCRESTART		1
#define EKTO_GCCOLLECT		2
#define EKTO_GCCOUNT		3
#define EKTO_GCCOUNTB		4
#define EKTO_GCSTEP		5
#define EKTO_GCSETPAUSE		6
#define EKTO_GCSETSTEPMUL	7

EKTO_API int (ekto_gc) (ekto_State *L, int what, int data);


/*
** miscellaneous functions
*/

EKTO_API int   (ekto_error) (ekto_State *L);

EKTO_API int   (ekto_next) (ekto_State *L, int idx);

EKTO_API void  (ekto_concat) (ekto_State *L, int n);

EKTO_API ekto_Alloc (ekto_getallocf) (ekto_State *L, void **ud);
EKTO_API void ekto_setallocf (ekto_State *L, ekto_Alloc f, void *ud);



/* 
** ===============================================================
** some useful macros
** ===============================================================
*/

#define ekto_pop(L,n)		ekto_settop(L, -(n)-1)

#define ekto_newtable(L)		ekto_createtable(L, 0, 0)

#define ekto_register(L,n,f) (ekto_pushcfunction(L, (f)), ekto_setglobal(L, (n)))

#define ekto_pushcfunction(L,f)	ekto_pushcclosure(L, (f), 0)

#define ekto_strlen(L,i)		ekto_objlen(L, (i))

#define ekto_isfunction(L,n)	(ekto_type(L, (n)) == EKTO_TFUNCTION)
#define ekto_istable(L,n)	(ekto_type(L, (n)) == EKTO_TTABLE)
#define ekto_islightuserdata(L,n)	(ekto_type(L, (n)) == EKTO_TLIGHTUSERDATA)
#define ekto_isnil(L,n)		(ekto_type(L, (n)) == EKTO_TNIL)
#define ekto_isboolean(L,n)	(ekto_type(L, (n)) == EKTO_TBOOLEAN)
#define ekto_isthread(L,n)	(ekto_type(L, (n)) == EKTO_TTHREAD)
#define ekto_isnone(L,n)		(ekto_type(L, (n)) == EKTO_TNONE)
#define ekto_isnoneornil(L, n)	(ekto_type(L, (n)) <= 0)

#define ekto_pushliteral(L, s)	\
	ekto_pushlstring(L, "" s, (sizeof(s)/sizeof(char))-1)

#define ekto_setglobal(L,s)	ekto_setfield(L, EKTO_GLOBALSINDEX, (s))
#define ekto_getglobal(L,s)	ekto_getfield(L, EKTO_GLOBALSINDEX, (s))

#define ekto_tostring(L,i)	ekto_tolstring(L, (i), NULL)



/*
** compatibility macros and functions
*/

#define ekto_open()	ektoL_newstate()

#define ekto_getregistry(L)	ekto_pushvalue(L, EKTO_REGISTRYINDEX)

#define ekto_getgccount(L)	ekto_gc(L, EKTO_GCCOUNT, 0)

#define ekto_Chunkreader		ekto_Reader
#define ekto_Chunkwriter		ekto_Writer


/* hack */
EKTO_API void ekto_setlevel	(ekto_State *from, ekto_State *to);


/*
** {======================================================================
** Debug API
** =======================================================================
*/


/*
** Event codes
*/
#define EKTO_HOOKCALL	0
#define EKTO_HOOKRET	1
#define EKTO_HOOKLINE	2
#define EKTO_HOOKCOUNT	3
#define EKTO_HOOKTAILRET 4


/*
** Event masks
*/
#define EKTO_MASKCALL	(1 << EKTO_HOOKCALL)
#define EKTO_MASKRET	(1 << EKTO_HOOKRET)
#define EKTO_MASKLINE	(1 << EKTO_HOOKLINE)
#define EKTO_MASKCOUNT	(1 << EKTO_HOOKCOUNT)

typedef struct ekto_Debug ekto_Debug;  /* activation record */


/* Functions to be called by the debuger in specific events */
typedef void (*ekto_Hook) (ekto_State *L, ekto_Debug *ar);


EKTO_API int ekto_getstack (ekto_State *L, int level, ekto_Debug *ar);
EKTO_API int ekto_getinfo (ekto_State *L, const char *what, ekto_Debug *ar);
EKTO_API const char *ekto_getlocal (ekto_State *L, const ekto_Debug *ar, int n);
EKTO_API const char *ekto_setlocal (ekto_State *L, const ekto_Debug *ar, int n);
EKTO_API const char *ekto_getupvalue (ekto_State *L, int funcindex, int n);
EKTO_API const char *ekto_setupvalue (ekto_State *L, int funcindex, int n);

EKTO_API int ekto_sethook (ekto_State *L, ekto_Hook func, int mask, int count);
EKTO_API ekto_Hook ekto_gethook (ekto_State *L);
EKTO_API int ekto_gethookmask (ekto_State *L);
EKTO_API int ekto_gethookcount (ekto_State *L);


struct ekto_Debug {
  int event;
  const char *name;	/* (n) */
  const char *namewhat;	/* (n) `global', `local', `field', `method' */
  const char *what;	/* (S) `Ekto', `C', `main', `tail' */
  const char *source;	/* (S) */
  int currentline;	/* (l) */
  int nups;		/* (u) number of upvalues */
  int linedefined;	/* (S) */
  int lastlinedefined;	/* (S) */
  char short_src[EKTO_IDSIZE]; /* (S) */
  /* private part */
  int i_ci;  /* active function */
};

/* }====================================================================== */


/******************************************************************************
* Copyright (C) 1994-2008 Ekto.org, PUC-Rio.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/


#endif
