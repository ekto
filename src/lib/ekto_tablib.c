/*
** $Id: ltablib.c,v 1.38.1.3 2008/02/14 16:46:58 roberto Exp $
** Library for Table Manipulation
** See Copyright Notice in ekto.h
*/


#include <stddef.h>

#define ltablib_c
#define EKTO_LIB

#include "Ekto.h"

#include "Ekto_Aux.h"
#include "Ekto_Lib.h"


#define aux_getn(L,n)	(ektoL_checktype(L, n, EKTO_TTABLE), ektoL_getn(L, n))


static int foreachi (ekto_State *L) {
  int i;
  int n = aux_getn(L, 1);
  ektoL_checktype(L, 2, EKTO_TFUNCTION);
  for (i=1; i <= n; i++) {
    ekto_pushvalue(L, 2);  /* function */
    ekto_pushinteger(L, i);  /* 1st argument */
    ekto_rawgeti(L, 1, i);  /* 2nd argument */
    ekto_call(L, 2, 1);
    if (!ekto_isnil(L, -1))
      return 1;
    ekto_pop(L, 1);  /* remove nil result */
  }
  return 0;
}


static int foreach (ekto_State *L) {
  ektoL_checktype(L, 1, EKTO_TTABLE);
  ektoL_checktype(L, 2, EKTO_TFUNCTION);
  ekto_pushnil(L);  /* first key */
  while (ekto_next(L, 1)) {
    ekto_pushvalue(L, 2);  /* function */
    ekto_pushvalue(L, -3);  /* key */
    ekto_pushvalue(L, -3);  /* value */
    ekto_call(L, 2, 1);
    if (!ekto_isnil(L, -1))
      return 1;
    ekto_pop(L, 2);  /* remove value and result */
  }
  return 0;
}


static int maxn (ekto_State *L) {
  ekto_Number max = 0;
  ektoL_checktype(L, 1, EKTO_TTABLE);
  ekto_pushnil(L);  /* first key */
  while (ekto_next(L, 1)) {
    ekto_pop(L, 1);  /* remove value */
    if (ekto_type(L, -1) == EKTO_TNUMBER) {
      ekto_Number v = ekto_tonumber(L, -1);
      if (v > max) max = v;
    }
  }
  ekto_pushnumber(L, max);
  return 1;
}


static int getn (ekto_State *L) {
  ekto_pushinteger(L, aux_getn(L, 1));
  return 1;
}


static int setn (ekto_State *L) {
  ektoL_checktype(L, 1, EKTO_TTABLE);
#ifndef ektoL_setn
  ektoL_setn(L, 1, ektoL_checkint(L, 2));
#else
  ektoL_error(L, EKTO_QL("setn") " is obsolete");
#endif
  ekto_pushvalue(L, 1);
  return 1;
}


static int tinsert (ekto_State *L) {
  int e = aux_getn(L, 1) + 1;  /* first empty element */
  int pos;  /* where to insert new element */
  switch (ekto_gettop(L)) {
    case 2: {  /* called with only 2 arguments */
      pos = e;  /* insert new element at the end */
      break;
    }
    case 3: {
      int i;
      pos = ektoL_checkint(L, 2);  /* 2nd argument is the position */
      if (pos > e) e = pos;  /* `grow' array if necessary */
      for (i = e; i > pos; i--) {  /* move up elements */
        ekto_rawgeti(L, 1, i-1);
        ekto_rawseti(L, 1, i);  /* t[i] = t[i-1] */
      }
      break;
    }
    default: {
      return ektoL_error(L, "wrong number of arguments to " EKTO_QL("insert"));
    }
  }
  ektoL_setn(L, 1, e);  /* new size */
  ekto_rawseti(L, 1, pos);  /* t[pos] = v */
  return 0;
}


static int tremove (ekto_State *L) {
  int e = aux_getn(L, 1);
  int pos = ektoL_optint(L, 2, e);
  if (!(1 <= pos && pos <= e))  /* position is outside bounds? */
   return 0;  /* nothing to remove */
  ektoL_setn(L, 1, e - 1);  /* t.n = n-1 */
  ekto_rawgeti(L, 1, pos);  /* result = t[pos] */
  for ( ;pos<e; pos++) {
    ekto_rawgeti(L, 1, pos+1);
    ekto_rawseti(L, 1, pos);  /* t[pos] = t[pos+1] */
  }
  ekto_pushnil(L);
  ekto_rawseti(L, 1, e);  /* t[e] = nil */
  return 1;
}


static void addfield (ekto_State *L, ektoL_Buffer *b, int i) {
  ekto_rawgeti(L, 1, i);
  if (!ekto_isstring(L, -1))
    ektoL_error(L, "invalid value (%s) at index %d in table for "
                  EKTO_QL("concat"), ektoL_typename(L, -1), i);
    ektoL_addvalue(b);
}


static int tconcat (ekto_State *L) {
  ektoL_Buffer b;
  size_t lsep;
  int i, last;
  const char *sep = ektoL_optlstring(L, 2, "", &lsep);
  ektoL_checktype(L, 1, EKTO_TTABLE);
  i = ektoL_optint(L, 3, 1);
  last = ektoL_opt(L, ektoL_checkint, 4, ektoL_getn(L, 1));
  ektoL_buffinit(L, &b);
  for (; i < last; i++) {
    addfield(L, &b, i);
    ektoL_addlstring(&b, sep, lsep);
  }
  if (i == last)  /* add last value (if interval was not empty) */
    addfield(L, &b, i);
  ektoL_pushresult(&b);
  return 1;
}



/*
** {======================================================
** Quicksort
** (based on `Algorithms in MODULA-3', Robert Sedgewick;
**  Addison-Wesley, 1993.)
*/


static void set2 (ekto_State *L, int i, int j) {
  ekto_rawseti(L, 1, i);
  ekto_rawseti(L, 1, j);
}

static int sort_comp (ekto_State *L, int a, int b) {
  if (!ekto_isnil(L, 2)) {  /* function? */
    int res;
    ekto_pushvalue(L, 2);
    ekto_pushvalue(L, a-1);  /* -1 to compensate function */
    ekto_pushvalue(L, b-2);  /* -2 to compensate function and `a' */
    ekto_call(L, 2, 1);
    res = ekto_toboolean(L, -1);
    ekto_pop(L, 1);
    return res;
  }
  else  /* a < b? */
    return ekto_lessthan(L, a, b);
}

static void auxsort (ekto_State *L, int l, int u) {
  while (l < u) {  /* for tail recursion */
    int i, j;
    /* sort elements a[l], a[(l+u)/2] and a[u] */
    ekto_rawgeti(L, 1, l);
    ekto_rawgeti(L, 1, u);
    if (sort_comp(L, -1, -2))  /* a[u] < a[l]? */
      set2(L, l, u);  /* swap a[l] - a[u] */
    else
      ekto_pop(L, 2);
    if (u-l == 1) break;  /* only 2 elements */
    i = (l+u)/2;
    ekto_rawgeti(L, 1, i);
    ekto_rawgeti(L, 1, l);
    if (sort_comp(L, -2, -1))  /* a[i]<a[l]? */
      set2(L, i, l);
    else {
      ekto_pop(L, 1);  /* remove a[l] */
      ekto_rawgeti(L, 1, u);
      if (sort_comp(L, -1, -2))  /* a[u]<a[i]? */
        set2(L, i, u);
      else
        ekto_pop(L, 2);
    }
    if (u-l == 2) break;  /* only 3 elements */
    ekto_rawgeti(L, 1, i);  /* Pivot */
    ekto_pushvalue(L, -1);
    ekto_rawgeti(L, 1, u-1);
    set2(L, i, u-1);
    /* a[l] <= P == a[u-1] <= a[u], only need to sort from l+1 to u-2 */
    i = l; j = u-1;
    for (;;) {  /* invariant: a[l..i] <= P <= a[j..u] */
      /* repeat ++i until a[i] >= P */
      while (ekto_rawgeti(L, 1, ++i), sort_comp(L, -1, -2)) {
        if (i>u) ektoL_error(L, "invalid order function for sorting");
        ekto_pop(L, 1);  /* remove a[i] */
      }
      /* repeat --j until a[j] <= P */
      while (ekto_rawgeti(L, 1, --j), sort_comp(L, -3, -1)) {
        if (j<l) ektoL_error(L, "invalid order function for sorting");
        ekto_pop(L, 1);  /* remove a[j] */
      }
      if (j<i) {
        ekto_pop(L, 3);  /* pop pivot, a[i], a[j] */
        break;
      }
      set2(L, i, j);
    }
    ekto_rawgeti(L, 1, u-1);
    ekto_rawgeti(L, 1, i);
    set2(L, u-1, i);  /* swap pivot (a[u-1]) with a[i] */
    /* a[l..i-1] <= a[i] == P <= a[i+1..u] */
    /* adjust so that smaller half is in [j..i] and larger one in [l..u] */
    if (i-l < u-i) {
      j=l; i=i-1; l=i+2;
    }
    else {
      j=i+1; i=u; u=j-2;
    }
    auxsort(L, j, i);  /* call recursively the smaller one */
  }  /* repeat the routine for the larger one */
}

static int sort (ekto_State *L) {
  int n = aux_getn(L, 1);
  ektoL_checkstack(L, 40, "");  /* assume array is smaller than 2^40 */
  if (!ekto_isnoneornil(L, 2))  /* is there a 2nd argument? */
    ektoL_checktype(L, 2, EKTO_TFUNCTION);
  ekto_settop(L, 2);  /* make sure there is two arguments */
  auxsort(L, 1, n);
  return 0;
}

/* }====================================================== */


static const ektoL_Reg tab_funcs[] = {
  {"concat", tconcat},
  {"foreach", foreach},
  {"foreachi", foreachi},
  {"getn", getn},
  {"maxn", maxn},
  {"insert", tinsert},
  {"remove", tremove},
  {"setn", setn},
  {"sort", sort},
  {NULL, NULL}
};


EKTOLIB_API int ektoopen_table (ekto_State *L) {
  ektoL_register(L, EKTO_TABLIBNAME, tab_funcs);
  return 1;
}

