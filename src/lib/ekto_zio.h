/*
** $Id: lzio.h,v 1.21.1.1 2007/12/27 13:02:25 roberto Exp $
** Buffered streams
** See Copyright Notice in ekto.h
*/


#ifndef lzio_h
#define lzio_h

#include "Ekto.h"

#include "ekto_mem.h"


#define EOZ	(-1)			/* end of stream */

typedef struct Zio ZIO;

#define char2int(c)	cast(int, cast(unsigned char, (c)))

#define zgetc(z)  (((z)->n--)>0 ?  char2int(*(z)->p++) : ektoZ_fill(z))

typedef struct Mbuffer {
  char *buffer;
  size_t n;
  size_t buffsize;
} Mbuffer;

#define ektoZ_initbuffer(L, buff) ((buff)->buffer = NULL, (buff)->buffsize = 0)

#define ektoZ_buffer(buff)	((buff)->buffer)
#define ektoZ_sizebuffer(buff)	((buff)->buffsize)
#define ektoZ_bufflen(buff)	((buff)->n)

#define ektoZ_resetbuffer(buff) ((buff)->n = 0)


#define ektoZ_resizebuffer(L, buff, size) \
	(ektoM_reallocvector(L, (buff)->buffer, (buff)->buffsize, size, char), \
	(buff)->buffsize = size)

#define ektoZ_freebuffer(L, buff)	ektoZ_resizebuffer(L, buff, 0)


EKTOI_FUNC char *ektoZ_openspace (ekto_State *L, Mbuffer *buff, size_t n);
EKTOI_FUNC void ektoZ_init (ekto_State *L, ZIO *z, ekto_Reader reader,
                                        void *data);
EKTOI_FUNC size_t ektoZ_read (ZIO* z, void* b, size_t n);	/* read next n bytes */
EKTOI_FUNC int ektoZ_lookahead (ZIO *z);



/* --------- Private Part ------------------ */

struct Zio {
  size_t n;			/* bytes still unread */
  const char *p;		/* current position in buffer */
  ekto_Reader reader;
  void* data;			/* additional data */
  ekto_State *L;			/* Ekto state (for reader) */
};


EKTOI_FUNC int ektoZ_fill (ZIO *z);

#endif
