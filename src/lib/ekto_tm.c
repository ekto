/*
** $Id: ltm.c,v 2.8.1.1 2007/12/27 13:02:25 roberto Exp $
** Tag methods
** See Copyright Notice in ekto.h
*/


#include <string.h>

#define ltm_c
#define EKTO_CORE

#include "Ekto.h"

#include "ekto_object.h"
#include "ekto_state.h"
#include "ekto_string.h"
#include "ekto_table.h"
#include "ekto_tm.h"



const char *const ektoT_typenames[] = {
  "nil", "boolean", "userdata", "number",
  "string", "table", "function", "userdata", "thread",
  "proto", "upval"
};


void ektoT_init (ekto_State *L) {
  static const char *const ektoT_eventname[] = {  /* ORDER TM */
    "__index", "__newindex",
    "__gc", "__mode", "__eq",
    "__add", "__sub", "__mul", "__div", "__mod",
    "__pow", "__unm", "__len", "__lt", "__le",
    "__concat", "__call"
  };
  int i;
  for (i=0; i<TM_N; i++) {
    G(L)->tmname[i] = ektoS_new(L, ektoT_eventname[i]);
    ektoS_fix(G(L)->tmname[i]);  /* never collect these names */
  }
}


/*
** function to be used with macro "fasttm": optimized for absence of
** tag methods
*/
const TValue *ektoT_gettm (Table *events, TMS event, TString *ename) {
  const TValue *tm = ektoH_getstr(events, ename);
  ekto_assert(event <= TM_EQ);
  if (ttisnil(tm)) {  /* no tag method? */
    events->flags |= cast_byte(1u<<event);  /* cache this fact */
    return NULL;
  }
  else return tm;
}


const TValue *ektoT_gettmbyobj (ekto_State *L, const TValue *o, TMS event) {
  Table *mt;
  switch (ttype(o)) {
    case EKTO_TTABLE:
      mt = hvalue(o)->metatable;
      break;
    case EKTO_TUSERDATA:
      mt = uvalue(o)->metatable;
      break;
    default:
      mt = G(L)->mt[ttype(o)];
  }
  return (mt ? ektoH_getstr(mt, G(L)->tmname[event]) : ektoO_nilobject);
}

