/*
** $Id: ltable.h,v 2.10.1.1 2007/12/27 13:02:25 roberto Exp $
** Ekto tables (hash)
** See Copyright Notice in ekto.h
*/

#ifndef ltable_h
#define ltable_h

#include "ekto_object.h"


#define gnode(t,i)	(&(t)->node[i])
#define gkey(n)		(&(n)->i_key.nk)
#define gval(n)		(&(n)->i_val)
#define gnext(n)	((n)->i_key.nk.next)

#define key2tval(n)	(&(n)->i_key.tvk)


EKTOI_FUNC const TValue *ektoH_getnum (Table *t, int key);
EKTOI_FUNC TValue *ektoH_setnum (ekto_State *L, Table *t, int key);
EKTOI_FUNC const TValue *ektoH_getstr (Table *t, TString *key);
EKTOI_FUNC TValue *ektoH_setstr (ekto_State *L, Table *t, TString *key);
EKTOI_FUNC const TValue *ektoH_get (Table *t, const TValue *key);
EKTOI_FUNC TValue *ektoH_set (ekto_State *L, Table *t, const TValue *key);
EKTOI_FUNC Table *ektoH_new (ekto_State *L, int narray, int lnhash);
EKTOI_FUNC void ektoH_resizearray (ekto_State *L, Table *t, int nasize);
EKTOI_FUNC void ektoH_free (ekto_State *L, Table *t);
EKTOI_FUNC int ektoH_next (ekto_State *L, Table *t, StkId key);
EKTOI_FUNC int ektoH_getn (Table *t);


#if defined(EKTO_DEBUG)
EKTOI_FUNC Node *ektoH_mainposition (const Table *t, const TValue *key);
EKTOI_FUNC int ektoH_isdummy (Node *n);
#endif


#endif
