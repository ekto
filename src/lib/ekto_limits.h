/*
** $Id: llimits.h,v 1.69.1.1 2007/12/27 13:02:25 roberto Exp $
** Limits, basic types, and some other `installation-dependent' definitions
** See Copyright Notice in ekto.h
*/

#ifndef llimits_h
#define llimits_h


#include <limits.h>
#include <stddef.h>


#include "Ekto.h"


typedef EKTOI_UINT32 lu_int32;

typedef EKTOI_UMEM lu_mem;

typedef EKTOI_MEM l_mem;



/* chars used as small naturals (so that `char' is reserved for characters) */
typedef unsigned char lu_byte;


#define MAX_SIZET	((size_t)(~(size_t)0)-2)

#define MAX_LUMEM	((lu_mem)(~(lu_mem)0)-2)


#define MAX_INT (INT_MAX-2)  /* maximum value of an int (-2 for safety) */

/*
** conversion of pointer to integer
** this is for hashing only; there is no problem if the integer
** cannot hold the whole pointer value
*/
#define IntPoint(p)  ((unsigned int)(lu_mem)(p))



/* type to ensure maximum alignment */
typedef EKTOI_USER_ALIGNMENT_T L_Umaxalign;


/* result of a `usual argument conversion' over ekto_Number */
typedef EKTOI_UACNUMBER l_uacNumber;


/* internal assertions for in-house debugging */
#ifdef ekto_assert

#define check_exp(c,e)		(ekto_assert(c), (e))
#define api_check(l,e)		ekto_assert(e)

#else

#define ekto_assert(c)		((void)0)
#define check_exp(c,e)		(e)
#define api_check		ektoi_apicheck

#endif


#ifndef UNUSED
#define UNUSED(x)	((void)(x))	/* to avoid warnings */
#endif


#ifndef cast
#define cast(t, exp)	((t)(exp))
#endif

#define cast_byte(i)	cast(lu_byte, (i))
#define cast_num(i)	cast(ekto_Number, (i))
#define cast_int(i)	cast(int, (i))



/*
** type for virtual-machine instructions
** must be an unsigned with (at least) 4 bytes (see details in lopcodes.h)
*/
typedef lu_int32 Instruction;



/* maximum stack for a Ekto function */
#define MAXSTACK	250



/* minimum size for the string table (must be power of 2) */
#ifndef MINSTRTABSIZE
#define MINSTRTABSIZE	32
#endif


/* minimum size for string buffer */
#ifndef EKTO_MINBUFFER
#define EKTO_MINBUFFER	32
#endif


#ifndef ekto_lock
#define ekto_lock(L)     ((void) 0) 
#define ekto_unlock(L)   ((void) 0)
#endif

#ifndef ektoi_threadyield
#define ektoi_threadyield(L)     {ekto_unlock(L); ekto_lock(L);}
#endif


/*
** macro to control inclusion of some hard tests on stack reallocation
*/ 
#ifndef HARDSTACKTESTS
#define condhardstacktests(x)	((void)0)
#else
#define condhardstacktests(x)	x
#endif

#endif
