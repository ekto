/*
** $Id: ektolib.h,v 1.36.1.1 2007/12/27 13:02:25 roberto Exp $
** Ekto standard libraries
** See Copyright Notice in ekto.h
*/


#ifndef ektolib_h
#define ektolib_h

#include "Ekto.h"


/* Key to file-handle type */
#define EKTO_FILEHANDLE		"FILE*"


#define EKTO_COLIBNAME	"coroutine"
EKTOLIB_API int (ektoopen_base) (ekto_State *L);

#define EKTO_TABLIBNAME	"table"
EKTOLIB_API int (ektoopen_table) (ekto_State *L);

#define EKTO_IOLIBNAME	"io"
EKTOLIB_API int (ektoopen_io) (ekto_State *L);

#define EKTO_OSLIBNAME	"os"
EKTOLIB_API int (ektoopen_os) (ekto_State *L);

#define EKTO_STRLIBNAME	"string"
EKTOLIB_API int (ektoopen_string) (ekto_State *L);

#define EKTO_MATHLIBNAME	"math"
EKTOLIB_API int (ektoopen_math) (ekto_State *L);

#define EKTO_DBLIBNAME	"debug"
EKTOLIB_API int (ektoopen_debug) (ekto_State *L);

#define EKTO_LOADLIBNAME	"package"
EKTOLIB_API int (ektoopen_package) (ekto_State *L);


/* open all previous libraries */
EKTOLIB_API void (ektoL_openlibs) (ekto_State *L); 



#ifndef ekto_assert
#define ekto_assert(x)	((void)0)
#endif


#endif
