/*
** $Id: ldo.c,v 2.38.1.3 2008/01/18 22:31:22 roberto Exp $
** Stack and Call structure of Ekto
** See Copyright Notice in ekto.h
*/


#include <setjmp.h>
#include <stdlib.h>
#include <string.h>

#define ldo_c
#define EKTO_CORE

#include "Ekto.h"

#include "ekto_debug.h"
#include "ekto_do.h"
#include "ekto_func.h"
#include "ekto_gc.h"
#include "ekto_mem.h"
#include "ekto_object.h"
#include "ekto_opcodes.h"
#include "ekto_parser.h"
#include "ekto_state.h"
#include "ekto_string.h"
#include "ekto_table.h"
#include "ekto_tm.h"
#include "ekto_undump.h"
#include "ekto_vm.h"
#include "ekto_zio.h"




/*
** {======================================================
** Error-recovery functions
** =======================================================
*/


/* chain list of long jump buffers */
struct ekto_longjmp {
  struct ekto_longjmp *previous;
  ektoi_jmpbuf b;
  volatile int status;  /* error code */
};


void ektoD_seterrorobj (ekto_State *L, int errcode, StkId oldtop) {
  switch (errcode) {
    case EKTO_ERRMEM: {
      setsvalue2s(L, oldtop, ektoS_newliteral(L, MEMERRMSG));
      break;
    }
    case EKTO_ERRERR: {
      setsvalue2s(L, oldtop, ektoS_newliteral(L, "error in error handling"));
      break;
    }
    case EKTO_ERRSYNTAX:
    case EKTO_ERRRUN: {
      setobjs2s(L, oldtop, L->top - 1);  /* error message on current top */
      break;
    }
  }
  L->top = oldtop + 1;
}


static void restore_stack_limit (ekto_State *L) {
  ekto_assert(L->stack_last - L->stack == L->stacksize - EXTRA_STACK - 1);
  if (L->size_ci > EKTOI_MAXCALLS) {  /* there was an overflow? */
    int inuse = cast_int(L->ci - L->base_ci);
    if (inuse + 1 < EKTOI_MAXCALLS)  /* can `undo' overflow? */
      ektoD_reallocCI(L, EKTOI_MAXCALLS);
  }
}


static void resetstack (ekto_State *L, int status) {
  L->ci = L->base_ci;
  L->base = L->ci->base;
  ektoF_close(L, L->base);  /* close eventual pending closures */
  ektoD_seterrorobj(L, status, L->base);
  L->nCcalls = L->baseCcalls;
  L->allowhook = 1;
  restore_stack_limit(L);
  L->errfunc = 0;
  L->errorJmp = NULL;
}


void ektoD_throw (ekto_State *L, int errcode) {
  if (L->errorJmp) {
    L->errorJmp->status = errcode;
    EKTOI_THROW(L, L->errorJmp);
  }
  else {
    L->status = cast_byte(errcode);
    if (G(L)->panic) {
      resetstack(L, errcode);
      ekto_unlock(L);
      G(L)->panic(L);
    }
    exit(EXIT_FAILURE);
  }
}


int ektoD_rawrunprotected (ekto_State *L, Pfunc f, void *ud) {
  struct ekto_longjmp lj;
  lj.status = 0;
  lj.previous = L->errorJmp;  /* chain new error handler */
  L->errorJmp = &lj;
  EKTOI_TRY(L, &lj,
    (*f)(L, ud);
  );
  L->errorJmp = lj.previous;  /* restore old error handler */
  return lj.status;
}

/* }====================================================== */


static void correctstack (ekto_State *L, TValue *oldstack) {
  CallInfo *ci;
  GCObject *up;
  L->top = (L->top - oldstack) + L->stack;
  for (up = L->openupval; up != NULL; up = up->gch.next)
    gco2uv(up)->v = (gco2uv(up)->v - oldstack) + L->stack;
  for (ci = L->base_ci; ci <= L->ci; ci++) {
    ci->top = (ci->top - oldstack) + L->stack;
    ci->base = (ci->base - oldstack) + L->stack;
    ci->func = (ci->func - oldstack) + L->stack;
  }
  L->base = (L->base - oldstack) + L->stack;
}


void ektoD_reallocstack (ekto_State *L, int newsize) {
  TValue *oldstack = L->stack;
  int realsize = newsize + 1 + EXTRA_STACK;
  ekto_assert(L->stack_last - L->stack == L->stacksize - EXTRA_STACK - 1);
  ektoM_reallocvector(L, L->stack, L->stacksize, realsize, TValue);
  L->stacksize = realsize;
  L->stack_last = L->stack+newsize;
  correctstack(L, oldstack);
}


void ektoD_reallocCI (ekto_State *L, int newsize) {
  CallInfo *oldci = L->base_ci;
  ektoM_reallocvector(L, L->base_ci, L->size_ci, newsize, CallInfo);
  L->size_ci = newsize;
  L->ci = (L->ci - oldci) + L->base_ci;
  L->end_ci = L->base_ci + L->size_ci - 1;
}


void ektoD_growstack (ekto_State *L, int n) {
  if (n <= L->stacksize)  /* double size is enough? */
    ektoD_reallocstack(L, 2*L->stacksize);
  else
    ektoD_reallocstack(L, L->stacksize + n);
}


static CallInfo *growCI (ekto_State *L) {
  if (L->size_ci > EKTOI_MAXCALLS)  /* overflow while handling overflow? */
    ektoD_throw(L, EKTO_ERRERR);
  else {
    ektoD_reallocCI(L, 2*L->size_ci);
    if (L->size_ci > EKTOI_MAXCALLS)
      ektoG_runerror(L, "stack overflow");
  }
  return ++L->ci;
}


void ektoD_callhook (ekto_State *L, int event, int line) {
  ekto_Hook hook = L->hook;
  if (hook && L->allowhook) {
    ptrdiff_t top = savestack(L, L->top);
    ptrdiff_t ci_top = savestack(L, L->ci->top);
    ekto_Debug ar;
    ar.event = event;
    ar.currentline = line;
    if (event == EKTO_HOOKTAILRET)
      ar.i_ci = 0;  /* tail call; no debug information about it */
    else
      ar.i_ci = cast_int(L->ci - L->base_ci);
    ektoD_checkstack(L, EKTO_MINSTACK);  /* ensure minimum stack size */
    L->ci->top = L->top + EKTO_MINSTACK;
    ekto_assert(L->ci->top <= L->stack_last);
    L->allowhook = 0;  /* cannot call hooks inside a hook */
    ekto_unlock(L);
    (*hook)(L, &ar);
    ekto_lock(L);
    ekto_assert(!L->allowhook);
    L->allowhook = 1;
    L->ci->top = restorestack(L, ci_top);
    L->top = restorestack(L, top);
  }
}


static StkId adjust_varargs (ekto_State *L, Proto *p, int actual) {
  int i;
  int nfixargs = p->numparams;
  Table *htab = NULL;
  StkId base, fixed;
  for (; actual < nfixargs; ++actual)
    setnilvalue(L->top++);
#if defined(EKTO_COMPAT_VARARG)
  if (p->is_vararg & VARARG_NEEDSARG) { /* compat. with old-style vararg? */
    int nvar = actual - nfixargs;  /* number of extra arguments */
    ekto_assert(p->is_vararg & VARARG_HASARG);
    ektoC_checkGC(L);
    htab = ektoH_new(L, nvar, 1);  /* create `arg' table */
    for (i=0; i<nvar; i++)  /* put extra arguments into `arg' table */
      setobj2n(L, ektoH_setnum(L, htab, i+1), L->top - nvar + i);
    /* store counter in field `n' */
    setnvalue(ektoH_setstr(L, htab, ektoS_newliteral(L, "n")), cast_num(nvar));
  }
#endif
  /* move fixed parameters to final position */
  fixed = L->top - actual;  /* first fixed argument */
  base = L->top;  /* final position of first argument */
  for (i=0; i<nfixargs; i++) {
    setobjs2s(L, L->top++, fixed+i);
    setnilvalue(fixed+i);
  }
  /* add `arg' parameter */
  if (htab) {
    sethvalue(L, L->top++, htab);
    ekto_assert(iswhite(obj2gco(htab)));
  }
  return base;
}


static StkId tryfuncTM (ekto_State *L, StkId func) {
  const TValue *tm = ektoT_gettmbyobj(L, func, TM_CALL);
  StkId p;
  ptrdiff_t funcr = savestack(L, func);
  if (!ttisfunction(tm))
    ektoG_typeerror(L, func, "call");
  /* Open a hole inside the stack at `func' */
  for (p = L->top; p > func; p--) setobjs2s(L, p, p-1);
  incr_top(L);
  func = restorestack(L, funcr);  /* previous call may change stack */
  setobj2s(L, func, tm);  /* tag method is the new function to be called */
  return func;
}



#define inc_ci(L) \
  ((L->ci == L->end_ci) ? growCI(L) : \
   (condhardstacktests(ektoD_reallocCI(L, L->size_ci)), ++L->ci))


int ektoD_precall (ekto_State *L, StkId func, int nresults) {
  LClosure *cl;
  ptrdiff_t funcr;
  if (!ttisfunction(func)) /* `func' is not a function? */
    func = tryfuncTM(L, func);  /* check the `function' tag method */
  funcr = savestack(L, func);
  cl = &clvalue(func)->l;
  L->ci->savedpc = L->savedpc;
  if (!cl->isC) {  /* Ekto function? prepare its call */
    CallInfo *ci;
    StkId st, base;
    Proto *p = cl->p;
    ektoD_checkstack(L, p->maxstacksize);
    func = restorestack(L, funcr);
    if (!p->is_vararg) {  /* no varargs? */
      base = func + 1;
      if (L->top > base + p->numparams)
        L->top = base + p->numparams;
    }
    else {  /* vararg function */
      int nargs = cast_int(L->top - func) - 1;
      base = adjust_varargs(L, p, nargs);
      func = restorestack(L, funcr);  /* previous call may change the stack */
    }
    ci = inc_ci(L);  /* now `enter' new function */
    ci->func = func;
    L->base = ci->base = base;
    ci->top = L->base + p->maxstacksize;
    ekto_assert(ci->top <= L->stack_last);
    L->savedpc = p->code;  /* starting point */
    ci->tailcalls = 0;
    ci->nresults = nresults;
    for (st = L->top; st < ci->top; st++)
      setnilvalue(st);
    L->top = ci->top;
    if (L->hookmask & EKTO_MASKCALL) {
      L->savedpc++;  /* hooks assume 'pc' is already incremented */
      ektoD_callhook(L, EKTO_HOOKCALL, -1);
      L->savedpc--;  /* correct 'pc' */
    }
    return PCREKTO;
  }
  else {  /* if is a C function, call it */
    CallInfo *ci;
    int n;
    ektoD_checkstack(L, EKTO_MINSTACK);  /* ensure minimum stack size */
    ci = inc_ci(L);  /* now `enter' new function */
    ci->func = restorestack(L, funcr);
    L->base = ci->base = ci->func + 1;
    ci->top = L->top + EKTO_MINSTACK;
    ekto_assert(ci->top <= L->stack_last);
    ci->nresults = nresults;
    if (L->hookmask & EKTO_MASKCALL)
      ektoD_callhook(L, EKTO_HOOKCALL, -1);
    ekto_unlock(L);
    n = (*curr_func(L)->c.f)(L);  /* do the actual call */
    ekto_lock(L);
    if (n < 0)  /* yielding? */
      return PCRYIELD;
    else {
      ektoD_poscall(L, L->top - n);
      return PCRC;
    }
  }
}


static StkId callrethooks (ekto_State *L, StkId firstResult) {
  ptrdiff_t fr = savestack(L, firstResult);  /* next call may change stack */
  ektoD_callhook(L, EKTO_HOOKRET, -1);
  if (f_isEkto(L->ci)) {  /* Ekto function? */
    while ((L->hookmask & EKTO_MASKRET) && L->ci->tailcalls--) /* tail calls */
      ektoD_callhook(L, EKTO_HOOKTAILRET, -1);
  }
  return restorestack(L, fr);
}


int ektoD_poscall (ekto_State *L, StkId firstResult) {
  StkId res;
  int wanted, i;
  CallInfo *ci;
  if (L->hookmask & EKTO_MASKRET)
    firstResult = callrethooks(L, firstResult);
  ci = L->ci--;
  res = ci->func;  /* res == final position of 1st result */
  wanted = ci->nresults;
  L->base = (ci - 1)->base;  /* restore base */
  L->savedpc = (ci - 1)->savedpc;  /* restore savedpc */
  /* move results to correct place */
  for (i = wanted; i != 0 && firstResult < L->top; i--)
    setobjs2s(L, res++, firstResult++);
  while (i-- > 0)
    setnilvalue(res++);
  L->top = res;
  return (wanted - EKTO_MULTRET);  /* 0 iff wanted == EKTO_MULTRET */
}


/*
** Call a function (C or Ekto). The function to be called is at *func.
** The arguments are on the stack, right after the function.
** When returns, all the results are on the stack, starting at the original
** function position.
*/ 
void ektoD_call (ekto_State *L, StkId func, int nResults) {
  if (++L->nCcalls >= EKTOI_MAXCCALLS) {
    if (L->nCcalls == EKTOI_MAXCCALLS)
      ektoG_runerror(L, "C stack overflow");
    else if (L->nCcalls >= (EKTOI_MAXCCALLS + (EKTOI_MAXCCALLS>>3)))
      ektoD_throw(L, EKTO_ERRERR);  /* error while handing stack error */
  }
  if (ektoD_precall(L, func, nResults) == PCREKTO)  /* is a Ekto function? */
    ektoV_execute(L, 1);  /* call it */
  L->nCcalls--;
  ektoC_checkGC(L);
}


static void resume (ekto_State *L, void *ud) {
  StkId firstArg = cast(StkId, ud);
  CallInfo *ci = L->ci;
  if (L->status == 0) {  /* start coroutine? */
    ekto_assert(ci == L->base_ci && firstArg > L->base);
    if (ektoD_precall(L, firstArg - 1, EKTO_MULTRET) != PCREKTO)
      return;
  }
  else {  /* resuming from previous yield */
    ekto_assert(L->status == EKTO_YIELD);
    L->status = 0;
    if (!f_isEkto(ci)) {  /* `common' yield? */
      /* finish interrupted execution of `OP_CALL' */
      ekto_assert(GET_OPCODE(*((ci-1)->savedpc - 1)) == OP_CALL ||
                 GET_OPCODE(*((ci-1)->savedpc - 1)) == OP_TAILCALL);
      if (ektoD_poscall(L, firstArg))  /* complete it... */
        L->top = L->ci->top;  /* and correct top if not multiple results */
    }
    else  /* yielded inside a hook: just continue its execution */
      L->base = L->ci->base;
  }
  ektoV_execute(L, cast_int(L->ci - L->base_ci));
}


static int resume_error (ekto_State *L, const char *msg) {
  L->top = L->ci->base;
  setsvalue2s(L, L->top, ektoS_new(L, msg));
  incr_top(L);
  ekto_unlock(L);
  return EKTO_ERRRUN;
}


EKTO_API int ekto_resume (ekto_State *L, int nargs) {
  int status;
  ekto_lock(L);
  if (L->status != EKTO_YIELD && (L->status != 0 || L->ci != L->base_ci))
      return resume_error(L, "cannot resume non-suspended coroutine");
  if (L->nCcalls >= EKTOI_MAXCCALLS)
    return resume_error(L, "C stack overflow");
  ektoi_userstateresume(L, nargs);
  ekto_assert(L->errfunc == 0);
  L->baseCcalls = ++L->nCcalls;
  status = ektoD_rawrunprotected(L, resume, L->top - nargs);
  if (status != 0) {  /* error? */
    L->status = cast_byte(status);  /* mark thread as `dead' */
    ektoD_seterrorobj(L, status, L->top);
    L->ci->top = L->top;
  }
  else {
    ekto_assert(L->nCcalls == L->baseCcalls);
    status = L->status;
  }
  --L->nCcalls;
  ekto_unlock(L);
  return status;
}


EKTO_API int ekto_yield (ekto_State *L, int nresults) {
  ektoi_userstateyield(L, nresults);
  ekto_lock(L);
  if (L->nCcalls > L->baseCcalls)
    ektoG_runerror(L, "attempt to yield across metamethod/C-call boundary");
  L->base = L->top - nresults;  /* protect stack slots below */
  L->status = EKTO_YIELD;
  ekto_unlock(L);
  return -1;
}


int ektoD_pcall (ekto_State *L, Pfunc func, void *u,
                ptrdiff_t old_top, ptrdiff_t ef) {
  int status;
  unsigned short oldnCcalls = L->nCcalls;
  ptrdiff_t old_ci = saveci(L, L->ci);
  lu_byte old_allowhooks = L->allowhook;
  ptrdiff_t old_errfunc = L->errfunc;
  L->errfunc = ef;
  status = ektoD_rawrunprotected(L, func, u);
  if (status != 0) {  /* an error occurred? */
    StkId oldtop = restorestack(L, old_top);
    ektoF_close(L, oldtop);  /* close eventual pending closures */
    ektoD_seterrorobj(L, status, oldtop);
    L->nCcalls = oldnCcalls;
    L->ci = restoreci(L, old_ci);
    L->base = L->ci->base;
    L->savedpc = L->ci->savedpc;
    L->allowhook = old_allowhooks;
    restore_stack_limit(L);
  }
  L->errfunc = old_errfunc;
  return status;
}



/*
** Execute a protected parser.
*/
struct SParser {  /* data to `f_parser' */
  ZIO *z;
  Mbuffer buff;  /* buffer to be used by the scanner */
  const char *name;
};

static void f_parser (ekto_State *L, void *ud) {
  int i;
  Proto *tf;
  Closure *cl;
  struct SParser *p = cast(struct SParser *, ud);
  int c = ektoZ_lookahead(p->z);
  ektoC_checkGC(L);
  tf = ((c == EKTO_SIGNATURE[0]) ? ektoU_undump : ektoY_parser)(L, p->z,
                                                             &p->buff, p->name);
  cl = ektoF_newLclosure(L, tf->nups, hvalue(gt(L)));
  cl->l.p = tf;
  for (i = 0; i < tf->nups; i++)  /* initialize eventual upvalues */
    cl->l.upvals[i] = ektoF_newupval(L);
  setclvalue(L, L->top, cl);
  incr_top(L);
}


int ektoD_protectedparser (ekto_State *L, ZIO *z, const char *name) {
  struct SParser p;
  int status;
  p.z = z; p.name = name;
  ektoZ_initbuffer(L, &p.buff);
  status = ektoD_pcall(L, f_parser, &p, savestack(L, L->top), L->errfunc);
  ektoZ_freebuffer(L, &p.buff);
  return status;
}


