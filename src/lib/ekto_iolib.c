/*
** $Id: liolib.c,v 2.73.1.3 2008/01/18 17:47:43 roberto Exp $
** Standard I/O (and system) library
** See Copyright Notice in ekto.h
*/


#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define liolib_c
#define EKTO_LIB

#include "Ekto.h"

#include "Ekto_Aux.h"
#include "Ekto_Lib.h"



#define IO_INPUT	1
#define IO_OUTPUT	2


static const char *const fnames[] = {"input", "output"};


static int pushresult (ekto_State *L, int i, const char *filename) {
  int en = errno;  /* calls to Ekto API may change this value */
  if (i) {
    ekto_pushboolean(L, 1);
    return 1;
  }
  else {
    ekto_pushnil(L);
    if (filename)
      ekto_pushfstring(L, "%s: %s", filename, strerror(en));
    else
      ekto_pushfstring(L, "%s", strerror(en));
    ekto_pushinteger(L, en);
    return 3;
  }
}


static void fileerror (ekto_State *L, int arg, const char *filename) {
  ekto_pushfstring(L, "%s: %s", filename, strerror(errno));
  ektoL_argerror(L, arg, ekto_tostring(L, -1));
}


#define tofilep(L)	((FILE **)ektoL_checkudata(L, 1, EKTO_FILEHANDLE))


static int io_type (ekto_State *L) {
  void *ud;
  ektoL_checkany(L, 1);
  ud = ekto_touserdata(L, 1);
  ekto_getfield(L, EKTO_REGISTRYINDEX, EKTO_FILEHANDLE);
  if (ud == NULL || !ekto_getmetatable(L, 1) || !ekto_rawequal(L, -2, -1))
    ekto_pushnil(L);  /* not a file */
  else if (*((FILE **)ud) == NULL)
    ekto_pushliteral(L, "closed file");
  else
    ekto_pushliteral(L, "file");
  return 1;
}


static FILE *tofile (ekto_State *L) {
  FILE **f = tofilep(L);
  if (*f == NULL)
    ektoL_error(L, "attempt to use a closed file");
  return *f;
}



/*
** When creating file handles, always creates a `closed' file handle
** before opening the actual file; so, if there is a memory error, the
** file is not left opened.
*/
static FILE **newfile (ekto_State *L) {
  FILE **pf = (FILE **)ekto_newuserdata(L, sizeof(FILE *));
  *pf = NULL;  /* file handle is currently `closed' */
  ektoL_getmetatable(L, EKTO_FILEHANDLE);
  ekto_setmetatable(L, -2);
  return pf;
}


/*
** function to (not) close the standard files stdin, stdout, and stderr
*/
static int io_noclose (ekto_State *L) {
  ekto_pushnil(L);
  ekto_pushliteral(L, "cannot close standard file");
  return 2;
}


/*
** function to close 'popen' files
*/
static int io_pclose (ekto_State *L) {
  FILE **p = tofilep(L);
  int ok = ekto_pclose(L, *p);
  *p = NULL;
  return pushresult(L, ok, NULL);
}


/*
** function to close regular files
*/
static int io_fclose (ekto_State *L) {
  FILE **p = tofilep(L);
  int ok = (fclose(*p) == 0);
  *p = NULL;
  return pushresult(L, ok, NULL);
}


static int aux_close (ekto_State *L) {
  ekto_getfenv(L, 1);
  ekto_getfield(L, -1, "__close");
  return (ekto_tocfunction(L, -1))(L);
}


static int io_close (ekto_State *L) {
  if (ekto_isnone(L, 1))
    ekto_rawgeti(L, EKTO_ENVIRONINDEX, IO_OUTPUT);
  tofile(L);  /* make sure argument is a file */
  return aux_close(L);
}


static int io_gc (ekto_State *L) {
  FILE *f = *tofilep(L);
  /* ignore closed files */
  if (f != NULL)
    aux_close(L);
  return 0;
}


static int io_tostring (ekto_State *L) {
  FILE *f = *tofilep(L);
  if (f == NULL)
    ekto_pushliteral(L, "file (closed)");
  else
    ekto_pushfstring(L, "file (%p)", f);
  return 1;
}


static int io_open (ekto_State *L) {
  const char *filename = ektoL_checkstring(L, 1);
  const char *mode = ektoL_optstring(L, 2, "r");
  FILE **pf = newfile(L);
  *pf = fopen(filename, mode);
  return (*pf == NULL) ? pushresult(L, 0, filename) : 1;
}


/*
** this function has a separated environment, which defines the
** correct __close for 'popen' files
*/
static int io_popen (ekto_State *L) {
  const char *filename = ektoL_checkstring(L, 1);
  const char *mode = ektoL_optstring(L, 2, "r");
  FILE **pf = newfile(L);
  *pf = ekto_popen(L, filename, mode);
  return (*pf == NULL) ? pushresult(L, 0, filename) : 1;
}


static int io_tmpfile (ekto_State *L) {
  FILE **pf = newfile(L);
  *pf = tmpfile();
  return (*pf == NULL) ? pushresult(L, 0, NULL) : 1;
}


static FILE *getiofile (ekto_State *L, int findex) {
  FILE *f;
  ekto_rawgeti(L, EKTO_ENVIRONINDEX, findex);
  f = *(FILE **)ekto_touserdata(L, -1);
  if (f == NULL)
    ektoL_error(L, "standard %s file is closed", fnames[findex - 1]);
  return f;
}


static int g_iofile (ekto_State *L, int f, const char *mode) {
  if (!ekto_isnoneornil(L, 1)) {
    const char *filename = ekto_tostring(L, 1);
    if (filename) {
      FILE **pf = newfile(L);
      *pf = fopen(filename, mode);
      if (*pf == NULL)
        fileerror(L, 1, filename);
    }
    else {
      tofile(L);  /* check that it's a valid file handle */
      ekto_pushvalue(L, 1);
    }
    ekto_rawseti(L, EKTO_ENVIRONINDEX, f);
  }
  /* return current value */
  ekto_rawgeti(L, EKTO_ENVIRONINDEX, f);
  return 1;
}


static int io_input (ekto_State *L) {
  return g_iofile(L, IO_INPUT, "r");
}


static int io_output (ekto_State *L) {
  return g_iofile(L, IO_OUTPUT, "w");
}


static int io_readline (ekto_State *L);


static void aux_lines (ekto_State *L, int idx, int toclose) {
  ekto_pushvalue(L, idx);
  ekto_pushboolean(L, toclose);  /* close/not close file when finished */
  ekto_pushcclosure(L, io_readline, 2);
}


static int f_lines (ekto_State *L) {
  tofile(L);  /* check that it's a valid file handle */
  aux_lines(L, 1, 0);
  return 1;
}


static int io_lines (ekto_State *L) {
  if (ekto_isnoneornil(L, 1)) {  /* no arguments? */
    /* will iterate over default input */
    ekto_rawgeti(L, EKTO_ENVIRONINDEX, IO_INPUT);
    return f_lines(L);
  }
  else {
    const char *filename = ektoL_checkstring(L, 1);
    FILE **pf = newfile(L);
    *pf = fopen(filename, "r");
    if (*pf == NULL)
      fileerror(L, 1, filename);
    aux_lines(L, ekto_gettop(L), 1);
    return 1;
  }
}


/*
** {======================================================
** READ
** =======================================================
*/


static int read_number (ekto_State *L, FILE *f) {
  ekto_Number d;
  if (fscanf(f, EKTO_NUMBER_SCAN, &d) == 1) {
    ekto_pushnumber(L, d);
    return 1;
  }
  else return 0;  /* read fails */
}


static int test_eof (ekto_State *L, FILE *f) {
  int c = getc(f);
  ungetc(c, f);
  ekto_pushlstring(L, NULL, 0);
  return (c != EOF);
}


static int read_line (ekto_State *L, FILE *f) {
  ektoL_Buffer b;
  ektoL_buffinit(L, &b);
  for (;;) {
    size_t l;
    char *p = ektoL_prepbuffer(&b);
    if (fgets(p, EKTOL_BUFFERSIZE, f) == NULL) {  /* eof? */
      ektoL_pushresult(&b);  /* close buffer */
      return (ekto_objlen(L, -1) > 0);  /* check whether read something */
    }
    l = strlen(p);
    if (l == 0 || p[l-1] != '\n')
      ektoL_addsize(&b, l);
    else {
      ektoL_addsize(&b, l - 1);  /* do not include `eol' */
      ektoL_pushresult(&b);  /* close buffer */
      return 1;  /* read at least an `eol' */
    }
  }
}


static int read_chars (ekto_State *L, FILE *f, size_t n) {
  size_t rlen;  /* how much to read */
  size_t nr;  /* number of chars actually read */
  ektoL_Buffer b;
  ektoL_buffinit(L, &b);
  rlen = EKTOL_BUFFERSIZE;  /* try to read that much each time */
  do {
    char *p = ektoL_prepbuffer(&b);
    if (rlen > n) rlen = n;  /* cannot read more than asked */
    nr = fread(p, sizeof(char), rlen, f);
    ektoL_addsize(&b, nr);
    n -= nr;  /* still have to read `n' chars */
  } while (n > 0 && nr == rlen);  /* until end of count or eof */
  ektoL_pushresult(&b);  /* close buffer */
  return (n == 0 || ekto_objlen(L, -1) > 0);
}


static int g_read (ekto_State *L, FILE *f, int first) {
  int nargs = ekto_gettop(L) - 1;
  int success;
  int n;
  clearerr(f);
  if (nargs == 0) {  /* no arguments? */
    success = read_line(L, f);
    n = first+1;  /* to return 1 result */
  }
  else {  /* ensure stack space for all results and for auxlib's buffer */
    ektoL_checkstack(L, nargs+EKTO_MINSTACK, "too many arguments");
    success = 1;
    for (n = first; nargs-- && success; n++) {
      if (ekto_type(L, n) == EKTO_TNUMBER) {
        size_t l = (size_t)ekto_tointeger(L, n);
        success = (l == 0) ? test_eof(L, f) : read_chars(L, f, l);
      }
      else {
        const char *p = ekto_tostring(L, n);
        ektoL_argcheck(L, p && p[0] == '*', n, "invalid option");
        switch (p[1]) {
          case 'n':  /* number */
            success = read_number(L, f);
            break;
          case 'l':  /* line */
            success = read_line(L, f);
            break;
          case 'a':  /* file */
            read_chars(L, f, ~((size_t)0));  /* read MAX_SIZE_T chars */
            success = 1; /* always success */
            break;
          default:
            return ektoL_argerror(L, n, "invalid format");
        }
      }
    }
  }
  if (ferror(f))
    return pushresult(L, 0, NULL);
  if (!success) {
    ekto_pop(L, 1);  /* remove last result */
    ekto_pushnil(L);  /* push nil instead */
  }
  return n - first;
}


static int io_read (ekto_State *L) {
  return g_read(L, getiofile(L, IO_INPUT), 1);
}


static int f_read (ekto_State *L) {
  return g_read(L, tofile(L), 2);
}


static int io_readline (ekto_State *L) {
  FILE *f = *(FILE **)ekto_touserdata(L, ekto_upvalueindex(1));
  int sucess;
  if (f == NULL)  /* file is already closed? */
    ektoL_error(L, "file is already closed");
  sucess = read_line(L, f);
  if (ferror(f))
    return ektoL_error(L, "%s", strerror(errno));
  if (sucess) return 1;
  else {  /* EOF */
    if (ekto_toboolean(L, ekto_upvalueindex(2))) {  /* generator created file? */
      ekto_settop(L, 0);
      ekto_pushvalue(L, ekto_upvalueindex(1));
      aux_close(L);  /* close it */
    }
    return 0;
  }
}

/* }====================================================== */


static int g_write (ekto_State *L, FILE *f, int arg) {
  int nargs = ekto_gettop(L) - 1;
  int status = 1;
  for (; nargs--; arg++) {
    if (ekto_type(L, arg) == EKTO_TNUMBER) {
      /* optimization: could be done exactly as for strings */
      status = status &&
          fprintf(f, EKTO_NUMBER_FMT, ekto_tonumber(L, arg)) > 0;
    }
    else {
      size_t l;
      const char *s = ektoL_checklstring(L, arg, &l);
      status = status && (fwrite(s, sizeof(char), l, f) == l);
    }
  }
  return pushresult(L, status, NULL);
}


static int io_write (ekto_State *L) {
  return g_write(L, getiofile(L, IO_OUTPUT), 1);
}


static int f_write (ekto_State *L) {
  return g_write(L, tofile(L), 2);
}


static int f_seek (ekto_State *L) {
  static const int mode[] = {SEEK_SET, SEEK_CUR, SEEK_END};
  static const char *const modenames[] = {"set", "cur", "end", NULL};
  FILE *f = tofile(L);
  int op = ektoL_checkoption(L, 2, "cur", modenames);
  long offset = ektoL_optlong(L, 3, 0);
  op = fseek(f, offset, mode[op]);
  if (op)
    return pushresult(L, 0, NULL);  /* error */
  else {
    ekto_pushinteger(L, ftell(f));
    return 1;
  }
}


static int f_setvbuf (ekto_State *L) {
  static const int mode[] = {_IONBF, _IOFBF, _IOLBF};
  static const char *const modenames[] = {"no", "full", "line", NULL};
  FILE *f = tofile(L);
  int op = ektoL_checkoption(L, 2, NULL, modenames);
  ekto_Integer sz = ektoL_optinteger(L, 3, EKTOL_BUFFERSIZE);
  int res = setvbuf(f, NULL, mode[op], sz);
  return pushresult(L, res == 0, NULL);
}



static int io_flush (ekto_State *L) {
  return pushresult(L, fflush(getiofile(L, IO_OUTPUT)) == 0, NULL);
}


static int f_flush (ekto_State *L) {
  return pushresult(L, fflush(tofile(L)) == 0, NULL);
}


static const ektoL_Reg iolib[] = {
  {"close", io_close},
  {"flush", io_flush},
  {"input", io_input},
  {"lines", io_lines},
  {"open", io_open},
  {"output", io_output},
  {"popen", io_popen},
  {"read", io_read},
  {"tmpfile", io_tmpfile},
  {"type", io_type},
  {"write", io_write},
  {NULL, NULL}
};


static const ektoL_Reg flib[] = {
  {"close", io_close},
  {"flush", f_flush},
  {"lines", f_lines},
  {"read", f_read},
  {"seek", f_seek},
  {"setvbuf", f_setvbuf},
  {"write", f_write},
  {"__gc", io_gc},
  {"__tostring", io_tostring},
  {NULL, NULL}
};


static void createmeta (ekto_State *L) {
  ektoL_newmetatable(L, EKTO_FILEHANDLE);  /* create metatable for file handles */
  ekto_pushvalue(L, -1);  /* push metatable */
  ekto_setfield(L, -2, "__index");  /* metatable.__index = metatable */
  ektoL_register(L, NULL, flib);  /* file methods */
}


static void createstdfile (ekto_State *L, FILE *f, int k, const char *fname) {
  *newfile(L) = f;
  if (k > 0) {
    ekto_pushvalue(L, -1);
    ekto_rawseti(L, EKTO_ENVIRONINDEX, k);
  }
  ekto_pushvalue(L, -2);  /* copy environment */
  ekto_setfenv(L, -2);  /* set it */
  ekto_setfield(L, -3, fname);
}


static void newfenv (ekto_State *L, ekto_CFunction cls) {
  ekto_createtable(L, 0, 1);
  ekto_pushcfunction(L, cls);
  ekto_setfield(L, -2, "__close");
}


EKTOLIB_API int ektoopen_io (ekto_State *L) {
  createmeta(L);
  /* create (private) environment (with fields IO_INPUT, IO_OUTPUT, __close) */
  newfenv(L, io_fclose);
  ekto_replace(L, EKTO_ENVIRONINDEX);
  /* open library */
  ektoL_register(L, EKTO_IOLIBNAME, iolib);
  /* create (and set) default files */
  newfenv(L, io_noclose);  /* close function for default files */
  createstdfile(L, stdin, IO_INPUT, "stdin");
  createstdfile(L, stdout, IO_OUTPUT, "stdout");
  createstdfile(L, stderr, 0, "stderr");
  ekto_pop(L, 1);  /* pop environment for default files */
  ekto_getfield(L, -1, "popen");
  newfenv(L, io_pclose);  /* create environment for 'popen' */
  ekto_setfenv(L, -2);  /* set fenv for 'popen' */
  ekto_pop(L, 1);  /* pop 'popen' */
  return 1;
}

