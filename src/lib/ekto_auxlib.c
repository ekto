/*
** $Id: lauxlib.c,v 1.159.1.3 2008/01/21 13:20:51 roberto Exp $
** Auxiliary functions for building Ekto libraries
** See Copyright Notice in ekto.h
*/


#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* This file uses only the official API of Ekto.
** Any function declared here could be written as an application function.
*/

#define lauxlib_c
#define EKTO_LIB

#include "Ekto.h"

#include "Ekto_Aux.h"


#define FREELIST_REF	0	/* free list of references */


/* convert a stack index to positive */
#define abs_index(L, i)		((i) > 0 || (i) <= EKTO_REGISTRYINDEX ? (i) : \
					ekto_gettop(L) + (i) + 1)


/*
** {======================================================
** Error-report functions
** =======================================================
*/


EKTOLIB_API int ektoL_argerror (ekto_State *L, int narg, const char *extramsg) {
  ekto_Debug ar;
  if (!ekto_getstack(L, 0, &ar))  /* no stack frame? */
    return ektoL_error(L, "bad argument #%d (%s)", narg, extramsg);
  ekto_getinfo(L, "n", &ar);
  if (strcmp(ar.namewhat, "method") == 0) {
    narg--;  /* do not count `self' */
    if (narg == 0)  /* error is in the self argument itself? */
      return ektoL_error(L, "calling " EKTO_QS " on bad self (%s)",
                           ar.name, extramsg);
  }
  if (ar.name == NULL)
    ar.name = "?";
  return ektoL_error(L, "bad argument #%d to " EKTO_QS " (%s)",
                        narg, ar.name, extramsg);
}


EKTOLIB_API int ektoL_typerror (ekto_State *L, int narg, const char *tname) {
  const char *msg = ekto_pushfstring(L, "%s expected, got %s",
                                    tname, ektoL_typename(L, narg));
  return ektoL_argerror(L, narg, msg);
}


static void tag_error (ekto_State *L, int narg, int tag) {
  ektoL_typerror(L, narg, ekto_typename(L, tag));
}


EKTOLIB_API void ektoL_where (ekto_State *L, int level) {
  ekto_Debug ar;
  if (ekto_getstack(L, level, &ar)) {  /* check function at level */
    ekto_getinfo(L, "Sl", &ar);  /* get info about it */
    if (ar.currentline > 0) {  /* is there info? */
      ekto_pushfstring(L, "%s:%d: ", ar.short_src, ar.currentline);
      return;
    }
  }
  ekto_pushliteral(L, "");  /* else, no information available... */
}


EKTOLIB_API int ektoL_error (ekto_State *L, const char *fmt, ...) {
  va_list argp;
  va_start(argp, fmt);
  ektoL_where(L, 1);
  ekto_pushvfstring(L, fmt, argp);
  va_end(argp);
  ekto_concat(L, 2);
  return ekto_error(L);
}

/* }====================================================== */


EKTOLIB_API int ektoL_checkoption (ekto_State *L, int narg, const char *def,
                                 const char *const lst[]) {
  const char *name = (def) ? ektoL_optstring(L, narg, def) :
                             ektoL_checkstring(L, narg);
  int i;
  for (i=0; lst[i]; i++)
    if (strcmp(lst[i], name) == 0)
      return i;
  return ektoL_argerror(L, narg,
                       ekto_pushfstring(L, "invalid option " EKTO_QS, name));
}


EKTOLIB_API int ektoL_newmetatable (ekto_State *L, const char *tname) {
  ekto_getfield(L, EKTO_REGISTRYINDEX, tname);  /* get registry.name */
  if (!ekto_isnil(L, -1))  /* name already in use? */
    return 0;  /* leave previous value on top, but return 0 */
  ekto_pop(L, 1);
  ekto_newtable(L);  /* create metatable */
  ekto_pushvalue(L, -1);
  ekto_setfield(L, EKTO_REGISTRYINDEX, tname);  /* registry.name = metatable */
  return 1;
}


EKTOLIB_API void *ektoL_checkudata (ekto_State *L, int ud, const char *tname) {
  void *p = ekto_touserdata(L, ud);
  if (p != NULL) {  /* value is a userdata? */
    if (ekto_getmetatable(L, ud)) {  /* does it have a metatable? */
      ekto_getfield(L, EKTO_REGISTRYINDEX, tname);  /* get correct metatable */
      if (ekto_rawequal(L, -1, -2)) {  /* does it have the correct mt? */
        ekto_pop(L, 2);  /* remove both metatables */
        return p;
      }
    }
  }
  ektoL_typerror(L, ud, tname);  /* else error */
  return NULL;  /* to avoid warnings */
}


EKTOLIB_API void ektoL_checkstack (ekto_State *L, int space, const char *mes) {
  if (!ekto_checkstack(L, space))
    ektoL_error(L, "stack overflow (%s)", mes);
}


EKTOLIB_API void ektoL_checktype (ekto_State *L, int narg, int t) {
  if (ekto_type(L, narg) != t)
    tag_error(L, narg, t);
}


EKTOLIB_API void ektoL_checkany (ekto_State *L, int narg) {
  if (ekto_type(L, narg) == EKTO_TNONE)
    ektoL_argerror(L, narg, "value expected");
}


EKTOLIB_API const char *ektoL_checklstring (ekto_State *L, int narg, size_t *len) {
  const char *s = ekto_tolstring(L, narg, len);
  if (!s) tag_error(L, narg, EKTO_TSTRING);
  return s;
}


EKTOLIB_API const char *ektoL_optlstring (ekto_State *L, int narg,
                                        const char *def, size_t *len) {
  if (ekto_isnoneornil(L, narg)) {
    if (len)
      *len = (def ? strlen(def) : 0);
    return def;
  }
  else return ektoL_checklstring(L, narg, len);
}


EKTOLIB_API ekto_Number ektoL_checknumber (ekto_State *L, int narg) {
  ekto_Number d = ekto_tonumber(L, narg);
  if (d == 0 && !ekto_isnumber(L, narg))  /* avoid extra test when d is not 0 */
    tag_error(L, narg, EKTO_TNUMBER);
  return d;
}


EKTOLIB_API ekto_Number ektoL_optnumber (ekto_State *L, int narg, ekto_Number def) {
  return ektoL_opt(L, ektoL_checknumber, narg, def);
}


EKTOLIB_API ekto_Integer ektoL_checkinteger (ekto_State *L, int narg) {
  ekto_Integer d = ekto_tointeger(L, narg);
  if (d == 0 && !ekto_isnumber(L, narg))  /* avoid extra test when d is not 0 */
    tag_error(L, narg, EKTO_TNUMBER);
  return d;
}


EKTOLIB_API ekto_Integer ektoL_optinteger (ekto_State *L, int narg,
                                                      ekto_Integer def) {
  return ektoL_opt(L, ektoL_checkinteger, narg, def);
}


EKTOLIB_API int ektoL_getmetafield (ekto_State *L, int obj, const char *event) {
  if (!ekto_getmetatable(L, obj))  /* no metatable? */
    return 0;
  ekto_pushstring(L, event);
  ekto_rawget(L, -2);
  if (ekto_isnil(L, -1)) {
    ekto_pop(L, 2);  /* remove metatable and metafield */
    return 0;
  }
  else {
    ekto_remove(L, -2);  /* remove only metatable */
    return 1;
  }
}


EKTOLIB_API int ektoL_callmeta (ekto_State *L, int obj, const char *event) {
  obj = abs_index(L, obj);
  if (!ektoL_getmetafield(L, obj, event))  /* no metafield? */
    return 0;
  ekto_pushvalue(L, obj);
  ekto_call(L, 1, 1);
  return 1;
}


EKTOLIB_API void (ektoL_register) (ekto_State *L, const char *libname,
                                const ektoL_Reg *l) {
  ektoI_openlib(L, libname, l, 0);
}


static int libsize (const ektoL_Reg *l) {
  int size = 0;
  for (; l->name; l++) size++;
  return size;
}


EKTOLIB_API void ektoI_openlib (ekto_State *L, const char *libname,
                              const ektoL_Reg *l, int nup) {
  if (libname) {
    int size = libsize(l);
    /* check whether lib already exists */
    ektoL_findtable(L, EKTO_REGISTRYINDEX, "_LOADED", 1);
    ekto_getfield(L, -1, libname);  /* get _LOADED[libname] */
    if (!ekto_istable(L, -1)) {  /* not found? */
      ekto_pop(L, 1);  /* remove previous result */
      /* try global variable (and create one if it does not exist) */
      if (ektoL_findtable(L, EKTO_GLOBALSINDEX, libname, size) != NULL)
        ektoL_error(L, "name conflict for module " EKTO_QS, libname);
      ekto_pushvalue(L, -1);
      ekto_setfield(L, -3, libname);  /* _LOADED[libname] = new table */
    }
    ekto_remove(L, -2);  /* remove _LOADED table */
    ekto_insert(L, -(nup+1));  /* move library table to below upvalues */
  }
  for (; l->name; l++) {
    int i;
    for (i=0; i<nup; i++)  /* copy upvalues to the top */
      ekto_pushvalue(L, -nup);
    ekto_pushcclosure(L, l->func, nup);
    ekto_setfield(L, -(nup+2), l->name);
  }
  ekto_pop(L, nup);  /* remove upvalues */
}



/*
** {======================================================
** getn-setn: size for arrays
** =======================================================
*/

#if defined(EKTO_COMPAT_GETN)

static int checkint (ekto_State *L, int topop) {
  int n = (ekto_type(L, -1) == EKTO_TNUMBER) ? ekto_tointeger(L, -1) : -1;
  ekto_pop(L, topop);
  return n;
}


static void getsizes (ekto_State *L) {
  ekto_getfield(L, EKTO_REGISTRYINDEX, "EKTO_SIZES");
  if (ekto_isnil(L, -1)) {  /* no `size' table? */
    ekto_pop(L, 1);  /* remove nil */
    ekto_newtable(L);  /* create it */
    ekto_pushvalue(L, -1);  /* `size' will be its own metatable */
    ekto_setmetatable(L, -2);
    ekto_pushliteral(L, "kv");
    ekto_setfield(L, -2, "__mode");  /* metatable(N).__mode = "kv" */
    ekto_pushvalue(L, -1);
    ekto_setfield(L, EKTO_REGISTRYINDEX, "EKTO_SIZES");  /* store in register */
  }
}


EKTOLIB_API void ektoL_setn (ekto_State *L, int t, int n) {
  t = abs_index(L, t);
  ekto_pushliteral(L, "n");
  ekto_rawget(L, t);
  if (checkint(L, 1) >= 0) {  /* is there a numeric field `n'? */
    ekto_pushliteral(L, "n");  /* use it */
    ekto_pushinteger(L, n);
    ekto_rawset(L, t);
  }
  else {  /* use `sizes' */
    getsizes(L);
    ekto_pushvalue(L, t);
    ekto_pushinteger(L, n);
    ekto_rawset(L, -3);  /* sizes[t] = n */
    ekto_pop(L, 1);  /* remove `sizes' */
  }
}


EKTOLIB_API int ektoL_getn (ekto_State *L, int t) {
  int n;
  t = abs_index(L, t);
  ekto_pushliteral(L, "n");  /* try t.n */
  ekto_rawget(L, t);
  if ((n = checkint(L, 1)) >= 0) return n;
  getsizes(L);  /* else try sizes[t] */
  ekto_pushvalue(L, t);
  ekto_rawget(L, -2);
  if ((n = checkint(L, 2)) >= 0) return n;
  return (int)ekto_objlen(L, t);
}

#endif

/* }====================================================== */



EKTOLIB_API const char *ektoL_gsub (ekto_State *L, const char *s, const char *p,
                                                               const char *r) {
  const char *wild;
  size_t l = strlen(p);
  ektoL_Buffer b;
  ektoL_buffinit(L, &b);
  while ((wild = strstr(s, p)) != NULL) {
    ektoL_addlstring(&b, s, wild - s);  /* push prefix */
    ektoL_addstring(&b, r);  /* push replacement in place of pattern */
    s = wild + l;  /* continue after `p' */
  }
  ektoL_addstring(&b, s);  /* push last suffix */
  ektoL_pushresult(&b);
  return ekto_tostring(L, -1);
}


EKTOLIB_API const char *ektoL_findtable (ekto_State *L, int idx,
                                       const char *fname, int szhint) {
  const char *e;
  ekto_pushvalue(L, idx);
  do {
    e = strchr(fname, '.');
    if (e == NULL) e = fname + strlen(fname);
    ekto_pushlstring(L, fname, e - fname);
    ekto_rawget(L, -2);
    if (ekto_isnil(L, -1)) {  /* no such field? */
      ekto_pop(L, 1);  /* remove this nil */
      ekto_createtable(L, 0, (*e == '.' ? 1 : szhint)); /* new table for field */
      ekto_pushlstring(L, fname, e - fname);
      ekto_pushvalue(L, -2);
      ekto_settable(L, -4);  /* set new table into field */
    }
    else if (!ekto_istable(L, -1)) {  /* field has a non-table value? */
      ekto_pop(L, 2);  /* remove table and value */
      return fname;  /* return problematic part of the name */
    }
    ekto_remove(L, -2);  /* remove previous table */
    fname = e + 1;
  } while (*e == '.');
  return NULL;
}



/*
** {======================================================
** Generic Buffer manipulation
** =======================================================
*/


#define bufflen(B)	((B)->p - (B)->buffer)
#define bufffree(B)	((size_t)(EKTOL_BUFFERSIZE - bufflen(B)))

#define LIMIT	(EKTO_MINSTACK/2)


static int emptybuffer (ektoL_Buffer *B) {
  size_t l = bufflen(B);
  if (l == 0) return 0;  /* put nothing on stack */
  else {
    ekto_pushlstring(B->L, B->buffer, l);
    B->p = B->buffer;
    B->lvl++;
    return 1;
  }
}


static void adjuststack (ektoL_Buffer *B) {
  if (B->lvl > 1) {
    ekto_State *L = B->L;
    int toget = 1;  /* number of levels to concat */
    size_t toplen = ekto_strlen(L, -1);
    do {
      size_t l = ekto_strlen(L, -(toget+1));
      if (B->lvl - toget + 1 >= LIMIT || toplen > l) {
        toplen += l;
        toget++;
      }
      else break;
    } while (toget < B->lvl);
    ekto_concat(L, toget);
    B->lvl = B->lvl - toget + 1;
  }
}


EKTOLIB_API char *ektoL_prepbuffer (ektoL_Buffer *B) {
  if (emptybuffer(B))
    adjuststack(B);
  return B->buffer;
}


EKTOLIB_API void ektoL_addlstring (ektoL_Buffer *B, const char *s, size_t l) {
  while (l--)
    ektoL_addchar(B, *s++);
}


EKTOLIB_API void ektoL_addstring (ektoL_Buffer *B, const char *s) {
  ektoL_addlstring(B, s, strlen(s));
}


EKTOLIB_API void ektoL_pushresult (ektoL_Buffer *B) {
  emptybuffer(B);
  ekto_concat(B->L, B->lvl);
  B->lvl = 1;
}


EKTOLIB_API void ektoL_addvalue (ektoL_Buffer *B) {
  ekto_State *L = B->L;
  size_t vl;
  const char *s = ekto_tolstring(L, -1, &vl);
  if (vl <= bufffree(B)) {  /* fit into buffer? */
    memcpy(B->p, s, vl);  /* put it there */
    B->p += vl;
    ekto_pop(L, 1);  /* remove from stack */
  }
  else {
    if (emptybuffer(B))
      ekto_insert(L, -2);  /* put buffer before new value */
    B->lvl++;  /* add new value into B stack */
    adjuststack(B);
  }
}


EKTOLIB_API void ektoL_buffinit (ekto_State *L, ektoL_Buffer *B) {
  B->L = L;
  B->p = B->buffer;
  B->lvl = 0;
}

/* }====================================================== */


EKTOLIB_API int ektoL_ref (ekto_State *L, int t) {
  int ref;
  t = abs_index(L, t);
  if (ekto_isnil(L, -1)) {
    ekto_pop(L, 1);  /* remove from stack */
    return EKTO_REFNIL;  /* `nil' has a unique fixed reference */
  }
  ekto_rawgeti(L, t, FREELIST_REF);  /* get first free element */
  ref = (int)ekto_tointeger(L, -1);  /* ref = t[FREELIST_REF] */
  ekto_pop(L, 1);  /* remove it from stack */
  if (ref != 0) {  /* any free element? */
    ekto_rawgeti(L, t, ref);  /* remove it from list */
    ekto_rawseti(L, t, FREELIST_REF);  /* (t[FREELIST_REF] = t[ref]) */
  }
  else {  /* no free elements */
    ref = (int)ekto_objlen(L, t);
    ref++;  /* create new reference */
  }
  ekto_rawseti(L, t, ref);
  return ref;
}


EKTOLIB_API void ektoL_unref (ekto_State *L, int t, int ref) {
  if (ref >= 0) {
    t = abs_index(L, t);
    ekto_rawgeti(L, t, FREELIST_REF);
    ekto_rawseti(L, t, ref);  /* t[ref] = t[FREELIST_REF] */
    ekto_pushinteger(L, ref);
    ekto_rawseti(L, t, FREELIST_REF);  /* t[FREELIST_REF] = ref */
  }
}



/*
** {======================================================
** Load functions
** =======================================================
*/

typedef struct LoadF {
  int extraline;
  FILE *f;
  char buff[EKTOL_BUFFERSIZE];
} LoadF;


static const char *getF (ekto_State *L, void *ud, size_t *size) {
  LoadF *lf = (LoadF *)ud;
  (void)L;
  if (lf->extraline) {
    lf->extraline = 0;
    *size = 1;
    return "\n";
  }
  if (feof(lf->f)) return NULL;
  *size = fread(lf->buff, 1, sizeof(lf->buff), lf->f);
  return (*size > 0) ? lf->buff : NULL;
}


static int errfile (ekto_State *L, const char *what, int fnameindex) {
  const char *serr = strerror(errno);
  const char *filename = ekto_tostring(L, fnameindex) + 1;
  ekto_pushfstring(L, "cannot %s %s: %s", what, filename, serr);
  ekto_remove(L, fnameindex);
  return EKTO_ERRFILE;
}


EKTOLIB_API int ektoL_loadfile (ekto_State *L, const char *filename) {
  LoadF lf;
  int status, readstatus;
  int c;
  int fnameindex = ekto_gettop(L) + 1;  /* index of filename on the stack */
  lf.extraline = 0;
  if (filename == NULL) {
    ekto_pushliteral(L, "=stdin");
    lf.f = stdin;
  }
  else {
    ekto_pushfstring(L, "@%s", filename);
    lf.f = fopen(filename, "r");
    if (lf.f == NULL) return errfile(L, "open", fnameindex);
  }
  c = getc(lf.f);
  if (c == '#') {  /* Unix exec. file? */
    lf.extraline = 1;
    while ((c = getc(lf.f)) != EOF && c != '\n') ;  /* skip first line */
    if (c == '\n') c = getc(lf.f);
  }
  if (c == EKTO_SIGNATURE[0] && filename) {  /* binary file? */
    lf.f = freopen(filename, "rb", lf.f);  /* reopen in binary mode */
    if (lf.f == NULL) return errfile(L, "reopen", fnameindex);
    /* skip eventual `#!...' */
   while ((c = getc(lf.f)) != EOF && c != EKTO_SIGNATURE[0]) ;
    lf.extraline = 0;
  }
  ungetc(c, lf.f);
  status = ekto_load(L, getF, &lf, ekto_tostring(L, -1));
  readstatus = ferror(lf.f);
  if (filename) fclose(lf.f);  /* close file (even in case of errors) */
  if (readstatus) {
    ekto_settop(L, fnameindex);  /* ignore results from `ekto_load' */
    return errfile(L, "read", fnameindex);
  }
  ekto_remove(L, fnameindex);
  return status;
}


typedef struct LoadS {
  const char *s;
  size_t size;
} LoadS;


static const char *getS (ekto_State *L, void *ud, size_t *size) {
  LoadS *ls = (LoadS *)ud;
  (void)L;
  if (ls->size == 0) return NULL;
  *size = ls->size;
  ls->size = 0;
  return ls->s;
}


EKTOLIB_API int ektoL_loadbuffer (ekto_State *L, const char *buff, size_t size,
                                const char *name) {
  LoadS ls;
  ls.s = buff;
  ls.size = size;
  return ekto_load(L, getS, &ls, name);
}


EKTOLIB_API int (ektoL_loadstring) (ekto_State *L, const char *s) {
  return ektoL_loadbuffer(L, s, strlen(s), s);
}



/* }====================================================== */


static void *l_alloc (void *ud, void *ptr, size_t osize, size_t nsize) {
  (void)ud;
  (void)osize;
  if (nsize == 0) {
    free(ptr);
    return NULL;
  }
  else
    return realloc(ptr, nsize);
}


static int panic (ekto_State *L) {
  (void)L;  /* to avoid warnings */
  fprintf(stderr, "PANIC: unprotected error in call to Ekto API (%s)\n",
                   ekto_tostring(L, -1));
  return 0;
}


EKTOLIB_API ekto_State *ektoL_newstate (void) {
  ekto_State *L = ekto_newstate(l_alloc, NULL);
  if (L) ekto_atpanic(L, &panic);
  return L;
}

