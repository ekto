/*
** $Id: lmem.h,v 1.31.1.1 2007/12/27 13:02:25 roberto Exp $
** Interface to Memory Manager
** See Copyright Notice in ekto.h
*/

#ifndef lmem_h
#define lmem_h


#include <stddef.h>

#include "ekto_limits.h"
#include "Ekto.h"

#define MEMERRMSG	"not enough memory"


#define ektoM_reallocv(L,b,on,n,e) \
	((cast(size_t, (n)+1) <= MAX_SIZET/(e)) ?  /* +1 to avoid warnings */ \
		ektoM_realloc_(L, (b), (on)*(e), (n)*(e)) : \
		ektoM_toobig(L))

#define ektoM_freemem(L, b, s)	ektoM_realloc_(L, (b), (s), 0)
#define ektoM_free(L, b)		ektoM_realloc_(L, (b), sizeof(*(b)), 0)
#define ektoM_freearray(L, b, n, t)   ektoM_reallocv(L, (b), n, 0, sizeof(t))

#define ektoM_malloc(L,t)	ektoM_realloc_(L, NULL, 0, (t))
#define ektoM_new(L,t)		cast(t *, ektoM_malloc(L, sizeof(t)))
#define ektoM_newvector(L,n,t) \
		cast(t *, ektoM_reallocv(L, NULL, 0, n, sizeof(t)))

#define ektoM_growvector(L,v,nelems,size,t,limit,e) \
          if ((nelems)+1 > (size)) \
            ((v)=cast(t *, ektoM_growaux_(L,v,&(size),sizeof(t),limit,e)))

#define ektoM_reallocvector(L, v,oldn,n,t) \
   ((v)=cast(t *, ektoM_reallocv(L, v, oldn, n, sizeof(t))))


EKTOI_FUNC void *ektoM_realloc_ (ekto_State *L, void *block, size_t oldsize,
                                                          size_t size);
EKTOI_FUNC void *ektoM_toobig (ekto_State *L);
EKTOI_FUNC void *ektoM_growaux_ (ekto_State *L, void *block, int *size,
                               size_t size_elem, int limit,
                               const char *errormsg);

#endif

