/*
** $Id: lstate.c,v 2.36.1.2 2008/01/03 15:20:39 roberto Exp $
** Global State
** See Copyright Notice in ekto.h
*/


#include <stddef.h>

#define lstate_c
#define EKTO_CORE

#include "Ekto.h"

#include "ekto_debug.h"
#include "ekto_do.h"
#include "ekto_func.h"
#include "ekto_gc.h"
#include "ekto_lex.h"
#include "ekto_mem.h"
#include "ekto_state.h"
#include "ekto_string.h"
#include "ekto_table.h"
#include "ekto_tm.h"


#define state_size(x)	(sizeof(x) + EKTOI_EXTRASPACE)
#define fromstate(l)	(cast(lu_byte *, (l)) - EKTOI_EXTRASPACE)
#define tostate(l)   (cast(ekto_State *, cast(lu_byte *, l) + EKTOI_EXTRASPACE))


/*
** Main thread combines a thread state and the global state
*/
typedef struct LG {
  ekto_State l;
  global_State g;
} LG;
  


static void stack_init (ekto_State *L1, ekto_State *L) {
  /* initialize CallInfo array */
  L1->base_ci = ektoM_newvector(L, BASIC_CI_SIZE, CallInfo);
  L1->ci = L1->base_ci;
  L1->size_ci = BASIC_CI_SIZE;
  L1->end_ci = L1->base_ci + L1->size_ci - 1;
  /* initialize stack array */
  L1->stack = ektoM_newvector(L, BASIC_STACK_SIZE + EXTRA_STACK, TValue);
  L1->stacksize = BASIC_STACK_SIZE + EXTRA_STACK;
  L1->top = L1->stack;
  L1->stack_last = L1->stack+(L1->stacksize - EXTRA_STACK)-1;
  /* initialize first ci */
  L1->ci->func = L1->top;
  setnilvalue(L1->top++);  /* `function' entry for this `ci' */
  L1->base = L1->ci->base = L1->top;
  L1->ci->top = L1->top + EKTO_MINSTACK;
}


static void freestack (ekto_State *L, ekto_State *L1) {
  ektoM_freearray(L, L1->base_ci, L1->size_ci, CallInfo);
  ektoM_freearray(L, L1->stack, L1->stacksize, TValue);
}


/*
** open parts that may cause memory-allocation errors
*/
static void f_ektoopen (ekto_State *L, void *ud) {
  global_State *g = G(L);
  UNUSED(ud);
  stack_init(L, L);  /* init stack */
  sethvalue(L, gt(L), ektoH_new(L, 0, 2));  /* table of globals */
  sethvalue(L, registry(L), ektoH_new(L, 0, 2));  /* registry */
  ektoS_resize(L, MINSTRTABSIZE);  /* initial size of string table */
  ektoT_init(L);
  ektoX_init(L);
  ektoS_fix(ektoS_newliteral(L, MEMERRMSG));
  g->GCthreshold = 4*g->totalbytes;
}


static void preinit_state (ekto_State *L, global_State *g) {
  G(L) = g;
  L->stack = NULL;
  L->stacksize = 0;
  L->errorJmp = NULL;
  L->hook = NULL;
  L->hookmask = 0;
  L->basehookcount = 0;
  L->allowhook = 1;
  resethookcount(L);
  L->openupval = NULL;
  L->size_ci = 0;
  L->nCcalls = L->baseCcalls = 0;
  L->status = 0;
  L->base_ci = L->ci = NULL;
  L->savedpc = NULL;
  L->errfunc = 0;
  setnilvalue(gt(L));
}


static void close_state (ekto_State *L) {
  global_State *g = G(L);
  ektoF_close(L, L->stack);  /* close all upvalues for this thread */
  ektoC_freeall(L);  /* collect all objects */
  ekto_assert(g->rootgc == obj2gco(L));
  ekto_assert(g->strt.nuse == 0);
  ektoM_freearray(L, G(L)->strt.hash, G(L)->strt.size, TString *);
  ektoZ_freebuffer(L, &g->buff);
  freestack(L, L);
  ekto_assert(g->totalbytes == sizeof(LG));
  (*g->frealloc)(g->ud, fromstate(L), state_size(LG), 0);
}


ekto_State *ektoE_newthread (ekto_State *L) {
  ekto_State *L1 = tostate(ektoM_malloc(L, state_size(ekto_State)));
  ektoC_link(L, obj2gco(L1), EKTO_TTHREAD);
  preinit_state(L1, G(L));
  stack_init(L1, L);  /* init stack */
  setobj2n(L, gt(L1), gt(L));  /* share table of globals */
  L1->hookmask = L->hookmask;
  L1->basehookcount = L->basehookcount;
  L1->hook = L->hook;
  resethookcount(L1);
  ekto_assert(iswhite(obj2gco(L1)));
  return L1;
}


void ektoE_freethread (ekto_State *L, ekto_State *L1) {
  ektoF_close(L1, L1->stack);  /* close all upvalues for this thread */
  ekto_assert(L1->openupval == NULL);
  ektoi_userstatefree(L1);
  freestack(L, L1);
  ektoM_freemem(L, fromstate(L1), state_size(ekto_State));
}


EKTO_API ekto_State *ekto_newstate (ekto_Alloc f, void *ud) {
  int i;
  ekto_State *L;
  global_State *g;
  void *l = (*f)(ud, NULL, 0, state_size(LG));
  if (l == NULL) return NULL;
  L = tostate(l);
  g = &((LG *)L)->g;
  L->next = NULL;
  L->tt = EKTO_TTHREAD;
  g->currentwhite = bit2mask(WHITE0BIT, FIXEDBIT);
  L->marked = ektoC_white(g);
  set2bits(L->marked, FIXEDBIT, SFIXEDBIT);
  preinit_state(L, g);
  g->frealloc = f;
  g->ud = ud;
  g->mainthread = L;
  g->uvhead.u.l.prev = &g->uvhead;
  g->uvhead.u.l.next = &g->uvhead;
  g->GCthreshold = 0;  /* mark it as unfinished state */
  g->strt.size = 0;
  g->strt.nuse = 0;
  g->strt.hash = NULL;
  setnilvalue(registry(L));
  ektoZ_initbuffer(L, &g->buff);
  g->panic = NULL;
  g->gcstate = GCSpause;
  g->rootgc = obj2gco(L);
  g->sweepstrgc = 0;
  g->sweepgc = &g->rootgc;
  g->gray = NULL;
  g->grayagain = NULL;
  g->weak = NULL;
  g->tmudata = NULL;
  g->totalbytes = sizeof(LG);
  g->gcpause = EKTOI_GCPAUSE;
  g->gcstepmul = EKTOI_GCMUL;
  g->gcdept = 0;
  for (i=0; i<NUM_TAGS; i++) g->mt[i] = NULL;
  if (ektoD_rawrunprotected(L, f_ektoopen, NULL) != 0) {
    /* memory allocation error: free partial state */
    close_state(L);
    L = NULL;
  }
  else
    ektoi_userstateopen(L);
  return L;
}


static void callallgcTM (ekto_State *L, void *ud) {
  UNUSED(ud);
  ektoC_callGCTM(L);  /* call GC metamethods for all udata */
}


EKTO_API void ekto_close (ekto_State *L) {
  L = G(L)->mainthread;  /* only the main thread can be closed */
  ekto_lock(L);
  ektoF_close(L, L->stack);  /* close all upvalues for this thread */
  ektoC_separateudata(L, 1);  /* separate udata that have GC metamethods */
  L->errfunc = 0;  /* no error function during GC metamethods */
  do {  /* repeat until no more errors */
    L->ci = L->base_ci;
    L->base = L->top = L->ci->base;
    L->nCcalls = L->baseCcalls = 0;
  } while (ektoD_rawrunprotected(L, callallgcTM, NULL) != 0);
  ekto_assert(G(L)->tmudata == NULL);
  ektoi_userstateclose(L);
  close_state(L);
}

