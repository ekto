/*
** $Id: lvm.h,v 2.5.1.1 2007/12/27 13:02:25 roberto Exp $
** Ekto virtual machine
** See Copyright Notice in ekto.h
*/

#ifndef lvm_h
#define lvm_h


#include "ekto_do.h"
#include "ekto_object.h"
#include "ekto_tm.h"


#define tostring(L,o) ((ttype(o) == EKTO_TSTRING) || (ektoV_tostring(L, o)))

#define tonumber(o,n)	(ttype(o) == EKTO_TNUMBER || \
                         (((o) = ektoV_tonumber(o,n)) != NULL))

#define equalobj(L,o1,o2) \
	(ttype(o1) == ttype(o2) && ektoV_equalval(L, o1, o2))


EKTOI_FUNC int ektoV_lessthan (ekto_State *L, const TValue *l, const TValue *r);
EKTOI_FUNC int ektoV_equalval (ekto_State *L, const TValue *t1, const TValue *t2);
EKTOI_FUNC const TValue *ektoV_tonumber (const TValue *obj, TValue *n);
EKTOI_FUNC int ektoV_tostring (ekto_State *L, StkId obj);
EKTOI_FUNC void ektoV_gettable (ekto_State *L, const TValue *t, TValue *key,
                                            StkId val);
EKTOI_FUNC void ektoV_settable (ekto_State *L, const TValue *t, TValue *key,
                                            StkId val);
EKTOI_FUNC void ektoV_execute (ekto_State *L, int nexeccalls);
EKTOI_FUNC void ektoV_concat (ekto_State *L, int total, int last);

#endif
