/*
** $Id: lcode.h,v 1.48.1.1 2007/12/27 13:02:25 roberto Exp $
** Code generator for Ekto
** See Copyright Notice in ekto.h
*/

#ifndef lcode_h
#define lcode_h

#include "ekto_lex.h"
#include "ekto_object.h"
#include "ekto_opcodes.h"
#include "ekto_parser.h"


/*
** Marks the end of a patch list. It is an invalid value both as an absolute
** address, and as a list link (would link an element to itself).
*/
#define NO_JUMP (-1)


/*
** grep "ORDER OPR" if you change these enums
*/
typedef enum BinOpr {
  OPR_ADD, OPR_SUB, OPR_MUL, OPR_DIV, OPR_MOD, OPR_POW,
  OPR_CONCAT,
  OPR_NE, OPR_EQ,
  OPR_LT, OPR_LE, OPR_GT, OPR_GE,
  OPR_AND, OPR_OR,
  OPR_NOBINOPR
} BinOpr;


typedef enum UnOpr { OPR_MINUS, OPR_NOT, OPR_LEN, OPR_NOUNOPR } UnOpr;


#define getcode(fs,e)	((fs)->f->code[(e)->u.s.info])

#define ektoK_codeAsBx(fs,o,A,sBx)	ektoK_codeABx(fs,o,A,(sBx)+MAXARG_sBx)

#define ektoK_setmultret(fs,e)	ektoK_setreturns(fs, e, EKTO_MULTRET)

EKTOI_FUNC int ektoK_codeABx (FuncState *fs, OpCode o, int A, unsigned int Bx);
EKTOI_FUNC int ektoK_codeABC (FuncState *fs, OpCode o, int A, int B, int C);
EKTOI_FUNC void ektoK_fixline (FuncState *fs, int line);
EKTOI_FUNC void ektoK_nil (FuncState *fs, int from, int n);
EKTOI_FUNC void ektoK_reserveregs (FuncState *fs, int n);
EKTOI_FUNC void ektoK_checkstack (FuncState *fs, int n);
EKTOI_FUNC int ektoK_stringK (FuncState *fs, TString *s);
EKTOI_FUNC int ektoK_numberK (FuncState *fs, ekto_Number r);
EKTOI_FUNC void ektoK_dischargevars (FuncState *fs, expdesc *e);
EKTOI_FUNC int ektoK_exp2anyreg (FuncState *fs, expdesc *e);
EKTOI_FUNC void ektoK_exp2nextreg (FuncState *fs, expdesc *e);
EKTOI_FUNC void ektoK_exp2val (FuncState *fs, expdesc *e);
EKTOI_FUNC int ektoK_exp2RK (FuncState *fs, expdesc *e);
EKTOI_FUNC void ektoK_self (FuncState *fs, expdesc *e, expdesc *key);
EKTOI_FUNC void ektoK_indexed (FuncState *fs, expdesc *t, expdesc *k);
EKTOI_FUNC void ektoK_goiftrue (FuncState *fs, expdesc *e);
EKTOI_FUNC void ektoK_storevar (FuncState *fs, expdesc *var, expdesc *e);
EKTOI_FUNC void ektoK_setreturns (FuncState *fs, expdesc *e, int nresults);
EKTOI_FUNC void ektoK_setoneret (FuncState *fs, expdesc *e);
EKTOI_FUNC int ektoK_jump (FuncState *fs);
EKTOI_FUNC void ektoK_ret (FuncState *fs, int first, int nret);
EKTOI_FUNC void ektoK_patchlist (FuncState *fs, int list, int target);
EKTOI_FUNC void ektoK_patchtohere (FuncState *fs, int list);
EKTOI_FUNC void ektoK_concat (FuncState *fs, int *l1, int l2);
EKTOI_FUNC int ektoK_getlabel (FuncState *fs);
EKTOI_FUNC void ektoK_prefix (FuncState *fs, UnOpr op, expdesc *v);
EKTOI_FUNC void ektoK_infix (FuncState *fs, BinOpr op, expdesc *v);
EKTOI_FUNC void ektoK_posfix (FuncState *fs, BinOpr op, expdesc *v1, expdesc *v2);
EKTOI_FUNC void ektoK_setlist (FuncState *fs, int base, int nelems, int tostore);


#endif
