/*
** $Id: lundump.h,v 1.37.1.1 2007/12/27 13:02:25 roberto Exp $
** load precompiled Ekto chunks
** See Copyright Notice in ekto.h
*/

#ifndef lundump_h
#define lundump_h

#include "ekto_object.h"
#include "ekto_zio.h"

/* load one chunk; from lundump.c */
EKTOI_FUNC Proto* ektoU_undump (ekto_State* L, ZIO* Z, Mbuffer* buff, const char* name);

/* make header; from lundump.c */
EKTOI_FUNC void ektoU_header (char* h);

/* dump one chunk; from ldump.c */
EKTOI_FUNC int ektoU_dump (ekto_State* L, const Proto* f, ekto_Writer w, void* data, int strip);

#ifdef ektoc_c
/* print one chunk; from print.c */
EKTOI_FUNC void ektoU_print (const Proto* f, int full);
#endif

/* for header of binary files -- this is Ekto 5.1 */
#define EKTOC_VERSION		0x51

/* for header of binary files -- this is the official format */
#define EKTOC_FORMAT		0x17

/* size of header of binary files */
#define EKTOC_HEADERSIZE		12

#endif
