/*
** $Id: ektoconf.h,v 1.82.1.7 2008/02/11 16:25:08 roberto Exp $
** Configuration file for Ekto
** See Copyright Notice in ekto.h
*/


#ifndef lconfig_h
#define lconfig_h

#include <limits.h>
#include <stddef.h>


/*
** ==================================================================
** Search for "@@" to find all configurable definitions.
** ===================================================================
*/


/*
@@ EKTO_ANSI controls the use of non-ansi features.
** CHANGE it (define it) if you want Ekto to avoid the use of any
** non-ansi feature or library.
*/
#if defined(__STRICT_ANSI__)
#define EKTO_ANSI
#endif


#if !defined(EKTO_ANSI) && defined(_WIN32)
#define EKTO_WIN
#endif

#if defined(EKTO_USE_LINUX)
#define EKTO_USE_POSIX
#define EKTO_USE_DLOPEN		/* needs an extra library: -ldl */
#define EKTO_USE_READLINE	/* needs some extra libraries */
#endif

#if defined(EKTO_USE_MACOSX)
#define EKTO_USE_POSIX
#define EKTO_DL_DYLD		/* does not need extra library */
#endif



/*
@@ EKTO_USE_POSIX includes all functionallity listed as X/Open System
@* Interfaces Extension (XSI).
** CHANGE it (define it) if your system is XSI compatible.
*/
#if defined(EKTO_USE_POSIX)
#define EKTO_USE_MKSTEMP
#define EKTO_USE_ISATTY
#define EKTO_USE_POPEN
#define EKTO_USE_ULONGJMP
#endif


/*
@@ EKTO_PATH and EKTO_CPATH are the names of the environment variables that
@* Ekto check to set its paths.
@@ EKTO_INIT is the name of the environment variable that Ekto
@* checks for initialization code.
** CHANGE them if you want different names.
*/
#define EKTO_PATH        "EKTO_PATH"
#define EKTO_CPATH       "EKTO_CPATH"
#define EKTO_INIT	"EKTO_INIT"


/*
@@ EKTO_PATH_DEFAULT is the default path that Ekto uses to look for
@* Ekto libraries.
@@ EKTO_CPATH_DEFAULT is the default path that Ekto uses to look for
@* C libraries.
** CHANGE them if your machine has a non-conventional directory
** hierarchy or if you want to install your libraries in
** non-conventional directories.
*/
#if defined(_WIN32)
/*
** In Windows, any exclamation mark ('!') in the path is replaced by the
** path of the directory of the executable file of the current process.
*/
#define EKTO_LDIR	"!\\ekto\\"
#define EKTO_CDIR	"!\\"
#define EKTO_PATH_DEFAULT  \
		".\\?.ekto;"  EKTO_LDIR"?.ekto;"  EKTO_LDIR"?\\init.ekto;" \
		             EKTO_CDIR"?.ekto;"  EKTO_CDIR"?\\init.ekto"
#define EKTO_CPATH_DEFAULT \
	".\\?.dll;"  EKTO_CDIR"?.dll;" EKTO_CDIR"loadall.dll"

#else
#define EKTO_ROOT	"/usr/local/"
#define EKTO_LDIR	EKTO_ROOT "share/ekto/5.1/"
#define EKTO_CDIR	EKTO_ROOT "lib/ekto/5.1/"
#define EKTO_PATH_DEFAULT  \
		"./?.ekto;"  EKTO_LDIR"?.ekto;"  EKTO_LDIR"?/init.ekto;" \
		            EKTO_CDIR"?.ekto;"  EKTO_CDIR"?/init.ekto"
#define EKTO_CPATH_DEFAULT \
	"./?.so;"  EKTO_CDIR"?.so;" EKTO_CDIR"loadall.so"
#endif


/*
@@ EKTO_DIRSEP is the directory separator (for submodules).
** CHANGE it if your machine does not use "/" as the directory separator
** and is not Windows. (On Windows Ekto automatically uses "\".)
*/
#if defined(_WIN32)
#define EKTO_DIRSEP	"\\"
#else
#define EKTO_DIRSEP	"/"
#endif


/*
@@ EKTO_PATHSEP is the character that separates templates in a path.
@@ EKTO_PATH_MARK is the string that marks the substitution points in a
@* template.
@@ EKTO_EXECDIR in a Windows path is replaced by the executable's
@* directory.
@@ EKTO_IGMARK is a mark to ignore all before it when bulding the
@* ektoopen_ function name.
** CHANGE them if for some reason your system cannot use those
** characters. (E.g., if one of those characters is a common character
** in file/directory names.) Probably you do not need to change them.
*/
#define EKTO_PATHSEP	";"
#define EKTO_PATH_MARK	"?"
#define EKTO_EXECDIR	"!"
#define EKTO_IGMARK	"-"


/*
@@ EKTO_INTEGER is the integral type used by ekto_pushinteger/ekto_tointeger.
** CHANGE that if ptrdiff_t is not adequate on your machine. (On most
** machines, ptrdiff_t gives a good choice between int or long.)
*/
#define EKTO_INTEGER	ptrdiff_t


/*
@@ EKTO_API is a mark for all core API functions.
@@ EKTOLIB_API is a mark for all standard library functions.
** CHANGE them if you need to define those functions in some special way.
** For instance, if you want to create one Windows DLL with the core and
** the libraries, you may want to use the following definition (define
** EKTO_BUILD_AS_DLL to get it).
*/
#if defined(EKTO_BUILD_AS_DLL)

#if defined(EKTO_CORE) || defined(EKTO_LIB)
#define EKTO_API __declspec(dllexport)
#else
#define EKTO_API __declspec(dllimport)
#endif

#else

#define EKTO_API		extern

#endif

/* more often than not the libs go together with the core */
#define EKTOLIB_API	EKTO_API


/*
@@ EKTOI_FUNC is a mark for all extern functions that are not to be
@* exported to outside modules.
@@ EKTOI_DATA is a mark for all extern (const) variables that are not to
@* be exported to outside modules.
** CHANGE them if you need to mark them in some special way. Elf/gcc
** (versions 3.2 and later) mark them as "hidden" to optimize access
** when Ekto is compiled as a shared library.
*/
#if defined(ektoall_c)
#define EKTOI_FUNC	static
#define EKTOI_DATA	/* empty */

#elif defined(__GNUC__) && ((__GNUC__*100 + __GNUC_MINOR__) >= 302) && \
      defined(__ELF__)
#define EKTOI_FUNC	__attribute__((visibility("hidden"))) extern
#define EKTOI_DATA	EKTOI_FUNC

#else
#define EKTOI_FUNC	extern
#define EKTOI_DATA	extern
#endif



/*
@@ EKTO_QL describes how error messages quote program elements.
** CHANGE it if you want a different appearance.
*/
#define EKTO_QL(x)	"'" x "'"
#define EKTO_QS		EKTO_QL("%s")


/*
@@ EKTO_IDSIZE gives the maximum size for the description of the source
@* of a function in debug information.
** CHANGE it if you want a different size.
*/
#define EKTO_IDSIZE	60


/*
** {==================================================================
** Stand-alone configuration
** ===================================================================
*/

#if defined(ekto_c) || defined(ektoall_c)

/*
@@ ekto_stdin_is_tty detects whether the standard input is a 'tty' (that
@* is, whether we're running ekto interactively).
** CHANGE it if you have a better definition for non-POSIX/non-Windows
** systems.
*/
#if defined(EKTO_USE_ISATTY)
#include <unistd.h>
#define ekto_stdin_is_tty()	isatty(0)
#elif defined(EKTO_WIN)
#include <io.h>
#include <stdio.h>
#define ekto_stdin_is_tty()	_isatty(_fileno(stdin))
#else
#define ekto_stdin_is_tty()	1  /* assume stdin is a tty */
#endif


/*
@@ EKTO_PROMPT is the default prompt used by stand-alone Ekto.
@@ EKTO_PROMPT2 is the default continuation prompt used by stand-alone Ekto.
** CHANGE them if you want different prompts. (You can also change the
** prompts dynamically, assigning to globals _PROMPT/_PROMPT2.)
*/
#define EKTO_PROMPT		"> "
#define EKTO_PROMPT2		">> "


/*
@@ EKTO_PROGNAME is the default name for the stand-alone Ekto program.
** CHANGE it if your stand-alone interpreter has a different name and
** your system is not able to detect that name automatically.
*/
#define EKTO_PROGNAME		"ekto"


/*
@@ EKTO_MAXINPUT is the maximum length for an input line in the
@* stand-alone interpreter.
** CHANGE it if you need longer lines.
*/
#define EKTO_MAXINPUT	512


/*
@@ ekto_readline defines how to show a prompt and then read a line from
@* the standard input.
@@ ekto_saveline defines how to "save" a read line in a "history".
@@ ekto_freeline defines how to free a line read by ekto_readline.
** CHANGE them if you want to improve this functionality (e.g., by using
** GNU readline and history facilities).
*/
#if defined(EKTO_USE_READLINE)
#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#define ekto_readline(L,b,p)	((void)L, ((b)=readline(p)) != NULL)
#define ekto_saveline(L,idx) \
	if (ekto_strlen(L,idx) > 0)  /* non-empty line? */ \
	  add_history(ekto_tostring(L, idx));  /* add it to history */
#define ekto_freeline(L,b)	((void)L, free(b))
#else
#define ekto_readline(L,b,p)	\
	((void)L, fputs(p, stdout), fflush(stdout),  /* show prompt */ \
	fgets(b, EKTO_MAXINPUT, stdin) != NULL)  /* get line */
#define ekto_saveline(L,idx)	{ (void)L; (void)idx; }
#define ekto_freeline(L,b)	{ (void)L; (void)b; }
#endif

#endif

/* }================================================================== */


/*
@@ EKTOI_GCPAUSE defines the default pause between garbage-collector cycles
@* as a percentage.
** CHANGE it if you want the GC to run faster or slower (higher values
** mean larger pauses which mean slower collection.) You can also change
** this value dynamically.
*/
#define EKTOI_GCPAUSE	200  /* 200% (wait memory to double before next GC) */


/*
@@ EKTOI_GCMUL defines the default speed of garbage collection relative to
@* memory allocation as a percentage.
** CHANGE it if you want to change the granularity of the garbage
** collection. (Higher values mean coarser collections. 0 represents
** infinity, where each step performs a full collection.) You can also
** change this value dynamically.
*/
#define EKTOI_GCMUL	200 /* GC runs 'twice the speed' of memory allocation */



/*
@@ EKTO_COMPAT_GETN controls compatibility with old getn behavior.
** CHANGE it (define it) if you want exact compatibility with the
** behavior of setn/getn in Ekto 5.0.
*/
#undef EKTO_COMPAT_GETN

/*
@@ EKTO_COMPAT_LOADLIB controls compatibility about global loadlib.
** CHANGE it to undefined as soon as you do not need a global 'loadlib'
** function (the function is still available as 'package.loadlib').
*/
#undef EKTO_COMPAT_LOADLIB

/*
@@ EKTO_COMPAT_VARARG controls compatibility with old vararg feature.
** CHANGE it to undefined as soon as your programs use only '...' to
** access vararg parameters (instead of the old 'arg' table).
*/
#define EKTO_COMPAT_VARARG

/*
@@ EKTO_COMPAT_MOD controls compatibility with old math.mod function.
** CHANGE it to undefined as soon as your programs use 'math.fmod' or
** the new '%' operator instead of 'math.mod'.
*/
#define EKTO_COMPAT_MOD

/*
@@ EKTO_COMPAT_LSTR controls compatibility with old long string nesting
@* facility.
** CHANGE it to 2 if you want the old behaviour, or undefine it to turn
** off the advisory error when nesting [[...]].
*/
#define EKTO_COMPAT_LSTR		1

/*
@@ EKTO_COMPAT_GFIND controls compatibility with old 'string.gfind' name.
** CHANGE it to undefined as soon as you rename 'string.gfind' to
** 'string.gmatch'.
*/
#define EKTO_COMPAT_GFIND

/*
@@ EKTO_COMPAT_OPENLIB controls compatibility with old 'ektoL_openlib'
@* behavior.
** CHANGE it to undefined as soon as you replace to 'ektoL_register'
** your uses of 'ektoL_openlib'
*/
#define EKTO_COMPAT_OPENLIB



/*
@@ ektoi_apicheck is the assert macro used by the Ekto-C API.
** CHANGE ektoi_apicheck if you want Ekto to perform some checks in the
** parameters it gets from API calls. This may slow down the interpreter
** a bit, but may be quite useful when debugging C code that interfaces
** with Ekto. A useful redefinition is to use assert.h.
*/
#if defined(EKTO_USE_APICHECK)
#include <assert.h>
#define ektoi_apicheck(L,o)	{ (void)L; assert(o); }
#else
#define ektoi_apicheck(L,o)	{ (void)L; }
#endif


/*
@@ EKTOI_BITSINT defines the number of bits in an int.
** CHANGE here if Ekto cannot automatically detect the number of bits of
** your machine. Probably you do not need to change this.
*/
/* avoid overflows in comparison */
#if INT_MAX-20 < 32760
#define EKTOI_BITSINT	16
#elif INT_MAX > 2147483640L
/* int has at least 32 bits */
#define EKTOI_BITSINT	32
#else
#error "you must define EKTO_BITSINT with number of bits in an integer"
#endif


/*
@@ EKTOI_UINT32 is an unsigned integer with at least 32 bits.
@@ EKTOI_INT32 is an signed integer with at least 32 bits.
@@ EKTOI_UMEM is an unsigned integer big enough to count the total
@* memory used by Ekto.
@@ EKTOI_MEM is a signed integer big enough to count the total memory
@* used by Ekto.
** CHANGE here if for some weird reason the default definitions are not
** good enough for your machine. (The definitions in the 'else'
** part always works, but may waste space on machines with 64-bit
** longs.) Probably you do not need to change this.
*/
#if EKTOI_BITSINT >= 32
#define EKTOI_UINT32	unsigned int
#define EKTOI_INT32	int
#define EKTOI_MAXINT32	INT_MAX
#define EKTOI_UMEM	size_t
#define EKTOI_MEM	ptrdiff_t
#else
/* 16-bit ints */
#define EKTOI_UINT32	unsigned long
#define EKTOI_INT32	long
#define EKTOI_MAXINT32	LONG_MAX
#define EKTOI_UMEM	unsigned long
#define EKTOI_MEM	long
#endif


/*
@@ EKTOI_MAXCALLS limits the number of nested calls.
** CHANGE it if you need really deep recursive calls. This limit is
** arbitrary; its only purpose is to stop infinite recursion before
** exhausting memory.
*/
#define EKTOI_MAXCALLS	20000


/*
@@ EKTOI_MAXCSTACK limits the number of Ekto stack slots that a C function
@* can use.
** CHANGE it if you need lots of (Ekto) stack space for your C
** functions. This limit is arbitrary; its only purpose is to stop C
** functions to consume unlimited stack space. (must be smaller than
** -EKTO_REGISTRYINDEX)
*/
#define EKTOI_MAXCSTACK	8000



/*
** {==================================================================
** CHANGE (to smaller values) the following definitions if your system
** has a small C stack. (Or you may want to change them to larger
** values if your system has a large C stack and these limits are
** too rigid for you.) Some of these constants control the size of
** stack-allocated arrays used by the compiler or the interpreter, while
** others limit the maximum number of recursive calls that the compiler
** or the interpreter can perform. Values too large may cause a C stack
** overflow for some forms of deep constructs.
** ===================================================================
*/


/*
@@ EKTOI_MAXCCALLS is the maximum depth for nested C calls (short) and
@* syntactical nested non-terminals in a program.
*/
#define EKTOI_MAXCCALLS		200


/*
@@ EKTOI_MAXVARS is the maximum number of local variables per function
@* (must be smaller than 250).
*/
#define EKTOI_MAXVARS		200


/*
@@ EKTOI_MAXUPVALUES is the maximum number of upvalues per function
@* (must be smaller than 250).
*/
#define EKTOI_MAXUPVALUES	60


/*
@@ EKTOL_BUFFERSIZE is the buffer size used by the lauxlib buffer system.
*/
#define EKTOL_BUFFERSIZE		BUFSIZ

/* }================================================================== */




/*
** {==================================================================
@@ EKTO_NUMBER is the type of numbers in Ekto.
** CHANGE the following definitions only if you want to build Ekto
** with a number type different from double. You may also need to
** change ekto_number2int & ekto_number2integer.
** ===================================================================
*/

#define EKTO_NUMBER_DOUBLE
#define EKTO_NUMBER	double

/*
@@ EKTOI_UACNUMBER is the result of an 'usual argument conversion'
@* over a number.
*/
#define EKTOI_UACNUMBER	double


/*
@@ EKTO_NUMBER_SCAN is the format for reading numbers.
@@ EKTO_NUMBER_FMT is the format for writing numbers.
@@ ekto_number2str converts a number to a string.
@@ EKTOI_MAXNUMBER2STR is maximum size of previous conversion.
@@ ekto_str2number converts a string to a number.
*/
#define EKTO_NUMBER_SCAN		"%lf"
#define EKTO_NUMBER_FMT		"%.14g"
#define ekto_number2str(s,n)	sprintf((s), EKTO_NUMBER_FMT, (n))
#define EKTOI_MAXNUMBER2STR	32 /* 16 digits, sign, point, and \0 */
#define ekto_str2number(s,p)	strtod((s), (p))


/*
@@ The ektoi_num* macros define the primitive operations over numbers.
*/
#if defined(EKTO_CORE)
#include <math.h>
#define ektoi_numadd(a,b)	((a)+(b))
#define ektoi_numsub(a,b)	((a)-(b))
#define ektoi_nummul(a,b)	((a)*(b))
#define ektoi_numdiv(a,b)	((a)/(b))
#define ektoi_nummod(a,b)	((a) - floor((a)/(b))*(b))
#define ektoi_numpow(a,b)	(pow(a,b))
#define ektoi_numunm(a)		(-(a))
#define ektoi_numeq(a,b)		((a)==(b))
#define ektoi_numlt(a,b)		((a)<(b))
#define ektoi_numle(a,b)		((a)<=(b))
#define ektoi_numisnan(a)	(!ektoi_numeq((a), (a)))
#endif


/*
@@ ekto_number2int is a macro to convert ekto_Number to int.
@@ ekto_number2integer is a macro to convert ekto_Number to ekto_Integer.
** CHANGE them if you know a faster way to convert a ekto_Number to
** int (with any rounding method and without throwing errors) in your
** system. In Pentium machines, a naive typecast from double to int
** in C is extremely slow, so any alternative is worth trying.
*/

/* On a Pentium, resort to a trick */
#if defined(EKTO_NUMBER_DOUBLE) && !defined(EKTO_ANSI) && !defined(__SSE2__) && \
    (defined(__i386) || defined (_M_IX86) || defined(__i386__))

/* On a Microsoft compiler, use assembler */
#if defined(_MSC_VER)

#define ekto_number2int(i,d)   __asm fld d   __asm fistp i
#define ekto_number2integer(i,n)		ekto_number2int(i, n)

/* the next trick should work on any Pentium, but sometimes clashes
   with a DirectX idiosyncrasy */
#else

union ektoi_Cast { double l_d; long l_l; };
#define ekto_number2int(i,d) \
  { volatile union ektoi_Cast u; u.l_d = (d) + 6755399441055744.0; (i) = u.l_l; }
#define ekto_number2integer(i,n)		ekto_number2int(i, n)

#endif


/* this option always works, but may be slow */
#else
#define ekto_number2int(i,d)	((i)=(int)(d))
#define ekto_number2integer(i,d)	((i)=(ekto_Integer)(d))

#endif

/* }================================================================== */


/*
@@ EKTOI_USER_ALIGNMENT_T is a type that requires maximum alignment.
** CHANGE it if your system requires alignments larger than double. (For
** instance, if your system supports long doubles and they must be
** aligned in 16-byte boundaries, then you should add long double in the
** union.) Probably you do not need to change this.
*/
#define EKTOI_USER_ALIGNMENT_T	union { double u; void *s; long l; }


/*
@@ EKTOI_THROW/EKTOI_TRY define how Ekto does exception handling.
** CHANGE them if you prefer to use longjmp/setjmp even with C++
** or if want/don't to use _longjmp/_setjmp instead of regular
** longjmp/setjmp. By default, Ekto handles errors with exceptions when
** compiling as C++ code, with _longjmp/_setjmp when asked to use them,
** and with longjmp/setjmp otherwise.
*/
#if defined(__cplusplus)
/* C++ exceptions */
#define EKTOI_THROW(L,c)	throw(c)
#define EKTOI_TRY(L,c,a)	try { a } catch(...) \
	{ if ((c)->status == 0) (c)->status = -1; }
#define ektoi_jmpbuf	int  /* dummy variable */

#elif defined(EKTO_USE_ULONGJMP)
/* in Unix, try _longjmp/_setjmp (more efficient) */
#define EKTOI_THROW(L,c)	_longjmp((c)->b, 1)
#define EKTOI_TRY(L,c,a)	if (_setjmp((c)->b) == 0) { a }
#define ektoi_jmpbuf	jmp_buf

#else
/* default handling with long jumps */
#define EKTOI_THROW(L,c)	longjmp((c)->b, 1)
#define EKTOI_TRY(L,c,a)	if (setjmp((c)->b) == 0) { a }
#define ektoi_jmpbuf	jmp_buf

#endif


/*
@@ EKTO_MAXCAPTURES is the maximum number of captures that a pattern
@* can do during pattern-matching.
** CHANGE it if you need more captures. This limit is arbitrary.
*/
#define EKTO_MAXCAPTURES		32


/*
@@ ekto_tmpnam is the function that the OS library uses to create a
@* temporary name.
@@ EKTO_TMPNAMBUFSIZE is the maximum size of a name created by ekto_tmpnam.
** CHANGE them if you have an alternative to tmpnam (which is considered
** insecure) or if you want the original tmpnam anyway.  By default, Ekto
** uses tmpnam except when POSIX is available, where it uses mkstemp.
*/
#if defined(loslib_c) || defined(ektoall_c)

#if defined(EKTO_USE_MKSTEMP)
#include <unistd.h>
#define EKTO_TMPNAMBUFSIZE	32
#define ekto_tmpnam(b,e)	{ \
	strcpy(b, "/tmp/ekto_XXXXXX"); \
	e = mkstemp(b); \
	if (e != -1) close(e); \
	e = (e == -1); }

#else
#define EKTO_TMPNAMBUFSIZE	L_tmpnam
#define ekto_tmpnam(b,e)		{ e = (tmpnam(b) == NULL); }
#endif

#endif


/*
@@ ekto_popen spawns a new process connected to the current one through
@* the file streams.
** CHANGE it if you have a way to implement it in your system.
*/
#if defined(EKTO_USE_POPEN)

#define ekto_popen(L,c,m)	((void)L, fflush(NULL), popen(c,m))
#define ekto_pclose(L,file)	((void)L, (pclose(file) != -1))

#elif defined(EKTO_WIN)

#define ekto_popen(L,c,m)	((void)L, _popen(c,m))
#define ekto_pclose(L,file)	((void)L, (_pclose(file) != -1))

#else

#define ekto_popen(L,c,m)	((void)((void)c, m),  \
		ektoL_error(L, EKTO_QL("popen") " not supported"), (FILE*)0)
#define ekto_pclose(L,file)		((void)((void)L, file), 0)

#endif

/*
@@ EKTO_DL_* define which dynamic-library system Ekto should use.
** CHANGE here if Ekto has problems choosing the appropriate
** dynamic-library system for your platform (either Windows' DLL, Mac's
** dyld, or Unix's dlopen). If your system is some kind of Unix, there
** is a good chance that it has dlopen, so EKTO_DL_DLOPEN will work for
** it.  To use dlopen you also need to adapt the src/Makefile (probably
** adding -ldl to the linker options), so Ekto does not select it
** automatically.  (When you change the makefile to add -ldl, you must
** also add -DEKTO_USE_DLOPEN.)
** If you do not want any kind of dynamic library, undefine all these
** options.
** By default, _WIN32 gets EKTO_DL_DLL and MAC OS X gets EKTO_DL_DYLD.
*/
#if defined(EKTO_USE_DLOPEN)
#define EKTO_DL_DLOPEN
#endif

#if defined(EKTO_WIN)
#define EKTO_DL_DLL
#endif


/*
@@ EKTOI_EXTRASPACE allows you to add user-specific data in a ekto_State
@* (the data goes just *before* the ekto_State pointer).
** CHANGE (define) this if you really need that. This value must be
** a multiple of the maximum alignment required for your machine.
*/
#define EKTOI_EXTRASPACE		0


/*
@@ ektoi_userstate* allow user-specific actions on threads.
** CHANGE them if you defined EKTOI_EXTRASPACE and need to do something
** extra when a thread is created/deleted/resumed/yielded.
*/
#define ektoi_userstateopen(L)		((void)L)
#define ektoi_userstateclose(L)		((void)L)
#define ektoi_userstatethread(L,L1)	((void)L)
#define ektoi_userstatefree(L)		((void)L)
#define ektoi_userstateresume(L,n)	((void)L)
#define ektoi_userstateyield(L,n)	((void)L)


/*
@@ EKTO_INTFRMLEN is the length modifier for integer conversions
@* in 'string.format'.
@@ EKTO_INTFRM_T is the integer type correspoding to the previous length
@* modifier.
** CHANGE them if your system supports long long or does not support long.
*/

#if defined(EKTO_USELONGLONG)

#define EKTO_INTFRMLEN		"ll"
#define EKTO_INTFRM_T		long long

#else

#define EKTO_INTFRMLEN		"l"
#define EKTO_INTFRM_T		long

#endif



/* =================================================================== */

/*
** Local configuration. You can use this space to add your redefinitions
** without modifying the main part of the file.
*/

#include <inttypes.h>
#include <byteswap.h>

#endif

