/*
** $Id: loslib.c,v 1.19.1.3 2008/01/18 16:38:18 roberto Exp $
** Standard Operating System library
** See Copyright Notice in ekto.h
*/


#include <errno.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define loslib_c
#define EKTO_LIB

#include "Ekto.h"

#include "Ekto_Aux.h"
#include "Ekto_Lib.h"


static int os_pushresult (ekto_State *L, int i, const char *filename) {
  int en = errno;  /* calls to Ekto API may change this value */
  if (i) {
    ekto_pushboolean(L, 1);
    return 1;
  }
  else {
    ekto_pushnil(L);
    ekto_pushfstring(L, "%s: %s", filename, strerror(en));
    ekto_pushinteger(L, en);
    return 3;
  }
}


static int os_execute (ekto_State *L) {
  ekto_pushinteger(L, system(ektoL_optstring(L, 1, NULL)));
  return 1;
}


static int os_remove (ekto_State *L) {
  const char *filename = ektoL_checkstring(L, 1);
  return os_pushresult(L, remove(filename) == 0, filename);
}


static int os_rename (ekto_State *L) {
  const char *fromname = ektoL_checkstring(L, 1);
  const char *toname = ektoL_checkstring(L, 2);
  return os_pushresult(L, rename(fromname, toname) == 0, fromname);
}


static int os_tmpname (ekto_State *L) {
  char buff[EKTO_TMPNAMBUFSIZE];
  int err;
  ekto_tmpnam(buff, err);
  if (err)
    return ektoL_error(L, "unable to generate a unique filename");
  ekto_pushstring(L, buff);
  return 1;
}


static int os_getenv (ekto_State *L) {
  ekto_pushstring(L, getenv(ektoL_checkstring(L, 1)));  /* if NULL push nil */
  return 1;
}


static int os_clock (ekto_State *L) {
  ekto_pushnumber(L, ((ekto_Number)clock())/(ekto_Number)CLOCKS_PER_SEC);
  return 1;
}


/*
** {======================================================
** Time/Date operations
** { year=%Y, month=%m, day=%d, hour=%H, min=%M, sec=%S,
**   wday=%w+1, yday=%j, isdst=? }
** =======================================================
*/

static void setfield (ekto_State *L, const char *key, int value) {
  ekto_pushinteger(L, value);
  ekto_setfield(L, -2, key);
}

static void setboolfield (ekto_State *L, const char *key, int value) {
  if (value < 0)  /* undefined? */
    return;  /* does not set field */
  ekto_pushboolean(L, value);
  ekto_setfield(L, -2, key);
}

static int getboolfield (ekto_State *L, const char *key) {
  int res;
  ekto_getfield(L, -1, key);
  res = ekto_isnil(L, -1) ? -1 : ekto_toboolean(L, -1);
  ekto_pop(L, 1);
  return res;
}


static int getfield (ekto_State *L, const char *key, int d) {
  int res;
  ekto_getfield(L, -1, key);
  if (ekto_isnumber(L, -1))
    res = (int)ekto_tointeger(L, -1);
  else {
    if (d < 0)
      return ektoL_error(L, "field " EKTO_QS " missing in date table", key);
    res = d;
  }
  ekto_pop(L, 1);
  return res;
}


static int os_date (ekto_State *L) {
  const char *s = ektoL_optstring(L, 1, "%c");
  time_t t = ektoL_opt(L, (time_t)ektoL_checknumber, 2, time(NULL));
  struct tm *stm;
  if (*s == '!') {  /* UTC? */
    stm = gmtime(&t);
    s++;  /* skip `!' */
  }
  else
    stm = localtime(&t);
  if (stm == NULL)  /* invalid date? */
    ekto_pushnil(L);
  else if (strcmp(s, "*t") == 0) {
    ekto_createtable(L, 0, 9);  /* 9 = number of fields */
    setfield(L, "sec", stm->tm_sec);
    setfield(L, "min", stm->tm_min);
    setfield(L, "hour", stm->tm_hour);
    setfield(L, "day", stm->tm_mday);
    setfield(L, "month", stm->tm_mon+1);
    setfield(L, "year", stm->tm_year+1900);
    setfield(L, "wday", stm->tm_wday+1);
    setfield(L, "yday", stm->tm_yday+1);
    setboolfield(L, "isdst", stm->tm_isdst);
  }
  else {
    char cc[3];
    ektoL_Buffer b;
    cc[0] = '%'; cc[2] = '\0';
    ektoL_buffinit(L, &b);
    for (; *s; s++) {
      if (*s != '%' || *(s + 1) == '\0')  /* no conversion specifier? */
        ektoL_addchar(&b, *s);
      else {
        size_t reslen;
        char buff[200];  /* should be big enough for any conversion result */
        cc[1] = *(++s);
        reslen = strftime(buff, sizeof(buff), cc, stm);
        ektoL_addlstring(&b, buff, reslen);
      }
    }
    ektoL_pushresult(&b);
  }
  return 1;
}


static int os_time (ekto_State *L) {
  time_t t;
  if (ekto_isnoneornil(L, 1))  /* called without args? */
    t = time(NULL);  /* get current time */
  else {
    struct tm ts;
    ektoL_checktype(L, 1, EKTO_TTABLE);
    ekto_settop(L, 1);  /* make sure table is at the top */
    ts.tm_sec = getfield(L, "sec", 0);
    ts.tm_min = getfield(L, "min", 0);
    ts.tm_hour = getfield(L, "hour", 12);
    ts.tm_mday = getfield(L, "day", -1);
    ts.tm_mon = getfield(L, "month", -1) - 1;
    ts.tm_year = getfield(L, "year", -1) - 1900;
    ts.tm_isdst = getboolfield(L, "isdst");
    t = mktime(&ts);
  }
  if (t == (time_t)(-1))
    ekto_pushnil(L);
  else
    ekto_pushnumber(L, (ekto_Number)t);
  return 1;
}


static int os_difftime (ekto_State *L) {
  ekto_pushnumber(L, difftime((time_t)(ektoL_checknumber(L, 1)),
                             (time_t)(ektoL_optnumber(L, 2, 0))));
  return 1;
}

/* }====================================================== */


static int os_setlocale (ekto_State *L) {
  static const int cat[] = {LC_ALL, LC_COLLATE, LC_CTYPE, LC_MONETARY,
                      LC_NUMERIC, LC_TIME};
  static const char *const catnames[] = {"all", "collate", "ctype", "monetary",
     "numeric", "time", NULL};
  const char *l = ektoL_optstring(L, 1, NULL);
  int op = ektoL_checkoption(L, 2, "all", catnames);
  ekto_pushstring(L, setlocale(cat[op], l));
  return 1;
}


static int os_exit (ekto_State *L) {
  exit(ektoL_optint(L, 1, EXIT_SUCCESS));
}

static const ektoL_Reg syslib[] = {
  {"clock",     os_clock},
  {"date",      os_date},
  {"difftime",  os_difftime},
  {"execute",   os_execute},
  {"exit",      os_exit},
  {"getenv",    os_getenv},
  {"remove",    os_remove},
  {"rename",    os_rename},
  {"setlocale", os_setlocale},
  {"time",      os_time},
  {"tmpname",   os_tmpname},
  {NULL, NULL}
};

/* }====================================================== */



EKTOLIB_API int ektoopen_os (ekto_State *L) {
  ektoL_register(L, EKTO_OSLIBNAME, syslib);
  return 1;
}

