/*
** $Id: lbaselib.c,v 1.191.1.6 2008/02/14 16:46:22 roberto Exp $
** Basic library
** See Copyright Notice in ekto.h
*/



#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define lbaselib_c
#define EKTO_LIB

#include "Ekto.h"

#include "Ekto_Aux.h"
#include "Ekto_Lib.h"




/*
** If your system does not support `stdout', you can just remove this function.
** If you need, you can define your own `print' function, following this
** model but changing `fputs' to put the strings at a proper place
** (a console window or a log file, for instance).
*/
static int ektoB_print (ekto_State *L) {
  int n = ekto_gettop(L);  /* number of arguments */
  int i;
  ekto_getglobal(L, "tostring");
  for (i=1; i<=n; i++) {
    const char *s;
    ekto_pushvalue(L, -1);  /* function to be called */
    ekto_pushvalue(L, i);   /* value to print */
    ekto_call(L, 1, 1);
    s = ekto_tostring(L, -1);  /* get result */
    if (s == NULL)
      return ektoL_error(L, EKTO_QL("tostring") " must return a string to "
                           EKTO_QL("print"));
    if (i>1) fputs("\t", stdout);
    fputs(s, stdout);
    ekto_pop(L, 1);  /* pop result */
  }
  fputs("\n", stdout);
  return 0;
}


static int ektoB_tonumber (ekto_State *L) {
  int base = ektoL_optint(L, 2, 10);
  if (base == 10) {  /* standard conversion */
    ektoL_checkany(L, 1);
    if (ekto_isnumber(L, 1)) {
      ekto_pushnumber(L, ekto_tonumber(L, 1));
      return 1;
    }
  }
  else {
    const char *s1 = ektoL_checkstring(L, 1);
    char *s2;
    unsigned long n;
    ektoL_argcheck(L, 2 <= base && base <= 36, 2, "base out of range");
    n = strtoul(s1, &s2, base);
    if (s1 != s2) {  /* at least one valid digit? */
      while (isspace((unsigned char)(*s2))) s2++;  /* skip trailing spaces */
      if (*s2 == '\0') {  /* no invalid trailing characters? */
        ekto_pushnumber(L, (ekto_Number)n);
        return 1;
      }
    }
  }
  ekto_pushnil(L);  /* else not a number */
  return 1;
}


static int ektoB_error (ekto_State *L) {
  int level = ektoL_optint(L, 2, 1);
  ekto_settop(L, 1);
  if (ekto_isstring(L, 1) && level > 0) {  /* add extra information? */
    ektoL_where(L, level);
    ekto_pushvalue(L, 1);
    ekto_concat(L, 2);
  }
  return ekto_error(L);
}


static int ektoB_getmetatable (ekto_State *L) {
  ektoL_checkany(L, 1);
  if (!ekto_getmetatable(L, 1)) {
    ekto_pushnil(L);
    return 1;  /* no metatable */
  }
  ektoL_getmetafield(L, 1, "__metatable");
  return 1;  /* returns either __metatable field (if present) or metatable */
}


static int ektoB_setmetatable (ekto_State *L) {
  int t = ekto_type(L, 2);
  ektoL_checktype(L, 1, EKTO_TTABLE);
  ektoL_argcheck(L, t == EKTO_TNIL || t == EKTO_TTABLE, 2,
                    "nil or table expected");
  if (ektoL_getmetafield(L, 1, "__metatable"))
    ektoL_error(L, "cannot change a protected metatable");
  ekto_settop(L, 2);
  ekto_setmetatable(L, 1);
  return 1;
}


static void getfunc (ekto_State *L, int opt) {
  if (ekto_isfunction(L, 1)) ekto_pushvalue(L, 1);
  else {
    ekto_Debug ar;
    int level = opt ? ektoL_optint(L, 1, 1) : ektoL_checkint(L, 1);
    ektoL_argcheck(L, level >= 0, 1, "level must be non-negative");
    if (ekto_getstack(L, level, &ar) == 0)
      ektoL_argerror(L, 1, "invalid level");
    ekto_getinfo(L, "f", &ar);
    if (ekto_isnil(L, -1))
      ektoL_error(L, "no function environment for tail call at level %d",
                    level);
  }
}


static int ektoB_getfenv (ekto_State *L) {
  getfunc(L, 1);
  if (ekto_iscfunction(L, -1))  /* is a C function? */
    ekto_pushvalue(L, EKTO_GLOBALSINDEX);  /* return the thread's global env. */
  else
    ekto_getfenv(L, -1);
  return 1;
}


static int ektoB_setfenv (ekto_State *L) {
  ektoL_checktype(L, 2, EKTO_TTABLE);
  getfunc(L, 0);
  ekto_pushvalue(L, 2);
  if (ekto_isnumber(L, 1) && ekto_tonumber(L, 1) == 0) {
    /* change environment of current thread */
    ekto_pushthread(L);
    ekto_insert(L, -2);
    ekto_setfenv(L, -2);
    return 0;
  }
  else if (ekto_iscfunction(L, -2) || ekto_setfenv(L, -2) == 0)
    ektoL_error(L,
          EKTO_QL("setfenv") " cannot change environment of given object");
  return 1;
}


static int ektoB_rawequal (ekto_State *L) {
  ektoL_checkany(L, 1);
  ektoL_checkany(L, 2);
  ekto_pushboolean(L, ekto_rawequal(L, 1, 2));
  return 1;
}


static int ektoB_rawget (ekto_State *L) {
  ektoL_checktype(L, 1, EKTO_TTABLE);
  ektoL_checkany(L, 2);
  ekto_settop(L, 2);
  ekto_rawget(L, 1);
  return 1;
}

static int ektoB_rawset (ekto_State *L) {
  ektoL_checktype(L, 1, EKTO_TTABLE);
  ektoL_checkany(L, 2);
  ektoL_checkany(L, 3);
  ekto_settop(L, 3);
  ekto_rawset(L, 1);
  return 1;
}


static int ektoB_gcinfo (ekto_State *L) {
  ekto_pushinteger(L, ekto_getgccount(L));
  return 1;
}


static int ektoB_collectgarbage (ekto_State *L) {
  static const char *const opts[] = {"stop", "restart", "collect",
    "count", "step", "setpause", "setstepmul", NULL};
  static const int optsnum[] = {EKTO_GCSTOP, EKTO_GCRESTART, EKTO_GCCOLLECT,
    EKTO_GCCOUNT, EKTO_GCSTEP, EKTO_GCSETPAUSE, EKTO_GCSETSTEPMUL};
  int o = ektoL_checkoption(L, 1, "collect", opts);
  int ex = ektoL_optint(L, 2, 0);
  int res = ekto_gc(L, optsnum[o], ex);
  switch (optsnum[o]) {
    case EKTO_GCCOUNT: {
      int b = ekto_gc(L, EKTO_GCCOUNTB, 0);
      ekto_pushnumber(L, res + ((ekto_Number)b/1024));
      return 1;
    }
    case EKTO_GCSTEP: {
      ekto_pushboolean(L, res);
      return 1;
    }
    default: {
      ekto_pushnumber(L, res);
      return 1;
    }
  }
}


static int ektoB_type (ekto_State *L) {
  ektoL_checkany(L, 1);
  ekto_pushstring(L, ektoL_typename(L, 1));
  return 1;
}


static int ektoB_next (ekto_State *L) {
  ektoL_checktype(L, 1, EKTO_TTABLE);
  ekto_settop(L, 2);  /* create a 2nd argument if there isn't one */
  if (ekto_next(L, 1))
    return 2;
  else {
    ekto_pushnil(L);
    return 1;
  }
}


static int ektoB_pairs (ekto_State *L) {
  ektoL_checktype(L, 1, EKTO_TTABLE);
  ekto_pushvalue(L, ekto_upvalueindex(1));  /* return generator, */
  ekto_pushvalue(L, 1);  /* state, */
  ekto_pushnil(L);  /* and initial value */
  return 3;
}


static int ipairsaux (ekto_State *L) {
  int i = ektoL_checkint(L, 2);
  ektoL_checktype(L, 1, EKTO_TTABLE);
  i++;  /* next value */
  ekto_pushinteger(L, i);
  ekto_rawgeti(L, 1, i);
  return (ekto_isnil(L, -1)) ? 0 : 2;
}


static int ektoB_ipairs (ekto_State *L) {
  ektoL_checktype(L, 1, EKTO_TTABLE);
  ekto_pushvalue(L, ekto_upvalueindex(1));  /* return generator, */
  ekto_pushvalue(L, 1);  /* state, */
  ekto_pushinteger(L, 0);  /* and initial value */
  return 3;
}


static int load_aux (ekto_State *L, int status) {
  if (status == 0)  /* OK? */
    return 1;
  else {
    ekto_pushnil(L);
    ekto_insert(L, -2);  /* put before error message */
    return 2;  /* return nil plus error message */
  }
}


static int ektoB_loadstring (ekto_State *L) {
  size_t l;
  const char *s = ektoL_checklstring(L, 1, &l);
  const char *chunkname = ektoL_optstring(L, 2, s);
  return load_aux(L, ektoL_loadbuffer(L, s, l, chunkname));
}


static int ektoB_loadfile (ekto_State *L) {
  const char *fname = ektoL_optstring(L, 1, NULL);
  return load_aux(L, ektoL_loadfile(L, fname));
}


/*
** Reader for generic `load' function: `ekto_load' uses the
** stack for internal stuff, so the reader cannot change the
** stack top. Instead, it keeps its resulting string in a
** reserved slot inside the stack.
*/
static const char *generic_reader (ekto_State *L, void *ud, size_t *size) {
  (void)ud;  /* to avoid warnings */
  ektoL_checkstack(L, 2, "too many nested functions");
  ekto_pushvalue(L, 1);  /* get function */
  ekto_call(L, 0, 1);  /* call it */
  if (ekto_isnil(L, -1)) {
    *size = 0;
    return NULL;
  }
  else if (ekto_isstring(L, -1)) {
    ekto_replace(L, 3);  /* save string in a reserved stack slot */
    return ekto_tolstring(L, 3, size);
  }
  else ektoL_error(L, "reader function must return a string");
  return NULL;  /* to avoid warnings */
}


static int ektoB_load (ekto_State *L) {
  int status;
  const char *cname = ektoL_optstring(L, 2, "=(load)");
  ektoL_checktype(L, 1, EKTO_TFUNCTION);
  ekto_settop(L, 3);  /* function, eventual name, plus one reserved slot */
  status = ekto_load(L, generic_reader, NULL, cname);
  return load_aux(L, status);
}


static int ektoB_dofile (ekto_State *L) {
  const char *fname = ektoL_optstring(L, 1, NULL);
  int n = ekto_gettop(L);
  if (ektoL_loadfile(L, fname) != 0) ekto_error(L);
  ekto_call(L, 0, EKTO_MULTRET);
  return ekto_gettop(L) - n;
}


static int ektoB_assert (ekto_State *L) {
  ektoL_checkany(L, 1);
  if (!ekto_toboolean(L, 1))
    return ektoL_error(L, "%s", ektoL_optstring(L, 2, "assertion failed!"));
  return ekto_gettop(L);
}


static int ektoB_unpack (ekto_State *L) {
  int i, e, n;
  ektoL_checktype(L, 1, EKTO_TTABLE);
  i = ektoL_optint(L, 2, 1);
  e = ektoL_opt(L, ektoL_checkint, 3, ektoL_getn(L, 1));
  if (i > e) return 0;  /* empty range */
  n = e - i + 1;  /* number of elements */
  if (n <= 0 || !ekto_checkstack(L, n))  /* n <= 0 means arith. overflow */
    return ektoL_error(L, "too many results to unpack");
  ekto_rawgeti(L, 1, i);  /* push arg[i] (avoiding overflow problems) */
  while (i++ < e)  /* push arg[i + 1...e] */
    ekto_rawgeti(L, 1, i);
  return n;
}


static int ektoB_select (ekto_State *L) {
  int n = ekto_gettop(L);
  if (ekto_type(L, 1) == EKTO_TSTRING && *ekto_tostring(L, 1) == '#') {
    ekto_pushinteger(L, n-1);
    return 1;
  }
  else {
    int i = ektoL_checkint(L, 1);
    if (i < 0) i = n + i;
    else if (i > n) i = n;
    ektoL_argcheck(L, 1 <= i, 1, "index out of range");
    return n - i;
  }
}


static int ektoB_pcall (ekto_State *L) {
  int status;
  ektoL_checkany(L, 1);
  status = ekto_pcall(L, ekto_gettop(L) - 1, EKTO_MULTRET, 0);
  ekto_pushboolean(L, (status == 0));
  ekto_insert(L, 1);
  return ekto_gettop(L);  /* return status + all results */
}


static int ektoB_xpcall (ekto_State *L) {
  int status;
  ektoL_checkany(L, 2);
  ekto_settop(L, 2);
  ekto_insert(L, 1);  /* put error function under function to be called */
  status = ekto_pcall(L, 0, EKTO_MULTRET, 1);
  ekto_pushboolean(L, (status == 0));
  ekto_replace(L, 1);
  return ekto_gettop(L);  /* return status + all results */
}


static int ektoB_tostring (ekto_State *L) {
  ektoL_checkany(L, 1);
  if (ektoL_callmeta(L, 1, "__tostring"))  /* is there a metafield? */
    return 1;  /* use its value */
  switch (ekto_type(L, 1)) {
    case EKTO_TNUMBER:
      ekto_pushstring(L, ekto_tostring(L, 1));
      break;
    case EKTO_TSTRING:
      ekto_pushvalue(L, 1);
      break;
    case EKTO_TBOOLEAN:
      ekto_pushstring(L, (ekto_toboolean(L, 1) ? "true" : "false"));
      break;
    case EKTO_TNIL:
      ekto_pushliteral(L, "nil");
      break;
    default:
      ekto_pushfstring(L, "%s: %p", ektoL_typename(L, 1), ekto_topointer(L, 1));
      break;
  }
  return 1;
}


static int ektoB_newproxy (ekto_State *L) {
  ekto_settop(L, 1);
  ekto_newuserdata(L, 0);  /* create proxy */
  if (ekto_toboolean(L, 1) == 0)
    return 1;  /* no metatable */
  else if (ekto_isboolean(L, 1)) {
    ekto_newtable(L);  /* create a new metatable `m' ... */
    ekto_pushvalue(L, -1);  /* ... and mark `m' as a valid metatable */
    ekto_pushboolean(L, 1);
    ekto_rawset(L, ekto_upvalueindex(1));  /* weaktable[m] = true */
  }
  else {
    int validproxy = 0;  /* to check if weaktable[metatable(u)] == true */
    if (ekto_getmetatable(L, 1)) {
      ekto_rawget(L, ekto_upvalueindex(1));
      validproxy = ekto_toboolean(L, -1);
      ekto_pop(L, 1);  /* remove value */
    }
    ektoL_argcheck(L, validproxy, 1, "boolean or proxy expected");
    ekto_getmetatable(L, 1);  /* metatable is valid; get it */
  }
  ekto_setmetatable(L, 2);
  return 1;
}


static const ektoL_Reg base_funcs[] = {
  {"assert", ektoB_assert},
  {"collectgarbage", ektoB_collectgarbage},
  {"dofile", ektoB_dofile},
  {"error", ektoB_error},
  {"gcinfo", ektoB_gcinfo},
  {"getfenv", ektoB_getfenv},
  {"getmetatable", ektoB_getmetatable},
  {"loadfile", ektoB_loadfile},
  {"load", ektoB_load},
  {"loadstring", ektoB_loadstring},
  {"next", ektoB_next},
  {"pcall", ektoB_pcall},
  {"print", ektoB_print},
  {"rawequal", ektoB_rawequal},
  {"rawget", ektoB_rawget},
  {"rawset", ektoB_rawset},
  {"select", ektoB_select},
  {"setfenv", ektoB_setfenv},
  {"setmetatable", ektoB_setmetatable},
  {"tonumber", ektoB_tonumber},
  {"tostring", ektoB_tostring},
  {"type", ektoB_type},
  {"unpack", ektoB_unpack},
  {"xpcall", ektoB_xpcall},
  {NULL, NULL}
};


/*
** {======================================================
** Coroutine library
** =======================================================
*/

#define CO_RUN	0	/* running */
#define CO_SUS	1	/* suspended */
#define CO_NOR	2	/* 'normal' (it resumed another coroutine) */
#define CO_DEAD	3

static const char *const statnames[] =
    {"running", "suspended", "normal", "dead"};

static int costatus (ekto_State *L, ekto_State *co) {
  if (L == co) return CO_RUN;
  switch (ekto_status(co)) {
    case EKTO_YIELD:
      return CO_SUS;
    case 0: {
      ekto_Debug ar;
      if (ekto_getstack(co, 0, &ar) > 0)  /* does it have frames? */
        return CO_NOR;  /* it is running */
      else if (ekto_gettop(co) == 0)
          return CO_DEAD;
      else
        return CO_SUS;  /* initial state */
    }
    default:  /* some error occured */
      return CO_DEAD;
  }
}


static int ektoB_costatus (ekto_State *L) {
  ekto_State *co = ekto_tothread(L, 1);
  ektoL_argcheck(L, co, 1, "coroutine expected");
  ekto_pushstring(L, statnames[costatus(L, co)]);
  return 1;
}


static int auxresume (ekto_State *L, ekto_State *co, int narg) {
  int status = costatus(L, co);
  if (!ekto_checkstack(co, narg))
    ektoL_error(L, "too many arguments to resume");
  if (status != CO_SUS) {
    ekto_pushfstring(L, "cannot resume %s coroutine", statnames[status]);
    return -1;  /* error flag */
  }
  ekto_xmove(L, co, narg);
  ekto_setlevel(L, co);
  status = ekto_resume(co, narg);
  if (status == 0 || status == EKTO_YIELD) {
    int nres = ekto_gettop(co);
    if (!ekto_checkstack(L, nres + 1))
      ektoL_error(L, "too many results to resume");
    ekto_xmove(co, L, nres);  /* move yielded values */
    return nres;
  }
  else {
    ekto_xmove(co, L, 1);  /* move error message */
    return -1;  /* error flag */
  }
}


static int ektoB_coresume (ekto_State *L) {
  ekto_State *co = ekto_tothread(L, 1);
  int r;
  ektoL_argcheck(L, co, 1, "coroutine expected");
  r = auxresume(L, co, ekto_gettop(L) - 1);
  if (r < 0) {
    ekto_pushboolean(L, 0);
    ekto_insert(L, -2);
    return 2;  /* return false + error message */
  }
  else {
    ekto_pushboolean(L, 1);
    ekto_insert(L, -(r + 1));
    return r + 1;  /* return true + `resume' returns */
  }
}


static int ektoB_auxwrap (ekto_State *L) {
  ekto_State *co = ekto_tothread(L, ekto_upvalueindex(1));
  int r = auxresume(L, co, ekto_gettop(L));
  if (r < 0) {
    if (ekto_isstring(L, -1)) {  /* error object is a string? */
      ektoL_where(L, 1);  /* add extra info */
      ekto_insert(L, -2);
      ekto_concat(L, 2);
    }
    ekto_error(L);  /* propagate error */
  }
  return r;
}


static int ektoB_cocreate (ekto_State *L) {
  ekto_State *NL = ekto_newthread(L);
  ektoL_argcheck(L, ekto_isfunction(L, 1) && !ekto_iscfunction(L, 1), 1,
    "Ekto function expected");
  ekto_pushvalue(L, 1);  /* move function to top */
  ekto_xmove(L, NL, 1);  /* move function from L to NL */
  return 1;
}


static int ektoB_cowrap (ekto_State *L) {
  ektoB_cocreate(L);
  ekto_pushcclosure(L, ektoB_auxwrap, 1);
  return 1;
}


static int ektoB_yield (ekto_State *L) {
  return ekto_yield(L, ekto_gettop(L));
}


static int ektoB_corunning (ekto_State *L) {
  if (ekto_pushthread(L))
    ekto_pushnil(L);  /* main thread is not a coroutine */
  return 1;
}


static const ektoL_Reg co_funcs[] = {
  {"create", ektoB_cocreate},
  {"resume", ektoB_coresume},
  {"running", ektoB_corunning},
  {"status", ektoB_costatus},
  {"wrap", ektoB_cowrap},
  {"yield", ektoB_yield},
  {NULL, NULL}
};

/* }====================================================== */


static void auxopen (ekto_State *L, const char *name,
                     ekto_CFunction f, ekto_CFunction u) {
  ekto_pushcfunction(L, u);
  ekto_pushcclosure(L, f, 1);
  ekto_setfield(L, -2, name);
}


static void base_open (ekto_State *L) {
  /* set global _G */
  ekto_pushvalue(L, EKTO_GLOBALSINDEX);
  ekto_setglobal(L, "_G");
  /* open lib into global table */
  ektoL_register(L, "_G", base_funcs);
  ekto_pushliteral(L, EKTO_VERSION);
  ekto_setglobal(L, "_VERSION");  /* set global _VERSION */
  /* `ipairs' and `pairs' need auxliliary functions as upvalues */
  auxopen(L, "ipairs", ektoB_ipairs, ipairsaux);
  auxopen(L, "pairs", ektoB_pairs, ektoB_next);
  /* `newproxy' needs a weaktable as upvalue */
  ekto_createtable(L, 0, 1);  /* new table `w' */
  ekto_pushvalue(L, -1);  /* `w' will be its own metatable */
  ekto_setmetatable(L, -2);
  ekto_pushliteral(L, "kv");
  ekto_setfield(L, -2, "__mode");  /* metatable(w).__mode = "kv" */
  ekto_pushcclosure(L, ektoB_newproxy, 1);
  ekto_setglobal(L, "newproxy");  /* set global `newproxy' */
}


EKTOLIB_API int ektoopen_base (ekto_State *L) {
  base_open(L);
  ektoL_register(L, EKTO_COLIBNAME, co_funcs);
  return 2;
}

