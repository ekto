/*
** $Id: lmathlib.c,v 1.67.1.1 2007/12/27 13:02:25 roberto Exp $
** Standard mathematical library
** See Copyright Notice in ekto.h
*/


#include <stdlib.h>
#include <math.h>

#define lmathlib_c
#define EKTO_LIB

#include "Ekto.h"

#include "Ekto_Aux.h"
#include "Ekto_Lib.h"


#undef PI
#define PI (3.14159265358979323846)
#define RADIANS_PER_DEGREE (PI/180.0)



static int math_abs (ekto_State *L) {
  ekto_pushnumber(L, fabs(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_sin (ekto_State *L) {
  ekto_pushnumber(L, sin(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_sinh (ekto_State *L) {
  ekto_pushnumber(L, sinh(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_cos (ekto_State *L) {
  ekto_pushnumber(L, cos(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_cosh (ekto_State *L) {
  ekto_pushnumber(L, cosh(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_tan (ekto_State *L) {
  ekto_pushnumber(L, tan(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_tanh (ekto_State *L) {
  ekto_pushnumber(L, tanh(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_asin (ekto_State *L) {
  ekto_pushnumber(L, asin(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_acos (ekto_State *L) {
  ekto_pushnumber(L, acos(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_atan (ekto_State *L) {
  ekto_pushnumber(L, atan(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_atan2 (ekto_State *L) {
  ekto_pushnumber(L, atan2(ektoL_checknumber(L, 1), ektoL_checknumber(L, 2)));
  return 1;
}

static int math_ceil (ekto_State *L) {
  ekto_pushnumber(L, ceil(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_floor (ekto_State *L) {
  ekto_pushnumber(L, floor(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_fmod (ekto_State *L) {
  ekto_pushnumber(L, fmod(ektoL_checknumber(L, 1), ektoL_checknumber(L, 2)));
  return 1;
}

static int math_modf (ekto_State *L) {
  double ip;
  double fp = modf(ektoL_checknumber(L, 1), &ip);
  ekto_pushnumber(L, ip);
  ekto_pushnumber(L, fp);
  return 2;
}

static int math_sqrt (ekto_State *L) {
  ekto_pushnumber(L, sqrt(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_pow (ekto_State *L) {
  ekto_pushnumber(L, pow(ektoL_checknumber(L, 1), ektoL_checknumber(L, 2)));
  return 1;
}

static int math_log (ekto_State *L) {
  ekto_pushnumber(L, log(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_log10 (ekto_State *L) {
  ekto_pushnumber(L, log10(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_exp (ekto_State *L) {
  ekto_pushnumber(L, exp(ektoL_checknumber(L, 1)));
  return 1;
}

static int math_deg (ekto_State *L) {
  ekto_pushnumber(L, ektoL_checknumber(L, 1)/RADIANS_PER_DEGREE);
  return 1;
}

static int math_rad (ekto_State *L) {
  ekto_pushnumber(L, ektoL_checknumber(L, 1)*RADIANS_PER_DEGREE);
  return 1;
}

static int math_frexp (ekto_State *L) {
  int e;
  ekto_pushnumber(L, frexp(ektoL_checknumber(L, 1), &e));
  ekto_pushinteger(L, e);
  return 2;
}

static int math_ldexp (ekto_State *L) {
  ekto_pushnumber(L, ldexp(ektoL_checknumber(L, 1), ektoL_checkint(L, 2)));
  return 1;
}



static int math_min (ekto_State *L) {
  int n = ekto_gettop(L);  /* number of arguments */
  ekto_Number dmin = ektoL_checknumber(L, 1);
  int i;
  for (i=2; i<=n; i++) {
    ekto_Number d = ektoL_checknumber(L, i);
    if (d < dmin)
      dmin = d;
  }
  ekto_pushnumber(L, dmin);
  return 1;
}


static int math_max (ekto_State *L) {
  int n = ekto_gettop(L);  /* number of arguments */
  ekto_Number dmax = ektoL_checknumber(L, 1);
  int i;
  for (i=2; i<=n; i++) {
    ekto_Number d = ektoL_checknumber(L, i);
    if (d > dmax)
      dmax = d;
  }
  ekto_pushnumber(L, dmax);
  return 1;
}


static int math_random (ekto_State *L) {
  /* the `%' avoids the (rare) case of r==1, and is needed also because on
     some systems (SunOS!) `rand()' may return a value larger than RAND_MAX */
  ekto_Number r = (ekto_Number)(rand()%RAND_MAX) / (ekto_Number)RAND_MAX;
  switch (ekto_gettop(L)) {  /* check number of arguments */
    case 0: {  /* no arguments */
      ekto_pushnumber(L, r);  /* Number between 0 and 1 */
      break;
    }
    case 1: {  /* only upper limit */
      int u = ektoL_checkint(L, 1);
      ektoL_argcheck(L, 1<=u, 1, "interval is empty");
      ekto_pushnumber(L, floor(r*u)+1);  /* int between 1 and `u' */
      break;
    }
    case 2: {  /* lower and upper limits */
      int l = ektoL_checkint(L, 1);
      int u = ektoL_checkint(L, 2);
      ektoL_argcheck(L, l<=u, 2, "interval is empty");
      ekto_pushnumber(L, floor(r*(u-l+1))+l);  /* int between `l' and `u' */
      break;
    }
    default: return ektoL_error(L, "wrong number of arguments");
  }
  return 1;
}


static int math_randomseed (ekto_State *L) {
  srand(ektoL_checkint(L, 1));
  return 0;
}


static const ektoL_Reg mathlib[] = {
  {"abs",   math_abs},
  {"acos",  math_acos},
  {"asin",  math_asin},
  {"atan2", math_atan2},
  {"atan",  math_atan},
  {"ceil",  math_ceil},
  {"cosh",   math_cosh},
  {"cos",   math_cos},
  {"deg",   math_deg},
  {"exp",   math_exp},
  {"floor", math_floor},
  {"fmod",   math_fmod},
  {"frexp", math_frexp},
  {"ldexp", math_ldexp},
  {"log10", math_log10},
  {"log",   math_log},
  {"max",   math_max},
  {"min",   math_min},
  {"modf",   math_modf},
  {"pow",   math_pow},
  {"rad",   math_rad},
  {"random",     math_random},
  {"randomseed", math_randomseed},
  {"sinh",   math_sinh},
  {"sin",   math_sin},
  {"sqrt",  math_sqrt},
  {"tanh",   math_tanh},
  {"tan",   math_tan},
  {NULL, NULL}
};


/*
** Open math library
*/
EKTOLIB_API int ektoopen_math (ekto_State *L) {
  ektoL_register(L, EKTO_MATHLIBNAME, mathlib);
  ekto_pushnumber(L, PI);
  ekto_setfield(L, -2, "pi");
  ekto_pushnumber(L, HUGE_VAL);
  ekto_setfield(L, -2, "huge");
#if defined(EKTO_COMPAT_MOD)
  ekto_getfield(L, -1, "fmod");
  ekto_setfield(L, -2, "mod");
#endif
  return 1;
}

