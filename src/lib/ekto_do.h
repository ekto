/*
** $Id: ldo.h,v 2.7.1.1 2007/12/27 13:02:25 roberto Exp $
** Stack and Call structure of Ekto
** See Copyright Notice in ekto.h
*/

#ifndef ldo_h
#define ldo_h


#include "ekto_object.h"
#include "ekto_state.h"
#include "ekto_zio.h"


#define ektoD_checkstack(L,n)	\
  if ((char *)L->stack_last - (char *)L->top <= (n)*(int)sizeof(TValue)) \
    ektoD_growstack(L, n); \
  else condhardstacktests(ektoD_reallocstack(L, L->stacksize - EXTRA_STACK - 1));


#define incr_top(L) {ektoD_checkstack(L,1); L->top++;}

#define savestack(L,p)		((char *)(p) - (char *)L->stack)
#define restorestack(L,n)	((TValue *)((char *)L->stack + (n)))

#define saveci(L,p)		((char *)(p) - (char *)L->base_ci)
#define restoreci(L,n)		((CallInfo *)((char *)L->base_ci + (n)))


/* results from ektoD_precall */
#define PCREKTO		0	/* initiated a call to a Ekto function */
#define PCRC		1	/* did a call to a C function */
#define PCRYIELD	2	/* C funtion yielded */


/* type of protected functions, to be ran by `runprotected' */
typedef void (*Pfunc) (ekto_State *L, void *ud);

EKTOI_FUNC int ektoD_protectedparser (ekto_State *L, ZIO *z, const char *name);
EKTOI_FUNC void ektoD_callhook (ekto_State *L, int event, int line);
EKTOI_FUNC int ektoD_precall (ekto_State *L, StkId func, int nresults);
EKTOI_FUNC void ektoD_call (ekto_State *L, StkId func, int nResults);
EKTOI_FUNC int ektoD_pcall (ekto_State *L, Pfunc func, void *u,
                                        ptrdiff_t oldtop, ptrdiff_t ef);
EKTOI_FUNC int ektoD_poscall (ekto_State *L, StkId firstResult);
EKTOI_FUNC void ektoD_reallocCI (ekto_State *L, int newsize);
EKTOI_FUNC void ektoD_reallocstack (ekto_State *L, int newsize);
EKTOI_FUNC void ektoD_growstack (ekto_State *L, int n);

EKTOI_FUNC void ektoD_throw (ekto_State *L, int errcode);
EKTOI_FUNC int ektoD_rawrunprotected (ekto_State *L, Pfunc f, void *ud);

EKTOI_FUNC void ektoD_seterrorobj (ekto_State *L, int errcode, StkId oldtop);

#endif

