/*
** $Id: loadlib.c,v 1.52.1.3 2008/08/06 13:29:28 roberto Exp $
** Dynamic library loader for Ekto
** See Copyright Notice in ekto.h
**
** This module contains an implementation of loadlib for Unix systems
** that have dlfcn, an implementation for Darwin (Mac OS X), an
** implementation for Windows, and a stub for other systems.
*/


#include <stdlib.h>
#include <string.h>


#define loadlib_c
#define EKTO_LIB

#include "Ekto.h"

#include "Ekto_Aux.h"
#include "Ekto_Lib.h"


/* prefix for open functions in C libraries */
#define EKTO_POF		"ektoopen_"

/* separator for open functions in C libraries */
#define EKTO_OFSEP	"_"


#define LIBPREFIX	"LOADLIB: "

#define POF		EKTO_POF
#define LIB_FAIL	"open"


/* error codes for ll_loadfunc */
#define ERRLIB		1
#define ERRFUNC		2

#define setprogdir(L)		((void)0)


static void ll_unloadlib (void *lib);
static void *ll_load (ekto_State *L, const char *path);
static ekto_CFunction ll_sym (ekto_State *L, void *lib, const char *sym);



#if defined(EKTO_DL_DLOPEN)
/*
** {========================================================================
** This is an implementation of loadlib based on the dlfcn interface.
** The dlfcn interface is available in Linux, SunOS, Solaris, IRIX, FreeBSD,
** NetBSD, AIX 4.2, HPUX 11, and  probably most other Unix flavors, at least
** as an emulation layer on top of native functions.
** =========================================================================
*/

#include <dlfcn.h>

static void ll_unloadlib (void *lib) {
  dlclose(lib);
}


static void *ll_load (ekto_State *L, const char *path) {
  void *lib = dlopen(path, RTLD_NOW);
  if (lib == NULL) ekto_pushstring(L, dlerror());
  return lib;
}


static ekto_CFunction ll_sym (ekto_State *L, void *lib, const char *sym) {
  ekto_CFunction f = (ekto_CFunction)dlsym(lib, sym);
  if (f == NULL) ekto_pushstring(L, dlerror());
  return f;
}

/* }====================================================== */



#elif defined(EKTO_DL_DLL)
/*
** {======================================================================
** This is an implementation of loadlib for Windows using native functions.
** =======================================================================
*/

#include <windows.h>


#undef setprogdir

static void setprogdir (ekto_State *L) {
  char buff[MAX_PATH + 1];
  char *lb;
  DWORD nsize = sizeof(buff)/sizeof(char);
  DWORD n = GetModuleFileNameA(NULL, buff, nsize);
  if (n == 0 || n == nsize || (lb = strrchr(buff, '\\')) == NULL)
    ektoL_error(L, "unable to get ModuleFileName");
  else {
    *lb = '\0';
    ektoL_gsub(L, ekto_tostring(L, -1), EKTO_EXECDIR, buff);
    ekto_remove(L, -2);  /* remove original string */
  }
}


static void pusherror (ekto_State *L) {
  int error = GetLastError();
  char buffer[128];
  if (FormatMessageA(FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM,
      NULL, error, 0, buffer, sizeof(buffer), NULL))
    ekto_pushstring(L, buffer);
  else
    ekto_pushfstring(L, "system error %d\n", error);
}

static void ll_unloadlib (void *lib) {
  FreeLibrary((HINSTANCE)lib);
}


static void *ll_load (ekto_State *L, const char *path) {
  HINSTANCE lib = LoadLibraryA(path);
  if (lib == NULL) pusherror(L);
  return lib;
}


static ekto_CFunction ll_sym (ekto_State *L, void *lib, const char *sym) {
  ekto_CFunction f = (ekto_CFunction)GetProcAddress((HINSTANCE)lib, sym);
  if (f == NULL) pusherror(L);
  return f;
}

/* }====================================================== */



#elif defined(EKTO_DL_DYLD)
/*
** {======================================================================
** Native Mac OS X / Darwin Implementation
** =======================================================================
*/

#include <mach-o/dyld.h>


/* Mac appends a `_' before C function names */
#undef POF
#define POF	"_" EKTO_POF


static void pusherror (ekto_State *L) {
  const char *err_str;
  const char *err_file;
  NSLinkEditErrors err;
  int err_num;
  NSLinkEditError(&err, &err_num, &err_file, &err_str);
  ekto_pushstring(L, err_str);
}


static const char *errorfromcode (NSObjectFileImageReturnCode ret) {
  switch (ret) {
    case NSObjectFileImageInappropriateFile:
      return "file is not a bundle";
    case NSObjectFileImageArch:
      return "library is for wrong CPU type";
    case NSObjectFileImageFormat:
      return "bad format";
    case NSObjectFileImageAccess:
      return "cannot access file";
    case NSObjectFileImageFailure:
    default:
      return "unable to load library";
  }
}


static void ll_unloadlib (void *lib) {
  NSUnLinkModule((NSModule)lib, NSUNLINKMODULE_OPTION_RESET_LAZY_REFERENCES);
}


static void *ll_load (ekto_State *L, const char *path) {
  NSObjectFileImage img;
  NSObjectFileImageReturnCode ret;
  /* this would be a rare case, but prevents crashing if it happens */
  if(!_dyld_present()) {
    ekto_pushliteral(L, "dyld not present");
    return NULL;
  }
  ret = NSCreateObjectFileImageFromFile(path, &img);
  if (ret == NSObjectFileImageSuccess) {
    NSModule mod = NSLinkModule(img, path, NSLINKMODULE_OPTION_PRIVATE |
                       NSLINKMODULE_OPTION_RETURN_ON_ERROR);
    NSDestroyObjectFileImage(img);
    if (mod == NULL) pusherror(L);
    return mod;
  }
  ekto_pushstring(L, errorfromcode(ret));
  return NULL;
}


static ekto_CFunction ll_sym (ekto_State *L, void *lib, const char *sym) {
  NSSymbol nss = NSLookupSymbolInModule((NSModule)lib, sym);
  if (nss == NULL) {
    ekto_pushfstring(L, "symbol " EKTO_QS " not found", sym);
    return NULL;
  }
  return (ekto_CFunction)NSAddressOfSymbol(nss);
}

/* }====================================================== */



#else
/*
** {======================================================
** Fallback for other systems
** =======================================================
*/

#undef LIB_FAIL
#define LIB_FAIL	"absent"


#define DLMSG	"dynamic libraries not enabled; check your Ekto installation"


static void ll_unloadlib (void *lib) {
  (void)lib;  /* to avoid warnings */
}


static void *ll_load (ekto_State *L, const char *path) {
  (void)path;  /* to avoid warnings */
  ekto_pushliteral(L, DLMSG);
  return NULL;
}


static ekto_CFunction ll_sym (ekto_State *L, void *lib, const char *sym) {
  (void)lib; (void)sym;  /* to avoid warnings */
  ekto_pushliteral(L, DLMSG);
  return NULL;
}

/* }====================================================== */
#endif



static void **ll_register (ekto_State *L, const char *path) {
  void **plib;
  ekto_pushfstring(L, "%s%s", LIBPREFIX, path);
  ekto_gettable(L, EKTO_REGISTRYINDEX);  /* check library in registry? */
  if (!ekto_isnil(L, -1))  /* is there an entry? */
    plib = (void **)ekto_touserdata(L, -1);
  else {  /* no entry yet; create one */
    ekto_pop(L, 1);
    plib = (void **)ekto_newuserdata(L, sizeof(const void *));
    *plib = NULL;
    ektoL_getmetatable(L, "_LOADLIB");
    ekto_setmetatable(L, -2);
    ekto_pushfstring(L, "%s%s", LIBPREFIX, path);
    ekto_pushvalue(L, -2);
    ekto_settable(L, EKTO_REGISTRYINDEX);
  }
  return plib;
}


/*
** __gc tag method: calls library's `ll_unloadlib' function with the lib
** handle
*/
static int gctm (ekto_State *L) {
  void **lib = (void **)ektoL_checkudata(L, 1, "_LOADLIB");
  if (*lib) ll_unloadlib(*lib);
  *lib = NULL;  /* mark library as closed */
  return 0;
}


static int ll_loadfunc (ekto_State *L, const char *path, const char *sym) {
  void **reg = ll_register(L, path);
  if (*reg == NULL) *reg = ll_load(L, path);
  if (*reg == NULL)
    return ERRLIB;  /* unable to load library */
  else {
    ekto_CFunction f = ll_sym(L, *reg, sym);
    if (f == NULL)
      return ERRFUNC;  /* unable to find function */
    ekto_pushcfunction(L, f);
    return 0;  /* return function */
  }
}


static int ll_loadlib (ekto_State *L) {
  const char *path = ektoL_checkstring(L, 1);
  const char *init = ektoL_checkstring(L, 2);
  int stat = ll_loadfunc(L, path, init);
  if (stat == 0)  /* no errors? */
    return 1;  /* return the loaded function */
  else {  /* error; error message is on stack top */
    ekto_pushnil(L);
    ekto_insert(L, -2);
    ekto_pushstring(L, (stat == ERRLIB) ?  LIB_FAIL : "init");
    return 3;  /* return nil, error message, and where */
  }
}



/*
** {======================================================
** 'require' function
** =======================================================
*/


static int readable (const char *filename) {
  FILE *f = fopen(filename, "r");  /* try to open file */
  if (f == NULL) return 0;  /* open failed */
  fclose(f);
  return 1;
}


static const char *pushnexttemplate (ekto_State *L, const char *path) {
  const char *l;
  while (*path == *EKTO_PATHSEP) path++;  /* skip separators */
  if (*path == '\0') return NULL;  /* no more templates */
  l = strchr(path, *EKTO_PATHSEP);  /* find next separator */
  if (l == NULL) l = path + strlen(path);
  ekto_pushlstring(L, path, l - path);  /* template */
  return l;
}


static const char *findfile (ekto_State *L, const char *name,
                                           const char *pname) {
  const char *path;
  name = ektoL_gsub(L, name, ".", EKTO_DIRSEP);
  ekto_getfield(L, EKTO_ENVIRONINDEX, pname);
  path = ekto_tostring(L, -1);
  if (path == NULL)
    ektoL_error(L, EKTO_QL("package.%s") " must be a string", pname);
  ekto_pushliteral(L, "");  /* error accumulator */
  while ((path = pushnexttemplate(L, path)) != NULL) {
    const char *filename;
    filename = ektoL_gsub(L, ekto_tostring(L, -1), EKTO_PATH_MARK, name);
    ekto_remove(L, -2);  /* remove path template */
    if (readable(filename))  /* does file exist and is readable? */
      return filename;  /* return that file name */
    ekto_pushfstring(L, "\n\tno file " EKTO_QS, filename);
    ekto_remove(L, -2);  /* remove file name */
    ekto_concat(L, 2);  /* add entry to possible error message */
  }
  return NULL;  /* not found */
}


static void loaderror (ekto_State *L, const char *filename) {
  ektoL_error(L, "error loading module " EKTO_QS " from file " EKTO_QS ":\n\t%s",
                ekto_tostring(L, 1), filename, ekto_tostring(L, -1));
}


static int loader_Ekto (ekto_State *L) {
  const char *filename;
  const char *name = ektoL_checkstring(L, 1);
  filename = findfile(L, name, "path");
  if (filename == NULL) return 1;  /* library not found in this path */
  if (ektoL_loadfile(L, filename) != 0)
    loaderror(L, filename);
  return 1;  /* library loaded successfully */
}


static const char *mkfuncname (ekto_State *L, const char *modname) {
  const char *funcname;
  const char *mark = strchr(modname, *EKTO_IGMARK);
  if (mark) modname = mark + 1;
  funcname = ektoL_gsub(L, modname, ".", EKTO_OFSEP);
  funcname = ekto_pushfstring(L, POF"%s", funcname);
  ekto_remove(L, -2);  /* remove 'gsub' result */
  return funcname;
}


static int loader_C (ekto_State *L) {
  const char *funcname;
  const char *name = ektoL_checkstring(L, 1);
  const char *filename = findfile(L, name, "cpath");
  if (filename == NULL) return 1;  /* library not found in this path */
  funcname = mkfuncname(L, name);
  if (ll_loadfunc(L, filename, funcname) != 0)
    loaderror(L, filename);
  return 1;  /* library loaded successfully */
}


static int loader_Croot (ekto_State *L) {
  const char *funcname;
  const char *filename;
  const char *name = ektoL_checkstring(L, 1);
  const char *p = strchr(name, '.');
  int stat;
  if (p == NULL) return 0;  /* is root */
  ekto_pushlstring(L, name, p - name);
  filename = findfile(L, ekto_tostring(L, -1), "cpath");
  if (filename == NULL) return 1;  /* root not found */
  funcname = mkfuncname(L, name);
  if ((stat = ll_loadfunc(L, filename, funcname)) != 0) {
    if (stat != ERRFUNC) loaderror(L, filename);  /* real error */
    ekto_pushfstring(L, "\n\tno module " EKTO_QS " in file " EKTO_QS,
                       name, filename);
    return 1;  /* function not found */
  }
  return 1;
}


static int loader_preload (ekto_State *L) {
  const char *name = ektoL_checkstring(L, 1);
  ekto_getfield(L, EKTO_ENVIRONINDEX, "preload");
  if (!ekto_istable(L, -1))
    ektoL_error(L, EKTO_QL("package.preload") " must be a table");
  ekto_getfield(L, -1, name);
  if (ekto_isnil(L, -1))  /* not found? */
    ekto_pushfstring(L, "\n\tno field package.preload['%s']", name);
  return 1;
}


static const int sentinel_ = 0;
#define sentinel	((void *)&sentinel_)


static int ll_require (ekto_State *L) {
  const char *name = ektoL_checkstring(L, 1);
  int i;
  ekto_settop(L, 1);  /* _LOADED table will be at index 2 */
  ekto_getfield(L, EKTO_REGISTRYINDEX, "_LOADED");
  ekto_getfield(L, 2, name);
  if (ekto_toboolean(L, -1)) {  /* is it there? */
    if (ekto_touserdata(L, -1) == sentinel)  /* check loops */
      ektoL_error(L, "loop or previous error loading module " EKTO_QS, name);
    return 1;  /* package is already loaded */
  }
  /* else must load it; iterate over available loaders */
  ekto_getfield(L, EKTO_ENVIRONINDEX, "loaders");
  if (!ekto_istable(L, -1))
    ektoL_error(L, EKTO_QL("package.loaders") " must be a table");
  ekto_pushliteral(L, "");  /* error message accumulator */
  for (i=1; ; i++) {
    ekto_rawgeti(L, -2, i);  /* get a loader */
    if (ekto_isnil(L, -1))
      ektoL_error(L, "module " EKTO_QS " not found:%s",
                    name, ekto_tostring(L, -2));
    ekto_pushstring(L, name);
    ekto_call(L, 1, 1);  /* call it */
    if (ekto_isfunction(L, -1))  /* did it find module? */
      break;  /* module loaded successfully */
    else if (ekto_isstring(L, -1))  /* loader returned error message? */
      ekto_concat(L, 2);  /* accumulate it */
    else
      ekto_pop(L, 1);
  }
  ekto_pushlightuserdata(L, sentinel);
  ekto_setfield(L, 2, name);  /* _LOADED[name] = sentinel */
  ekto_pushstring(L, name);  /* pass name as argument to module */
  ekto_call(L, 1, 1);  /* run loaded module */
  if (!ekto_isnil(L, -1))  /* non-nil return? */
    ekto_setfield(L, 2, name);  /* _LOADED[name] = returned value */
  ekto_getfield(L, 2, name);
  if (ekto_touserdata(L, -1) == sentinel) {   /* module did not set a value? */
    ekto_pushboolean(L, 1);  /* use true as result */
    ekto_pushvalue(L, -1);  /* extra copy to be returned */
    ekto_setfield(L, 2, name);  /* _LOADED[name] = true */
  }
  return 1;
}

/* }====================================================== */



/*
** {======================================================
** 'module' function
** =======================================================
*/
  

static void setfenv (ekto_State *L) {
  ekto_Debug ar;
  if (ekto_getstack(L, 1, &ar) == 0 ||
      ekto_getinfo(L, "f", &ar) == 0 ||  /* get calling function */
      ekto_iscfunction(L, -1))
    ektoL_error(L, EKTO_QL("module") " not called from a Ekto function");
  ekto_pushvalue(L, -2);
  ekto_setfenv(L, -2);
  ekto_pop(L, 1);
}


static void dooptions (ekto_State *L, int n) {
  int i;
  for (i = 2; i <= n; i++) {
    ekto_pushvalue(L, i);  /* get option (a function) */
    ekto_pushvalue(L, -2);  /* module */
    ekto_call(L, 1, 0);
  }
}


static void modinit (ekto_State *L, const char *modname) {
  const char *dot;
  ekto_pushvalue(L, -1);
  ekto_setfield(L, -2, "_M");  /* module._M = module */
  ekto_pushstring(L, modname);
  ekto_setfield(L, -2, "_NAME");
  dot = strrchr(modname, '.');  /* look for last dot in module name */
  if (dot == NULL) dot = modname;
  else dot++;
  /* set _PACKAGE as package name (full module name minus last part) */
  ekto_pushlstring(L, modname, dot - modname);
  ekto_setfield(L, -2, "_PACKAGE");
}


static int ll_module (ekto_State *L) {
  const char *modname = ektoL_checkstring(L, 1);
  int loaded = ekto_gettop(L) + 1;  /* index of _LOADED table */
  ekto_getfield(L, EKTO_REGISTRYINDEX, "_LOADED");
  ekto_getfield(L, loaded, modname);  /* get _LOADED[modname] */
  if (!ekto_istable(L, -1)) {  /* not found? */
    ekto_pop(L, 1);  /* remove previous result */
    /* try global variable (and create one if it does not exist) */
    if (ektoL_findtable(L, EKTO_GLOBALSINDEX, modname, 1) != NULL)
      return ektoL_error(L, "name conflict for module " EKTO_QS, modname);
    ekto_pushvalue(L, -1);
    ekto_setfield(L, loaded, modname);  /* _LOADED[modname] = new table */
  }
  /* check whether table already has a _NAME field */
  ekto_getfield(L, -1, "_NAME");
  if (!ekto_isnil(L, -1))  /* is table an initialized module? */
    ekto_pop(L, 1);
  else {  /* no; initialize it */
    ekto_pop(L, 1);
    modinit(L, modname);
  }
  ekto_pushvalue(L, -1);
  setfenv(L);
  dooptions(L, loaded - 1);
  return 0;
}


static int ll_seeall (ekto_State *L) {
  ektoL_checktype(L, 1, EKTO_TTABLE);
  if (!ekto_getmetatable(L, 1)) {
    ekto_createtable(L, 0, 1); /* create new metatable */
    ekto_pushvalue(L, -1);
    ekto_setmetatable(L, 1);
  }
  ekto_pushvalue(L, EKTO_GLOBALSINDEX);
  ekto_setfield(L, -2, "__index");  /* mt.__index = _G */
  return 0;
}


/* }====================================================== */



/* auxiliary mark (for internal use) */
#define AUXMARK		"\1"

static void setpath (ekto_State *L, const char *fieldname, const char *envname,
                                   const char *def) {
  const char *path = getenv(envname);
  if (path == NULL)  /* no environment variable? */
    ekto_pushstring(L, def);  /* use default */
  else {
    /* replace ";;" by ";AUXMARK;" and then AUXMARK by default path */
    path = ektoL_gsub(L, path, EKTO_PATHSEP EKTO_PATHSEP,
                              EKTO_PATHSEP AUXMARK EKTO_PATHSEP);
    ektoL_gsub(L, path, AUXMARK, def);
    ekto_remove(L, -2);
  }
  setprogdir(L);
  ekto_setfield(L, -2, fieldname);
}


static const ektoL_Reg pk_funcs[] = {
  {"loadlib", ll_loadlib},
  {"seeall", ll_seeall},
  {NULL, NULL}
};


static const ektoL_Reg ll_funcs[] = {
  {"module", ll_module},
  {"require", ll_require},
  {NULL, NULL}
};


static const ekto_CFunction loaders[] =
  {loader_preload, loader_Ekto, loader_C, loader_Croot, NULL};


EKTOLIB_API int ektoopen_package (ekto_State *L) {
  int i;
  /* create new type _LOADLIB */
  ektoL_newmetatable(L, "_LOADLIB");
  ekto_pushcfunction(L, gctm);
  ekto_setfield(L, -2, "__gc");
  /* create `package' table */
  ektoL_register(L, EKTO_LOADLIBNAME, pk_funcs);
#if defined(EKTO_COMPAT_LOADLIB) 
  ekto_getfield(L, -1, "loadlib");
  ekto_setfield(L, EKTO_GLOBALSINDEX, "loadlib");
#endif
  ekto_pushvalue(L, -1);
  ekto_replace(L, EKTO_ENVIRONINDEX);
  /* create `loaders' table */
  ekto_createtable(L, 0, sizeof(loaders)/sizeof(loaders[0]) - 1);
  /* fill it with pre-defined loaders */
  for (i=0; loaders[i] != NULL; i++) {
    ekto_pushcfunction(L, loaders[i]);
    ekto_rawseti(L, -2, i+1);
  }
  ekto_setfield(L, -2, "loaders");  /* put it in field `loaders' */
  setpath(L, "path", EKTO_PATH, EKTO_PATH_DEFAULT);  /* set field `path' */
  setpath(L, "cpath", EKTO_CPATH, EKTO_CPATH_DEFAULT); /* set field `cpath' */
  /* store config information */
  ekto_pushliteral(L, EKTO_DIRSEP "\n" EKTO_PATHSEP "\n" EKTO_PATH_MARK "\n"
                     EKTO_EXECDIR "\n" EKTO_IGMARK);
  ekto_setfield(L, -2, "config");
  /* set field `loaded' */
  ektoL_findtable(L, EKTO_REGISTRYINDEX, "_LOADED", 2);
  ekto_setfield(L, -2, "loaded");
  /* set field `preload' */
  ekto_newtable(L);
  ekto_setfield(L, -2, "preload");
  ekto_pushvalue(L, EKTO_GLOBALSINDEX);
  ektoL_register(L, NULL, ll_funcs);  /* open lib into global table */
  ekto_pop(L, 1);
  return 1;  /* return 'package' table */
}

