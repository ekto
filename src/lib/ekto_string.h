/*
** $Id: lstring.h,v 1.43.1.1 2007/12/27 13:02:25 roberto Exp $
** String table (keep all strings handled by Ekto)
** See Copyright Notice in ekto.h
*/

#ifndef lstring_h
#define lstring_h


#include "ekto_gc.h"
#include "ekto_object.h"
#include "ekto_state.h"


#define sizestring(s)	(sizeof(union TString)+((s)->len+1)*sizeof(char))

#define sizeudata(u)	(sizeof(union Udata)+(u)->len)

#define ektoS_new(L, s)	(ektoS_newlstr(L, s, strlen(s)))
#define ektoS_newliteral(L, s)	(ektoS_newlstr(L, "" s, \
                                 (sizeof(s)/sizeof(char))-1))

#define ektoS_fix(s)	l_setbit((s)->tsv.marked, FIXEDBIT)

EKTOI_FUNC void ektoS_resize (ekto_State *L, int newsize);
EKTOI_FUNC Udata *ektoS_newudata (ekto_State *L, size_t s, Table *e);
EKTOI_FUNC TString *ektoS_newlstr (ekto_State *L, const char *str, size_t l);


#endif
