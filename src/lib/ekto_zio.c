/*
** $Id: lzio.c,v 1.31.1.1 2007/12/27 13:02:25 roberto Exp $
** a generic input stream interface
** See Copyright Notice in ekto.h
*/


#include <string.h>

#define lzio_c
#define EKTO_CORE

#include "Ekto.h"

#include "ekto_limits.h"
#include "ekto_mem.h"
#include "ekto_state.h"
#include "ekto_zio.h"


int ektoZ_fill (ZIO *z) {
  size_t size;
  ekto_State *L = z->L;
  const char *buff;
  ekto_unlock(L);
  buff = z->reader(L, z->data, &size);
  ekto_lock(L);
  if (buff == NULL || size == 0) return EOZ;
  z->n = size - 1;
  z->p = buff;
  return char2int(*(z->p++));
}


int ektoZ_lookahead (ZIO *z) {
  if (z->n == 0) {
    if (ektoZ_fill(z) == EOZ)
      return EOZ;
    else {
      z->n++;  /* ektoZ_fill removed first byte; put back it */
      z->p--;
    }
  }
  return char2int(*z->p);
}


void ektoZ_init (ekto_State *L, ZIO *z, ekto_Reader reader, void *data) {
  z->L = L;
  z->reader = reader;
  z->data = data;
  z->n = 0;
  z->p = NULL;
}


/* --------------------------------------------------------------- read --- */
size_t ektoZ_read (ZIO *z, void *b, size_t n) {
  while (n) {
    size_t m;
    if (ektoZ_lookahead(z) == EOZ)
      return n;  /* return number of missing bytes */
    m = (n <= z->n) ? n : z->n;  /* min. between n and z->n */
    memcpy(b, z->p, m);
    z->n -= m;
    z->p += m;
    b = (char *)b + m;
    n -= m;
  }
  return 0;
}

/* ------------------------------------------------------------------------ */
char *ektoZ_openspace (ekto_State *L, Mbuffer *buff, size_t n) {
  if (n > buff->buffsize) {
    if (n < EKTO_MINBUFFER) n = EKTO_MINBUFFER;
    ektoZ_resizebuffer(L, buff, n);
  }
  return buff->buffer;
}


