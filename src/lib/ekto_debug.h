/*
** $Id: ldebug.h,v 2.3.1.1 2007/12/27 13:02:25 roberto Exp $
** Auxiliary functions from Debug Interface module
** See Copyright Notice in ekto.h
*/

#ifndef ldebug_h
#define ldebug_h


#include "ekto_state.h"


#define pcRel(pc, p)	(cast(int, (pc) - (p)->code) - 1)

#define getline(f,pc)	(((f)->lineinfo) ? (f)->lineinfo[pc] : 0)

#define resethookcount(L)	(L->hookcount = L->basehookcount)


EKTOI_FUNC void ektoG_typeerror (ekto_State *L, const TValue *o,
                                             const char *opname);
EKTOI_FUNC void ektoG_concaterror (ekto_State *L, StkId p1, StkId p2);
EKTOI_FUNC void ektoG_aritherror (ekto_State *L, const TValue *p1,
                                              const TValue *p2);
EKTOI_FUNC int ektoG_ordererror (ekto_State *L, const TValue *p1,
                                             const TValue *p2);
EKTOI_FUNC void ektoG_runerror (ekto_State *L, const char *fmt, ...);
EKTOI_FUNC void ektoG_errormsg (ekto_State *L);
EKTOI_FUNC int ektoG_checkcode (const Proto *pt);
EKTOI_FUNC int ektoG_checkopenop (Instruction i);

#endif
