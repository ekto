/*
** $Id: linit.c,v 1.14.1.1 2007/12/27 13:02:25 roberto Exp $
** Initialization of libraries for ekto.c
** See Copyright Notice in ekto.h
*/


#define linit_c
#define EKTO_LIB

#include "Ekto.h"

#include "Ekto_Lib.h"
#include "Ekto_Aux.h"


static const ektoL_Reg ektolibs[] = {
  {"", ektoopen_base},
  {EKTO_LOADLIBNAME, ektoopen_package},
  {EKTO_TABLIBNAME, ektoopen_table},
  {EKTO_IOLIBNAME, ektoopen_io},
  {EKTO_OSLIBNAME, ektoopen_os},
  {EKTO_STRLIBNAME, ektoopen_string},
  {EKTO_MATHLIBNAME, ektoopen_math},
  {EKTO_DBLIBNAME, ektoopen_debug},
  {NULL, NULL}
};


EKTOLIB_API void ektoL_openlibs (ekto_State *L) {
  const ektoL_Reg *lib = ektolibs;
  for (; lib->func; lib++) {
    ekto_pushcfunction(L, lib->func);
    ekto_pushstring(L, lib->name);
    ekto_call(L, 1, 0);
  }
}

